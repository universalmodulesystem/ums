﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid.Localization;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraPrinting.Control;
using System.IO;
using UMS;
using System.Diagnostics;
using LLayer;

namespace UMS
{
    /// <summary>
    /// Русифицированный настроеный грид сразу в панели. Добавляй на форму и радуйся. Стандартные методы прилагаются
    /// </summary>
    public partial class GridPanel : UserControl
    {
        string grdViewSettingsPath=""; //Путь к папке с настройками грида
        /// <summary>
        /// Название файла настроек грида
        /// </summary>
        public string SettingsFileName { get; set; }
        /// <summary>
        /// Возвращает GridView, связанный с панелью
        /// </summary>
        public GridView GridView
        {
            get { return grdView; }
        }
        /// <summary>
        /// Возвращает GridControl, связанный с панелью
        /// </summary>
        public GridControl GridControl
        {
            get { return grdControl; }
        }
        /// <summary>
        /// Возвращает или задает признак видимости нижней панели таблицы
        /// </summary>
        public bool ShowFooter 
        {
           get {
                return GridView.OptionsView.ShowFooter;
               }
           set
           {
                GridView.OptionsView.ShowFooter = value;
           }
        }
        /// <summary>
        /// Выровнивает ширину колонок GridView по содержимому
        /// </summary>
        public void BestFitColumns()
        {
            grdView.BestFitColumns();
        }
        /// <summary>
        /// Возвращает или задает источник данных для таблицы
        /// </summary>
        public object DataSource
        {
            get { return grdControl.DataSource; }
            set { grdControl.DataSource = value; }
        }
        /// <summary>
        /// Возвращает или задает значение отображения автоматического фильтра  по колонкам
        /// </summary>
        public bool ShowAutoFilter
        {
            get { return grdView.OptionsView.ShowAutoFilterRow; }
            set { grdView.OptionsView.ShowAutoFilterRow = value; }
        }
        /// <summary>
        /// Производит экспорт видимой части таблицы в документ Excel
        /// </summary>
        /// <param name="Path"></param>
        public void Export(string Path)
        {
            //grdControl.ExportToExcelOld(Path);
            grdControl.ExportToXls(Path);

        }
        /// <summary>
        /// Сохраняет настройки отображения грида
        /// </summary>
        public void SaveViewSettings()
        {
                if (SettingsFileName != "")
                {
                    grdViewSettingsPath = UMS.Forms.FormSettingsFolder + "\\" + SettingsFileName + ".xml";
                    if (!Directory.Exists(System.IO.Path.GetDirectoryName(grdViewSettingsPath)))
                    {
                        Directory.CreateDirectory(System.IO.Path.GetDirectoryName(grdViewSettingsPath));
                    }
                    grdView.SaveLayoutToXml(grdViewSettingsPath);
                }
                else
                {
                    new Exception("Нельзя сохранить настройки грида, так как не указано имя файла для сохранения");
                }
        }
        /// <summary>
        /// Загружает настройки отображения грида
        /// </summary>
        public void LoadViewSettings()
        {
            if (File.Exists(grdViewSettingsPath))
            {
                grdView.RestoreLayoutFromXml(grdViewSettingsPath);
            }
        }
        public GridPanel()
        {
            ComponentPrinter c;
            ComponentExporter componentExporter;
            InitializeComponent();
            GridLocalizer.Active = new RussianGridLocalizer();//Устанавливаем локализацию экстрагрида
            Localizer.Active = new RussianEditorsLocalizer(); //Устанавливаем локализацию редакторов экстрагрида
            //grdViewSettingsPath = "A";
        }
        /// <summary>
        /// Класс локализации грида
        /// </summary>
        public class RussianGridLocalizer : GridLocalizer
        {
            public override string Language { get { return "Russia"; } }
            public override string GetLocalizedString(GridStringId id)
            {
                string ret = "";
                switch (id)
                {
                    // ...  
                    case GridStringId.GridGroupPanelText: return "Группировать";
                    case GridStringId.MenuColumnClearSorting: return "Сброс сортировки";
                    case GridStringId.MenuGroupPanelHide: return "Скрыть панель группировки";
                    case GridStringId.MenuColumnRemoveColumn: return "Скрыть колонку";
                    case GridStringId.MenuColumnFilterEditor: return "Фильтр";
                    case GridStringId.MenuColumnFindFilterShow: return "Поиск по колонкам";
                    case GridStringId.MenuColumnAutoFilterRowShow: return "Фильтр по колонкам";
                    case GridStringId.MenuColumnSortAscending: return "Сортировать по порядку";
                    case GridStringId.MenuColumnSortDescending: return "Сортировать в обратном порядке";
                    case GridStringId.MenuColumnGroup: return "Группировать по колонке";
                    case GridStringId.MenuColumnUnGroup: return "Разгруппировать";
                    case GridStringId.MenuColumnColumnCustomization: return "Скрытые колонки";
                    case GridStringId.MenuColumnShowColumn: return "Показать колонку";
                    case GridStringId.MenuColumnBestFit: return "Автоширина";
                    case GridStringId.MenuColumnFilter: return "Фильтр";
                    case GridStringId.MenuColumnClearFilter: return "Сбросить фильтр";
                    case GridStringId.MenuColumnBestFitAllColumns: return "Автоширина для всех колонок";
                    case GridStringId.MenuColumnGroupBox: return "Показать панель группировки";
                    case GridStringId.MenuColumnAutoFilterRowHide: return "Скрыть фильтр по колонкам";
                    case GridStringId.MenuColumnFindFilterHide: return "Скрыть поиск по колонкам";
                    //case GridStringId.CustomFilterDialogConditionEQU: return "Равно";
                    case GridStringId.CustomFilterDialogCaption: return "Редиактор фильтра";
                    case GridStringId.CustomFilterDialogConditionNotLike: return "Не содержит";
                    case GridStringId.MenuGroupPanelShow: return "Панель группировки";
                    case GridStringId.FilterBuilderOkButton: return "ОК";
                    case GridStringId.CustomFilterDialogCancelButton: return "Отмена";
                    case GridStringId.FilterBuilderCancelButton: return "Отмена";
                    case GridStringId.FilterBuilderApplyButton: return "Принять";
                    case GridStringId.FilterBuilderCaption: return "Редактор фильтра";
                    case GridStringId.FilterPanelCustomizeButton: return "Изменить фильтр";
                    // ...  
                    default:
                        ret = base.GetLocalizedString(id);
                        break;
                }
                return ret;
            }
        }
        /// <summary>
        /// класс локализации редактора грида
        /// </summary>
        public class RussianEditorsLocalizer : Localizer
        {
            public override string Language { get { return "Russia"; } }
            public override string GetLocalizedString(StringId id)
            {
                string ret = "";
                switch (id)
                {
                    // ... 
                    case StringId.NavigatorTextStringFormat: return "Zeile {0} von {1}";
                    case StringId.PictureEditMenuCut: return "Вырезать";
                    case StringId.PictureEditMenuCopy: return "Копировать";
                    case StringId.PictureEditMenuPaste: return "Вставить";
                    case StringId.PictureEditMenuDelete: return "Удалить";
                    case StringId.PictureEditMenuLoad: return "Загрузить";
                    case StringId.PictureEditMenuSave: return "Сохранить";
                    case StringId.FilterGroupAnd: return "И";
                    case StringId.FilterGroupNotAnd: return "НЕ И";
                    case StringId.FilterGroupOr: return "ИЛИ";
                    case StringId.FilterGroupNotOr: return "НЕ ИЛИ";
                    case StringId.FilterMenuClearAll: return "Очистить";
                    case StringId.FilterClauseBeginsWith: return "Начинается с";
                    case StringId.FilterClauseEquals: return "Равно";
                    case StringId.FilterClauseContains: return "Содержит";
                    case StringId.FilterClauseBetween: return "Между (не включительно)";
                    case StringId.FilterClauseDoesNotContain: return "Не содержит";
                    case StringId.FilterClauseDoesNotEqual: return "Не равно";
                    case StringId.FilterClauseGreater: return "Больше";
                    case StringId.FilterClauseGreaterOrEqual: return "Больше или равно";
                    case StringId.FilterClauseLessOrEqual: return "Меньше или равно";
                    case StringId.FilterClauseLess: return "Меньше";
                    case StringId.FilterClauseNotBetween: return "Не между";
                    case StringId.FilterClauseBetweenAnd: return "Между (включительно)";

                    case StringId.FilterEmptyEnter: return "< значение >";
                    case StringId.FilterMenuConditionAdd: return "Добавить условие";
                    case StringId.FilterMenuGroupAdd: return "Добавить группу условий";
                    case StringId.FilterMenuRowRemove: return "Удалить группу условий";
                    //Перевод для группировки фильтра внизу слева
                    case StringId.FilterCriteriaToStringGroupOperatorAnd: return "И";
                    case StringId.FilterCriteriaToStringGroupOperatorOr: return "ИЛИ";
                    case StringId.FilterCriteriaToStringNotLike: return "НЕ СОДЕРЖИТ";
                    default:
                        ret = base.GetLocalizedString(id);
                        break;
                        // ... 
                }
                return ret;
            }
        }
        /// <summary>
        /// Рисует номера строк
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdView_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }
        /// <summary>
        /// Открывает диалог сохранения файла и производит экспорт видимой части в Excel
        /// </summary>
        public void Export()
        {
            sfdExport.DefaultExt = "xls";
            sfdExport.Filter = "Excel | *.xls";
            
            if (sfdExport.ShowDialog() == DialogResult.OK)
            {
                bool ok = false;
                while (!ok)
                {
                    try
                    {
                        if (File.Exists(sfdExport.FileName))
                        {
                            using (FileStream fs = new FileStream(sfdExport.FileName, FileMode.Open)) { };
                        }
                        ok = true;
                        this.Export(sfdExport.FileName);
                    }
                    catch (IOException ex)
                    {
                        if (MessageBox.Show("Файл занят другим процессом. Попробовать сохранить еще раз?", "Файл занят", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                        {
                            return;
                        };
                    }
                }

                try
                {
                    if (MessageBox.Show("Открыть документ?", "Готово", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Process.Start(sfdExport.FileName);
                    }
                }
                catch (Exception ex)
                {
                    Errors.WriteError(ex);
                }
            }
        }
        /// <summary>
        /// Нажатие правой кнопкой на GridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grdView_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.CellValue != null)
            {
                Clipboard.SetText(e.CellValue.ToString());
                //ttInfo.ToolTipTitle = "Скопировано: " + e.CellValue.ToString();
                ttInfo.Show("Скопировано: " + e.CellValue.ToString(), this, grdControl.Left + e.X + 20, grdControl.Top + e.Y + 30, 1500);
            }
        }

        private void grdView_ShownEditor(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.BaseEdit edit = (sender as DevExpress.XtraGrid.Views.Base.BaseView).ActiveEditor;
            if (edit is DevExpress.XtraEditors.TextEdit)
                (edit as DevExpress.XtraEditors.TextEdit).Properties.MaxLength = 0;
        }

        public new void Dispose()
        {
            Dispose();
            GC.SuppressFinalize(this);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (!disposed)
        //    {
        //        if (disposing)
        //        {
        //            // Free other state (managed objects).
        //        }
        //        // Free your own state (unmanaged objects).
        //        // Set large fields to null.
        //        disposed = true;
        //    }
        //}

        // Use C# destructor syntax for finalization code.
        ~GridPanel()
        {
            // Simply call Dispose(false).
            Dispose();
        }

        private void GridPanel_Load(object sender, EventArgs e)
        {
            
        }
        public void AddFilterCriteria(string Col, string Val)
        {
            this.GridView.ActiveFilterCriteria = new DevExpress.Data.Filtering.ContainsOperator(Col, Val);
        }
    }
}
