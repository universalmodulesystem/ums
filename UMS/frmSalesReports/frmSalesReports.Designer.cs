﻿namespace UMS
{
    partial class frmSalesReports
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbGoods = new System.Windows.Forms.RadioButton();
            this.rbSales = new System.Windows.Forms.RadioButton();
            this.btnYear = new System.Windows.Forms.Button();
            this.btnToday = new System.Windows.Forms.Button();
            this.btnYerstaday = new System.Windows.Forms.Button();
            this.btnCurrentMonth = new System.Windows.Forms.Button();
            this.btnPreviousMonth = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.grdSales = new global::UMS.GridPanel();
            this.pnlButtons.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlButtons.Controls.Add(this.panel1);
            this.pnlButtons.Controls.Add(this.btnYear);
            this.pnlButtons.Controls.Add(this.btnToday);
            this.pnlButtons.Controls.Add(this.btnYerstaday);
            this.pnlButtons.Controls.Add(this.btnCurrentMonth);
            this.pnlButtons.Controls.Add(this.btnPreviousMonth);
            this.pnlButtons.Controls.Add(this.label2);
            this.pnlButtons.Controls.Add(this.label1);
            this.pnlButtons.Controls.Add(this.dtpEnd);
            this.pnlButtons.Controls.Add(this.dtpStart);
            this.pnlButtons.Controls.Add(this.btnClose);
            this.pnlButtons.Controls.Add(this.btnExport);
            this.pnlButtons.Controls.Add(this.btnRefresh);
            this.pnlButtons.Location = new System.Drawing.Point(5, 4);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(904, 52);
            this.pnlButtons.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbGoods);
            this.panel1.Controls.Add(this.rbSales);
            this.panel1.Location = new System.Drawing.Point(444, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(144, 40);
            this.panel1.TabIndex = 19;
            // 
            // rbGoods
            // 
            this.rbGoods.AutoSize = true;
            this.rbGoods.Location = new System.Drawing.Point(3, 20);
            this.rbGoods.Name = "rbGoods";
            this.rbGoods.Size = new System.Drawing.Size(106, 17);
            this.rbGoods.TabIndex = 1;
            this.rbGoods.Text = "Список товаров";
            this.rbGoods.UseVisualStyleBackColor = true;
            this.rbGoods.CheckedChanged += new System.EventHandler(this.rbSales_CheckedChanged);
            // 
            // rbSales
            // 
            this.rbSales.AutoSize = true;
            this.rbSales.Checked = true;
            this.rbSales.Location = new System.Drawing.Point(3, 3);
            this.rbSales.Name = "rbSales";
            this.rbSales.Size = new System.Drawing.Size(103, 17);
            this.rbSales.TabIndex = 0;
            this.rbSales.TabStop = true;
            this.rbSales.Text = "Список продаж";
            this.rbSales.UseVisualStyleBackColor = true;
            this.rbSales.CheckedChanged += new System.EventHandler(this.rbSales_CheckedChanged);
            // 
            // btnYear
            // 
            this.btnYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnYear.Location = new System.Drawing.Point(29, 32);
            this.btnYear.Name = "btnYear";
            this.btnYear.Size = new System.Drawing.Size(82, 19);
            this.btnYear.TabIndex = 18;
            this.btnYear.Text = "Год";
            this.btnYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnYear.UseVisualStyleBackColor = true;
            this.btnYear.Click += new System.EventHandler(this.btnSetDate_Click);
            // 
            // btnToday
            // 
            this.btnToday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnToday.Location = new System.Drawing.Point(357, 32);
            this.btnToday.Name = "btnToday";
            this.btnToday.Size = new System.Drawing.Size(82, 19);
            this.btnToday.TabIndex = 17;
            this.btnToday.Text = "Сегодня";
            this.btnToday.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnToday.UseVisualStyleBackColor = true;
            this.btnToday.Click += new System.EventHandler(this.btnSetDate_Click);
            // 
            // btnYerstaday
            // 
            this.btnYerstaday.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnYerstaday.Location = new System.Drawing.Point(275, 32);
            this.btnYerstaday.Name = "btnYerstaday";
            this.btnYerstaday.Size = new System.Drawing.Size(82, 19);
            this.btnYerstaday.TabIndex = 16;
            this.btnYerstaday.Text = "Вчера";
            this.btnYerstaday.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnYerstaday.UseVisualStyleBackColor = true;
            this.btnYerstaday.Click += new System.EventHandler(this.btnSetDate_Click);
            // 
            // btnCurrentMonth
            // 
            this.btnCurrentMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCurrentMonth.Location = new System.Drawing.Point(193, 32);
            this.btnCurrentMonth.Name = "btnCurrentMonth";
            this.btnCurrentMonth.Size = new System.Drawing.Size(82, 19);
            this.btnCurrentMonth.TabIndex = 15;
            this.btnCurrentMonth.Text = "текущий месяц";
            this.btnCurrentMonth.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCurrentMonth.UseVisualStyleBackColor = true;
            this.btnCurrentMonth.Click += new System.EventHandler(this.btnSetDate_Click);
            // 
            // btnPreviousMonth
            // 
            this.btnPreviousMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnPreviousMonth.Location = new System.Drawing.Point(111, 32);
            this.btnPreviousMonth.Name = "btnPreviousMonth";
            this.btnPreviousMonth.Size = new System.Drawing.Size(82, 19);
            this.btnPreviousMonth.TabIndex = 14;
            this.btnPreviousMonth.Text = "прошлый месяц";
            this.btnPreviousMonth.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPreviousMonth.UseVisualStyleBackColor = true;
            this.btnPreviousMonth.Click += new System.EventHandler(this.btnSetDate_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(223, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "по:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "с:";
            // 
            // dtpEnd
            // 
            this.dtpEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpEnd.Location = new System.Drawing.Point(261, 8);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(177, 22);
            this.dtpEnd.TabIndex = 9;
            // 
            // dtpStart
            // 
            this.dtpStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpStart.Location = new System.Drawing.Point(29, 8);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(190, 22);
            this.dtpStart.TabIndex = 8;
            this.dtpStart.ValueChanged += new System.EventHandler(this.dtpStart_ValueChanged);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnClose.Location = new System.Drawing.Point(802, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(98, 41);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Закрыть (Esc)";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnExport
            // 
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btnExport.Location = new System.Drawing.Point(698, 6);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(98, 41);
            this.btnExport.TabIndex = 2;
            this.btnExport.Text = "Экспорт";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRefresh.Location = new System.Drawing.Point(594, 6);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(98, 41);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Обновить (F5)";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // grdSales
            // 
            this.grdSales.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdSales.DataSource = null;
            this.grdSales.Location = new System.Drawing.Point(5, 61);
            this.grdSales.Name = "grdSales";
            this.grdSales.SettingsFileName = null;
            this.grdSales.ShowAutoFilter = false;
            this.grdSales.ShowFooter = false;
            this.grdSales.Size = new System.Drawing.Size(904, 383);
            this.grdSales.TabIndex = 7;
            // 
            // frmSalesReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 445);
            this.Controls.Add(this.grdSales);
            this.Controls.Add(this.pnlButtons);
            this.Name = "frmSalesReports";
            this.Text = "Отчет по продажам";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmSalesReports_KeyUp);
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button btnYear;
        private System.Windows.Forms.Button btnToday;
        private System.Windows.Forms.Button btnYerstaday;
        private System.Windows.Forms.Button btnCurrentMonth;
        private System.Windows.Forms.Button btnPreviousMonth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnRefresh;
        private GridPanel grdSales;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbGoods;
        private System.Windows.Forms.RadioButton rbSales;
    }
}

