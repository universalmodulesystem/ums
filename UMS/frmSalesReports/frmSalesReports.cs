﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UMS
{
    public partial class frmSalesReports : Forms
    {
        public frmSalesReports()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            dtpStart.Value = DateTime.Now;
            dtpEnd.Value = DateTime.Now;
            btnRefresh.PerformClick();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            UpdateGrid(rbSales.Checked);
        }
        private void UpdateGrid(bool Sales)
        {
            if (Sales)
            {
                grdSales.DataSource = null;
                grdSales.GridView.Columns.Clear();
                grdSales.DataSource = LLayer.Sale.GetSales(dtpStart.Value, dtpEnd.Value);
                grdSales.GridView.Columns["Итого"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum; //Добавить строчку с суммой для колонки Итог
                grdSales.BestFitColumns(); //И зачем-то еще раз по ширине выровним
                grdSales.GridView.OptionsView.ShowAutoFilterRow = true; //Покажем автофильтр по колнкам принудительно
                grdSales.ShowAutoFilter = true; //Покажем автофильтр по колнкам принудительно
            }
            else
            {
                grdSales.DataSource = null;
                grdSales.GridView.Columns.Clear();
                grdSales.DataSource = LLayer.Sale.GetGoodsSales(dtpStart.Value, dtpEnd.Value);
                grdSales.GridView.Columns["Итого"].SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum; //Добавить строчку с суммой для колонки Итог
                grdSales.BestFitColumns(); //И зачем-то еще раз по ширине выровним
                grdSales.GridView.OptionsView.ShowAutoFilterRow = true; //Покажем автофильтр по колнкам принудительно
                grdSales.ShowAutoFilter = true; //Покажем автофильтр по колнкам принудительно
            }
            grdSales.GridView.OptionsView.ShowFooter = true;
        }

        private void btnYear_Click(object sender, EventArgs e)
        {

        }

        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Устанавливает указанные даты в элементах выбора дат
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        private void SetDates(DateTime dateFrom, DateTime dateTo)
        {
            dtpStart.Value = dateFrom;
            dtpEnd.Value = dateTo;
        }

        private void btnSetDate_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.Name)
            {
                case "btnYear":
                    {
                        SetDates(new DateTime(DateTime.Now.Year, 1, 1), new DateTime(DateTime.Now.Year, 12, 31));
                        break;
                    }
                case "btnPreviousMonth":
                    {
                        SetDates(new DateTime(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month, 1), new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1));
                        break;
                    }
                case "btnCurrentMonth":
                    {
                        SetDates(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1).AddDays(-1));
                        break;
                    }
                case "btnYerstaday":
                    {
                        SetDates(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day));
                        break;
                    }
                case "btnToday":
                    {
                        SetDates(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                        break;
                    }
            }
            btnRefresh.PerformClick();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            grdSales.Export();
        }

        private void rbSales_CheckedChanged(object sender, EventArgs e)
        {
            btnRefresh.PerformClick();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmSalesReports_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    {
                        btnRefresh.PerformClick();
                        break;
                    }
                case Keys.Escape:
                    {
                        btnClose.PerformClick();
                        break;
                    }
            }
        }
    }
}
