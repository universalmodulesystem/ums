﻿using DevExpress.XtraGrid.Columns;
using LLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UMS;
using DevExpress.XtraGrid.Views.Base;

namespace UMS
{
    public partial class frmProviders : Forms
    {
        Provider p;

        public frmProviders()
        {
            InitializeComponent();
        }

        private void frmProviders_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            btnRefresh.PerformClick();
            this.gpProviders.GridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(ValueChanged);
            this.gpProviders.GridView.ShowingEditor += new CancelEventHandler(ShowEditor);
            this.gpProviders.GridView.HiddenEditor += new EventHandler(CloseEditor);

        }
        private void ValueChanged(object sender, CellValueChangedEventArgs e)
        {
            p.Name = Convert.ToString(this.gpProviders.GridView.ActiveEditor.EditValue);
        }
        private void CloseEditor(object sender, EventArgs e)
        {
            if (p != null)
            {
                p.Update();
                p = null;
            }
        }
        private void ShowEditor(object sender, CancelEventArgs e)
        {
            int id;
            int[] selRows = gpProviders.GridView.GetSelectedRows();
            if (selRows[0] >= 0)
            {
                DataRowView selRow = (DataRowView)((gpProviders.GridView).GetRow(selRows[0]));
                if (int.TryParse(Convert.ToString(selRow["id"]), out id))
                {
                    p = Provider.GetProvider(id);
                }
                else
                {
                    p = new Provider();
                }
            }
            else
            {
                p = new Provider();
                //SendKeys.Send("{DOWN}");
            }
        }
        private void gpProviders_Load(object sender, EventArgs e)
        {

        }
        private void DataBind()
        {
            gpProviders.DataSource = Provider.GetProviders();
            gpProviders.BestFitColumns();
            gpProviders.ShowAutoFilter = true;
            foreach (GridColumn c in gpProviders.GridView.Columns)
            {
                c.OptionsFilter.AutoFilterCondition = AutoFilterCondition.Contains;
            }
            gpProviders.GridView.OptionsBehavior.ReadOnly = false;
            gpProviders.GridView.OptionsBehavior.Editable = true;
            gpProviders.GridView.Columns["Название"].OptionsColumn.AllowEdit = true;
            gpProviders.GridView.Columns["id"].OptionsColumn.AllowEdit = false;

            gpProviders.BestFitColumns();
         }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            DataBind();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            gpProviders.GridView.AddNewRow();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int id;
            int[] selRows = gpProviders.GridView.GetSelectedRows();
            if (selRows[0] >= 0)
            {
                DataRowView selRow = (DataRowView)((gpProviders.GridView).GetRow(selRows[0]));
                if (int.TryParse(Convert.ToString(selRow["id"]), out id))
                {
                    p = Provider.GetProvider(id);
                    if (MessageBox.Show("Удалить поставщика с ID " + p.id.ToString()+  " '"+ p.Name+"'?","Удаление поставщика", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2 ) == DialogResult.Yes)
                    {
                        p.Delete();
                        btnRefresh.PerformClick();
                    }
                }
            }
        }
    }
}
