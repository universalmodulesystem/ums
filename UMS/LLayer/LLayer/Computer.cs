﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace LLayer
{
    /// <summary>
    /// Класс настроек текущего компьютера
    /// </summary>
    public class Computer
    {
        int _ComputerId;
        string _ComputerName;
        /// <summary>
        /// К компьютеру подключена касса
        /// </summary>
        public bool IsKassa { get; set; }
        /// <summary>
        /// Задает или возвращает идентификатор текущего компьютера
        /// </summary>
        public int Id
        {
            get
            {
                return _ComputerId;
            }

            set
            {
                _ComputerId = value;
            }
        }
        /// <summary>
        /// Возвращает или задает имя компьютера
        /// </summary>
        public string ComputerName
        {
            get
            {
                return _ComputerName;
            }

            set
            {
                _ComputerName = value;
            }
        }
        /// <summary>
        /// Получает данные по всем компьютерам
        /// </summary>
        /// <returns></returns>
        public static DataTable GetComputers()
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("Call GetComputers()");
        }
        /// <summary>
        /// Получает имя компьютера
        /// </summary>
        public static string GetComputerName()
        {
            return Environment.MachineName;
        }
        /// <summary>
        /// Получает идентфикатор компьютера по его имени
        /// </summary>
        /// <param name="computerName">Имя компьютера</param>
        /// <returns></returns>
        public static int GetComputerId(string computerName)
        {
            return Convert.ToInt16(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT GetComputerIdByName(?ComputerName)", new object[] {computerName}));
        }
        /// <summary>
        /// Добавляет компьютер в таблицу компьютеров
        /// </summary>
        /// <param name="computerName">Имя компьютера</param>
        /// <returns>ID</returns>
        public static int AddComputerToBase(string computerName)
        {
            return Convert.ToInt16(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT AddComputer(?ComputerName)", new object[] { computerName }));
        }
    }
}
