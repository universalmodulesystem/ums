﻿using System;
using System.IO;
using System.Xml.Linq;
using System.Drawing;
using System.Windows.Forms;
using LLayer;

namespace UMS
{
    /// <summary>
    /// Класс для работы с исключениями и ошибками. NLog? Не, не слышали
    /// </summary>
    public class Errors
    {
        static string sFileName = "error.log";
        /// <summary>
        /// Записывает ошибку в файл. Если файла нет - создает
        /// </summary>
        /// <param name="sErrorText">Текст ошибки</param>
        public static void WriteErrorToLog(string sErrorText)
        {
            XDocument doc;
            if (!File.Exists(sFileName))
            {
                doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("Errors"));
            }
            else
            {
                doc = XDocument.Load(sFileName);
            }
            XElement root = new XElement("Error");
            root.Add(new XAttribute("Date", DateTime.Now.ToString()));
            root.Add(new XAttribute("UCid",Session.User.UCid.ToString()));
            root.Value = sErrorText;
            doc.Element("Errors").Add(root);
            doc.Save(sFileName);
        }
        /// <summary>
        /// Записывает ошибку в файл. Если файла нет - создает
        /// </summary>
        /// <param name="sErrorText">Текст ошибки</param>
        public static void WriteErrorToLog(Exception ex)
        {
            string sErrorText = ex.ToString();
            XDocument doc;
            if (!File.Exists(sFileName))
            {
                doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("Errors"));
            }
            else
            {
                doc = XDocument.Load(sFileName);
            }
            XElement root = new XElement("Error");
            root.Add(new XAttribute("Date", DateTime.Now.ToString()));
            root.Add(new XAttribute("UCid",Session.User.UCid.ToString()));
            root.Value = sErrorText;
            doc.Element("Errors").Add(root);
            doc.Save(sFileName);
        }
        /// <summary>
        /// Записывает ошибку в базу данных
        /// </summary>
        /// <param name="sErrorText"></param>
        public static void WriteErrorToDatabase(string sErrorText)
        {
            try
            {
                Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL InsertError(?UCid, ?ErrorText, ?Screen)", new object[] { Session.User.UCid, sErrorText, Screenshot() });              
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex.ToString());
            }
        }
        /// <summary>
        /// Делает скриншот
        /// </summary>
        /// <returns></returns>
        public static byte[] Screenshot()
        {
            Size ScreenSize = Screen.PrimaryScreen.Bounds.Size;
            Bitmap image = new Bitmap(ScreenSize.Width, ScreenSize.Height);
            using (Graphics g = Graphics.FromImage(image))
            {
                g.CopyFromScreen(Point.Empty, Point.Empty, ScreenSize);
                using (var stream = new MemoryStream())
                {
                    image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    return stream.ToArray();
                }
                //image.Save("1.bmp", System.Drawing.Imaging.ImageFormat.Jpeg);
                //return null;
            }
        }
        /// <summary>
        /// Записывает ошибку в БД.
        /// </summary>
        /// <param name="ShowMessageBox">Показывать окно с ошибкой</param>
        public static void WriteError(string ErrorText, bool ShowMessageBox = true)
        {
            WriteErrorToDatabase(ErrorText);
            if (ShowMessageBox)
            {
                MessageBox.Show(ErrorText, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
        /// <summary>
        /// Записывает текст исключения в БД и отображает окно с текстом исключения
        /// </summary>
        /// <param name="ShowMessageBox">Показывать окно с ошибкой</param>
        public static void WriteError(Exception ex, bool ShowMessageBox = true)
        {
            WriteErrorToDatabase(ex.ToString());
            if (ShowMessageBox)
            {
                MessageBox.Show(ex.ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
