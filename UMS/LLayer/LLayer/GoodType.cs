﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LLayer
{
    /// <summary>
    /// Тип товара
    /// </summary>
    public class GoodType
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; }
        
        public string Name { get; set; }
        /// <summary>
        /// Получает данные по товару
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GoodType GetGoodType(int id)
        {
            GoodType gt = null;
            DataTable dtType = Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetGoodType(?id)", new object[] { id });
            if (dtType.Rows.Count > 0)
            {
                gt = new GoodType();
                foreach (DataColumn Col in dtType.Columns)
                {
                    PropertyInfo fi = typeof(Good).GetProperty(Col.ColumnName);
                    if (fi != null)
                    {
                        Console.WriteLine(dtType.Rows[0][Col].ToString());
                        try
                        {

                            if (fi.PropertyType.Equals(typeof(Version)))
                            {
                                fi.SetValue(gt, Convert.ChangeType(new Version(Convert.ToString(dtType.Rows[0][Col])), fi.PropertyType), null);
                            }
                            else
                            {
                                object value = dtType.Rows[0][Col];
                                if (DBNull.Value == value)
                                {
                                    if (fi.PropertyType.IsValueType)
                                    {
                                        value = Activator.CreateInstance(fi.PropertyType);
                                    }
                                    else
                                    {
                                        value = null;
                                    }
                                }
                                fi.SetValue(gt, Convert.ChangeType(value, fi.PropertyType), null);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return gt;
        }
        /// <summary>
        /// Получает все типы товаров
        /// </summary>
        /// <returns></returns>
        public static DataTable GetGoodTypes()
        {
            return Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetGoodTypes()");
        }
    }
}
