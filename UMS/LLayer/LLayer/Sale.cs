﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace LLayer
{
    /// <summary>
    /// Класс продажи
    /// </summary>
    public class Sale
    {
        /// <summary>
        /// Идентификатор продажи
        /// </summary>
        public int id {get;set;}
        /// <summary>
        /// Итого
        /// </summary>
        double total = 0;


        /// <summary>
        /// Создает и открывает продажу
        /// </summary>
        public Sale()
        {
            id = Convert.ToInt32(Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("SELECT OpenSale(GetOpenedSaleStatusID(), null)"));
            //Goods.CollectionChanged += listChanged;
        }
        /// <summary>
        /// Закрывает продажу
        /// </summary>
        /// <returns>Общая сумма</returns>
        public double CloseSale()
        {
            return Convert.ToDouble(Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("SELECT CloseSale(?sale_Id)", new object[] { id }));
        }
        /// <summary>
        /// Отменяет продажу
        /// </summary>
        /// <returns>Общая сумма</returns>
        public double ExitSale()
        {
            return Convert.ToDouble(Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("SELECT ExitSale(?sale_Id)", new object[] { id }));
        }

        /// <summary>
        /// Добавляет товар в продажу
        /// </summary>
        /// <param name="good">товар</param>
        /// <param name="quantity">Количество продаваемого товара</param>
        /// <returns>Общая сумма продажи товара</returns>
        public double AddGood(Good good, int quantity)
        {
            return Convert.ToDouble(Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("SELECT add_good_to_sale(?sale_Id, ?good_id, ?quantity)", new object[] { id, good.id, quantity }));
        }

        /// <summary>
        /// Получает товары в продаже
        /// </summary>
        /// <returns></returns>
        public DataTable GetGoods()
        {
            return (Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetGoodsInSale(?sale_Id)", new object[] { id }));
        }

        /// <summary>
        /// Получает все продажи за период времени
        /// </summary>
        public static DataTable GetSales(DateTime startDate, DateTime endDate)
        {
            return (Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetSales(?startDate, ?endDate)", new object[] { startDate, endDate }));
        }
        /// <summary>
        /// Получает все проданные товары за период времени
        /// </summary>
        public static DataTable GetGoodsSales(DateTime startDate, DateTime endDate)
        {
            return (Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetGoodsSales(?startDate, ?endDate)", new object[] { startDate, endDate }));
        }
        private void listChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs args)
        {
            // list changed
            //RecalcTotal();
        }

        /// <summary>
        /// Возвращает значение Итого
        /// </summary>
        public double Total
        {
            get
            {
               // total = RecalcTotal();
                return total;
            }
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = TotalChanged;
            if (handler != null)
                handler(this, e);
        }

        protected void OnTotalChanged()
        {
            OnPropertyChanged(new PropertyChangedEventArgs("Total"));
        }

        /// <summary>
        /// Событие изменение суммы Итого
        /// </summary>
        public event PropertyChangedEventHandler TotalChanged;

    }
}
