﻿using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace LLayer
{
    /// <summary>
    /// Класс работы с пользователями.
    /// </summary>
    public class User: IDisposable
    {
        #region Классы
        /// <summary>
        /// Класс настроек пользователя
        /// </summary>
         public class PropertiesDictionary: Dictionary<string, object>
        {
            /// <summary>
            /// Возвращает значение параметра настройки по его имени (null в случае отсутствия параметра)
            /// </summary>
            /// <param name="PropertyName">Название параметра</param>
            /// <returns>Значение параметра. В случае отсутствия параметра возвращает null</returns>
            new public object this[string PropertyName]
            {
                get
                {
                    if (this.ContainsKey(PropertyName))
                    {
                        object value;
                        this.TryGetValue(PropertyName, out value);
                        return value;
                        //return this[PropertyName];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        #endregion
        #region Поля
        int iId = 0; //Идентификатор записи пользователь-компьютер
        int iComputerId; //Номер компьютера
        int iUserId; //Номер оператора
        string sOperatorName; //Имя оператора
        string sOperatorMiddleName;//Отчество оператора
        string sOperatorSurname; //Фамилия оператора
        int iGroupId; //Код группы
        public bool Active { get; set; }//Признак активности
        /// <summary>
        /// Список номеров компьютеров на которых разрешена работа данному пользователю
        /// </summary>
        public List<int> Computers { get; set; } = new List<int>();
        /// <summary>
        /// Название группы, к которой относится пользователь
        /// </summary>
        public string GroupName {get; private set; }
        public DataTable dtMenu = new DataTable(); //Данные по меню
        /// <summary>
        /// Возвращает или задает словарик с настройками пользователя
        /// </summary>
        public PropertiesDictionary Preferences;

        #endregion
        #region Свойства
        /// <summary>
        /// Имя юзера для подключения к базе
        /// </summary>
        public string db_user_name { get; set; }
        /// <summary>
        /// Флаг кассира
        /// </summary>
        public bool IsKassir { get; set; }

        /// <summary>
        /// Идентификатор пользователя в users
        /// </summary>
        public int UserId
        {
            get { return iUserId; }
            set { iUserId = value; }
        }
        /// <summary>
        /// Идентификатор записи пользователь-компьютер
        /// </summary>
        public int UCid
        {
            get { return iId; }
            set { iId = value; }
        }

        /// <summary>
        /// Имя Оператора
        /// </summary>
        public string Name
        {
            get { return sOperatorName; }
            set { sOperatorName = value; }
        }
        /// <summary>
        /// Отчество Оператора
        /// </summary>
        public string MiddleName
        {
            get { return sOperatorMiddleName; }
            set { sOperatorMiddleName = value; }
        }
        /// <summary>
        /// Фамилия Оператора
        /// </summary>
        public string Surname
        {
            get { return sOperatorSurname; }
            set { sOperatorSurname = value; }
        }

        /// <summary>
        /// Полные Фамилия Имя Отчество
        /// </summary>
        public string FullName
        {
            get { return sOperatorSurname + " " + sOperatorName + " " + sOperatorMiddleName; }
        }
        /// <summary>
        /// Сокращенные Фамилия И.О.
        /// </summary>
        public string ShortName
        {
            get { return sOperatorSurname + " " + sOperatorName[0] + "." + sOperatorMiddleName[0] + "."; }
        }
        /// <summary>
        /// Возвращает или задает Код группы, к которой относится пользователь
        /// </summary>
        public int GroupID
        {
            get
            {
                return iGroupId;
            }

            set
            {
                iGroupId = value;
            }
        }
        #endregion
        #region Методы
        public User()
        {

        }
        /// <summary>
        /// Создает экземпляр пользователя
        /// </summary>
        /// <param name="id"></param>
        public User(int id)
        {
            DataTable dtUserData = Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetUser(?UserId)", new object[] { id });
            if (dtUserData.Rows.Count == 1)
            {
                this.UserId = id;
                this.UCid = Convert.ToInt16(dtUserData.Rows[0]["id"]);
                this.Surname = dtUserData.Rows[0]["Surname"].ToString();
                this.MiddleName = dtUserData.Rows[0]["MiddleName"].ToString();
                this.Name = dtUserData.Rows[0]["Name"].ToString();
                this.GroupID = Convert.ToInt16(dtUserData.Rows[0]["GroupId"]);
                //this.GroupName = Convert.ToString(dtUserData.Rows[0]["GroupName"]);
                //this.IsKassir = Convert.ToBoolean(dtUserData.Rows[0]["IsKassir"]);
                this.Active = Convert.ToBoolean(dtUserData.Rows[0]["Active"]);
                //Session.User.dtMenu = GetUserMenu(Session.User.UCid)
                try
                {
                    this.Computers=this.GetComputers().Rows.Cast<DataRow>().Select(row => Convert.ToInt32(row["ComputerId"])).ToList();
                }
                catch(Exception ex)
                {
                    Console.Write(ex);
                }
                //this.Computers.AddRange();

                
            }
        }
        /// <summary>
        /// Получает значение свойств элементов управления заданной формы для группы пользователя
        /// </summary>
        /// <param name="FormName">Имя формы для которой требуется получить настройки</param>
        /// <returns></returns>
        public DataTable GetFormPropertySettings(string FormName)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetDefaultFormControlsProperties(?FormName, ?UserId)", new object[] { FormName, UserId });
        }
        /// <summary>
        /// Получает значение свойств элементов управления заданной формы для группы пользователя
        /// </summary>
        /// <param name="FormName">Имя формы для которой требуется получить настройки</param>
        /// <returns></returns>
        public DataTable GetFormPropertySettings(int FormId)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetDefaultFormControlsPropertiesById(?FormId, ?UserId)", new object[] { FormId, UserId });
        }
        /// <summary>
        /// Получает список пунктов меню для пользователя
        /// </summary>
        /// <param name="UCid">Идентификатор User-Computer</param>
        /// <returns></returns>
        public DataTable GetUserMenu(int UCid)
        {
            //return Session.DataBases[Session.DataBasesName.Terminal].FillDataTableByQuery("CALL GetUserMenu(?UCId)", new object[] { UCid });
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetUserMenu(?UCId)", new object[] { UCid });
        }
        ///// <summary>
        ///// Получает список пунктов меню, по нажатию которых не открывается форма, а происходит указанное событие
        ///// </summary>
        ///// <returns></returns>
        //public DataTable GetUserMenuWithoutForms()
        //{
        //    return Session.DataBases[Session.DataBasesName.Terminal].FillDataTableByQuery("CALL GetMenuWithoutforms()");
        //}
        /// <summary>
        /// Получает список пунктов меню, по нажатию которых не открывается форма, а происходит указанное событие
        /// </summary>
        /// <returns></returns>
        public DataTable GetUserMenuWithoutForms()
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetUserMenuWithoutForms(?uCid)", new object[] { UCid });
        }
        /// <summary>
        /// Производит вход под указанным пользователем
        /// </summary>
        /// <param name="UserId">Идентификатор пользователя</param>
        /// <param name="ComputerId">Идентификатор компьютера</param>
        /// <returns></returns>
        public bool Login(int UserId, int ComputerId)
        {
            bool ok = FillUserData(UserId, ComputerId);
            Logs.AddLog(1);
            return ok;
        }
        public void Exit()
        {
            Dispose();
            Logs.AddLog(2);
        }
        
        /// <summary>
        /// Производит вход под указанным пользователем
        /// </summary>
        /// <param name="UserId">Идентификатор пользователя</param>
        /// <param name="ComputerName">Имя компьютера</param>
        /// <returns></returns>
        public bool Login(int UserId, string ComputerName)
        {
            //Если понадобится позже
            return false;
        }
        /// <summary>
        /// Заполняет данные пользователя
        /// </summary>
        private bool FillUserData(int UserId, int ComputerId)
        {
           
            DataTable dtUserData = Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("Call GetUserDataByUserIdComputerId(?UserId, ?ComputerId)", new object[] { UserId, ComputerId });
            if (dtUserData.Rows.Count == 1)
            {
                //Получим пункты меню
                Session.User.dtMenu = GetUserMenu(Session.User.UCid);
                //Заполним данные юзера. Можно было и рефлексией, конечно
                Session.User.UserId = UserId;
                Session.User.UCid = Convert.ToInt16(dtUserData.Rows[0]["id"]);
                Session.User.Surname = dtUserData.Rows[0]["Surname"].ToString();
                Session.User.MiddleName = dtUserData.Rows[0]["MiddleName"].ToString();
                Session.User.Name = dtUserData.Rows[0]["Name"].ToString();
                Session.User.GroupID = Convert.ToInt16(dtUserData.Rows[0]["GroupId"]);
                Session.User.GroupName = Convert.ToString(dtUserData.Rows[0]["GroupName"]);
                Session.User.IsKassir = Convert.ToBoolean(dtUserData.Rows[0]["IsKassir"]);
                Session.Computer.IsKassa = Convert.ToBoolean(dtUserData.Rows[0]["IsKassa"]);
                Session.Computer.ComputerName= Convert.ToString(dtUserData.Rows[0]["ComputerName"]);
                Session.User.db_user_name = Convert.ToString(dtUserData.Rows[0]["db_username"]);
                //Заполним настройки
                Session.User.GetPreferences();
                //Удалим файл смены
                File.Delete("Smena");
                //Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL SetComputerName(?id, ?ComputerName)", new object[] { Session.User.UCid, Environment.MachineName });
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Получает текущее меню пользователя
        /// </summary>
        /// <returns></returns>
        public DataTable GetMenu()
        {
            dtMenu = GetUserMenu(UCid);
            return dtMenu;
        }
        /// <summary>
        /// Получает настройки пользователя и заполняет словарик с настройками
        /// </summary>
        public void GetPreferences()
        {
            Preferences = new PropertiesDictionary();
            DataTable dt = Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetUserPreferences(?UserId)", new object[] { UserId });
            foreach (DataRow r in dt.Rows)
            {
                Preferences.Add(r["Name"].ToString(), r["Value"]);
            }
        }
        /// <summary>
        /// Добавляет нового пользователя
        /// </summary>
        public int Add()
        {
            return Convert.ToInt32(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL AddUser(?surname, ?name, ?middleName, ?active, ?groupID, ?ComputerIdString)", new object[] { Surname, Name, MiddleName, Active, GroupID, String.Join(", ", Computers.ToArray()) }));
        }
        /// <summary>
        /// Обновляет данные пользователя
        /// </summary>
        public void Update()
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL UpdateUser(?id, ?surname, ?name, ?middleName, ?active, ?groupID, ?ComputerIdString)", new object[] {UserId ,Surname, Name, MiddleName, Active, GroupID, String.Join(", ", Computers.ToArray())});
        }
        /// <summary>
        /// Удаляет пользвоателя (Делает неактивным)
        /// </summary>
        public void Delete()
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL SetUserInactive(?id)", new object[] {UserId });
        }
        /// <summary>
        /// Получает идентификаторы компьютеров на которых может работать пользователь
        /// </summary>
        /// <returns></returns>
        public DataTable GetComputers()
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetComputersByUser(?UserId)", new object[] { UserId });
        }
        public void Dispose()
        {
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        #endregion
        #region Статические методы
        /// <summary>
        /// Получает данные пользователей 
        /// </summary>
        /// <param name="OnlyActive">True - Только активных, False - всех</param>
        public static DataTable GetUsers(bool OnlyActive = true)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("Call GetUsers(?Active)", new object[] { OnlyActive ? 1 : 0 });
        }
        /// <summary>
        /// Получает пользователей, у которых есть доступ на этом компьютере
        /// </summary>
        /// <param name="OnlyActive">True - Только активных, False - всех</param>
        public static DataTable GetUsers(int ComputerId)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("Call GetUsersByComputer(?Computer)", new object[] { ComputerId });
        }
        /// <summary>
        /// Получает идентификатор группы пользователей "Кассиры"
        /// </summary>
        /// <returns></returns>
        public static int GetKassirsGroupID()
        {
            return Convert.ToInt16(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT GetKassirsGroupID()"));
        }
        /// <summary>
        /// Получает ФИО оператора по его идентификатору
        /// </summary>
        /// <param name="OperatorId">Идентификатор оператора</param>
        /// <returns>ФИО</returns>
        public static string GetOperatorFIO(int OperatorId)
        {
            return Convert.ToString(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT GetOperatorFIO(?id)", new object[] { OperatorId }));
        }
        public static DataTable GetUsersByGroupId( int GroupId, bool OnlyActive = true)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("Call GetUsersByGroupId( ?GroupId, ?Active)", new object[] {GroupId,   OnlyActive ? 1 : 0 });
        }/// <summary>
        /// Получает список операторов в кассовой справке в зависимости от того, кто ее формирует
        /// </summary>
        /// <param name="GroupId"></param>
        /// <param name="OnlyActive"></param>
        /// <returns></returns>
        public static DataTable GetUsersByGroupIdCashReport(int GroupId, bool OnlyActive = true)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("Call GetUsersByGroupIdCashReport( ?GroupId, ?Active)", new object[] { GroupId, OnlyActive ? 1 : 0 });
        }
        /// <summary>
        /// Получает ид базы ems оператора по его идентификатору
        /// </summary>
        /// <param name="OperatorId">Идентификатор оператора</param>
        /// <returns>ид базы ems</returns>
        public static int GetOldIdOperator(int OperatorId)
        {
            return Convert.ToInt32(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT GetOldIdOperator(?id)", new object[] { OperatorId }));
        }
        /// <summary>
        /// Получает группу оператора по его идентификатору
        /// </summary>
        /// <param name="OperatorId">Идентификатор оператора</param>
        /// <returns>группа</returns>
        public static int GetOperatorGroupId(int OperatorId)
        {
            return Convert.ToInt32(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT GetOperatorGroupId(?id)", new object[] { OperatorId }));
        }
        ///<summary>
        /// Получает все группы пользователей
        /// </summary>
        /// <returns></returns>
        public static DataTable GetGroups()
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetGroups()");
        }
        /// <summary>
        /// Проверяет правильность пароля пользователя
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool CheckPassword(int UserId, string password)
        {
            return Convert.ToBoolean(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT CheckPassword(?UserID, ?pass)", new object[] { UserId, password }));
        }

       
        #endregion
    }
}
