﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LLayer;
using System.Reflection;

namespace LLayer
{
    /// <summary>
    /// Класс товара
    /// </summary>
    public class Good: IGood
    {
        public Good g {get;set;}
        public int id { get; set; }
        /// <summary>
        /// Код типа товара
        /// </summary>
        public int Type_Id { get; set; }
        /// <summary>
        /// Штрих-код товара
        /// </summary>
        public string BarCode { get; set; }
        /// <summary>
        /// Название товара
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Цена
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Минимальная розничная цена
        /// </summary>
        public double mrp { get; set; }
        /// <summary>
        /// Количество
        /// </summary>
        public int amount { get; set; }
        /// <summary>
        /// Идентификатор поставщика
        /// </summary>
        public int provider_id { get; set; }
        /// <summary>
        /// Цена закупки
        /// </summary>
        public double purchase_price { get; set; }

        public Good()
        {

        }
        /// <summary>
        /// Делает продажу товара
        /// </summary>
        /// <param name="quantity">Количествоо продаваемого товара</param>
        /// <returns></returns>
        public void Sale(int quantity)
        {
            Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL SaleGood(?id, ?Quantity)", new object[] {id, quantity });
        }

        public void Update()
        {
            List<object> Parameters = new List<object>();
            Parameters.Add(this.id);
            Parameters.Add(this.Type_Id);
            Parameters.Add(this.BarCode);
            Parameters.Add(this.Name);
            Parameters.Add(this.purchase_price);
            Parameters.Add(this.Price);
            Parameters.Add(this.mrp);
            Parameters.Add(this.amount);
            Parameters.Add(this.provider_id);
            Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("CALL UpdateGood(?id, ?Type_Id,?BarCode,?Name, ?purchase_price, ?Price, ?mrp, ?amount, ?provider_id)", Parameters.ToArray());
        }
        public void Delete()
        {
            List<object> Parameters = new List<object>();
            Parameters.Add(this.id);
            Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("CALL DeleteGood(?id)", Parameters.ToArray());
        }
        /// <summary>
        /// Получает данные по товару
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Good GetGood(int id)
        {
            Good g = null;
            DataTable dtGood = Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetGood(?id)", new object[] { id });
            if (dtGood.Rows.Count > 0)
            {     
                g = new Good();
                foreach (DataColumn Col in dtGood.Columns)
                {
                    PropertyInfo fi = typeof(Good).GetProperty(Col.ColumnName);
                    if (fi != null)
                    {
                        Console.WriteLine(dtGood.Rows[0][Col].ToString());
                        try
                        {

                            if (fi.PropertyType.Equals(typeof(Version)))
                            {
                                fi.SetValue(g, Convert.ChangeType(new Version(Convert.ToString(dtGood.Rows[0][Col])), fi.PropertyType), null);
                            }
                            else
                            {
                                object value = dtGood.Rows[0][Col];
                                if (DBNull.Value == value)
                                {
                                    if (fi.PropertyType.IsValueType)
                                    {
                                        value = Activator.CreateInstance(fi.PropertyType);
                                    }
                                    else
                                    {
                                        value = null;
                                    }
                                }
                                fi.SetValue(g, Convert.ChangeType(value, fi.PropertyType), null);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return g;
        }
        /// <summary>
        /// Получает товар из базы
        /// </summary>
        /// <param name="barCode"></param>
        /// <returns></returns>
        public static Good GetGood(string barCode)
        {
            int id=0;
            Int32.TryParse(Convert.ToString(Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("SELECT GetGoodIdByBarCode(?BarCode)", new object[] { barCode })), out id);
 
            return GetGood(id);
        }

        /// <summary>
        /// Получает все товары
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllGoods()
        {
            return Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetAllGoods()");
        }

    }
}
