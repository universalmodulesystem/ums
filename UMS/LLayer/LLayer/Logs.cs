﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LLayer
{
    /// <summary>
    /// Класс работы с логами
    /// </summary>
    public class Logs
    {
        /// <summary>
        /// Добавляет запись в таблицу логов
        /// </summary>
        /// <param name="operation_id">id операции</param>
        /// <param name="Comment">Доп комментарий</param>
        public static void AddLog(int operation_id, string Comment= null)
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL AddToLog(?UCid,?operation_id, ?Comment, ?Version)", new object[] { Session.User.UCid,operation_id ,Comment, System.Windows.Forms.Application.ProductVersion.ToString() });
        }
    }
}
