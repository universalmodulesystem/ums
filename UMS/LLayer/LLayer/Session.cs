﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LLayer
{
    /// <summary>
    /// Класс текущей сессии работы с программой
    /// </summary>
    public class Session
    {
        //public struct ConnectionName
        //{
        //    public static string EMS = "EMS";
        //    public static string Terminal = "Terminal";
        //}
        /// <summary>
        /// Наименования баз данных
        /// </summary>
        public struct DataBasesName
        {
            public static string IndEnt = "IntEnd";
            public static string UMS = "UMS";
        }
        static User _user = new User(); //Текущий пользователь
        static Computer _computer = new Computer();//Текущий компьютер
        static Dictionary<string, DbConnection> _connections = new Dictionary<string, DbConnection>(); //Подключения к базам
        static Dictionary<string, DataBase> _DataBases = new Dictionary<string, DataBase>();
        /// <summary>
        /// Возвращает или задает текущего пользователя сессии
        /// </summary>
        public static User User
        {
            get
            {
                return _user;
            }

            set
            {
                _user = value;
            }
        }
        ///// <summary>
        ///// Возвращает словарик с основными подключениями программы
        ///// </summary>
        //public static Dictionary<string, DbConnection> Connections
        //{
        //    get
        //    {
        //        return _connections;
        //    }
        //}
        /// <summary>
        /// Возвращает словарь объектов стандартных методов работы с базой данных
        /// </summary>
        /// <remarks>Ключ - название базы, значение - объект со стандартными методами</remarks>
        public static Dictionary<string, DataBase> DataBases
        {
            get
            {
                return _DataBases;
            }
        }
        /// <summary>
        /// Возвращает или задает настрйоки текущего компьютера
        /// </summary>
        public static Computer Computer
        {
            get
            {
                return _computer;
            }

            set
            {
                _computer = value;
            }
        }
    }
}
