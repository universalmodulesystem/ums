﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;

namespace LLayer
{
    /// <summary>
    /// Поставщик
    /// </summary>
    public class Provider
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; } = 0;
        public string Name { get; set; }

        public void Update()
        {
            List<object> Parameters = new List<object>();
            Parameters.Add(this.id);
            Parameters.Add(this.Name);
            Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("CALL UpdateProvider(?id, ?Name)", Parameters.ToArray());
        }
        /// <summary>
        /// Удаляет поставщика
        /// </summary>
        public void Delete()
        {
            List<object> Parameters = new List<object>();
            Parameters.Add(this.id);
            Session.DataBases[Session.DataBasesName.IndEnt].ExecuteQuery("CALL DeleteProvider(?id)", Parameters.ToArray());
        }

        /// <summary>
        /// Получает данные по Поставщику
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Provider GetProvider(int id)
        {
            Provider p = null;
            DataTable dtProvider = Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetProvider(?id)", new object[] { id });
            if (dtProvider.Rows.Count > 0)
            {
                p = new Provider();
                foreach (DataColumn Col in dtProvider.Columns)
                {
                    PropertyInfo fi = typeof(Provider).GetProperty(Col.ColumnName);
                    if (fi != null)
                    {
                        Console.WriteLine(dtProvider.Rows[0][Col].ToString());
                        try
                        {

                            if (fi.PropertyType.Equals(typeof(Version)))
                            {
                                fi.SetValue(p, Convert.ChangeType(new Version(Convert.ToString(dtProvider.Rows[0][Col])), fi.PropertyType), null);
                            }
                            else
                            {
                                object value = dtProvider.Rows[0][Col];
                                if (DBNull.Value == value)
                                {
                                    if (fi.PropertyType.IsValueType)
                                    {
                                        value = Activator.CreateInstance(fi.PropertyType);
                                    }
                                    else
                                    {
                                        value = null;
                                    }
                                }
                                fi.SetValue(p, Convert.ChangeType(value, fi.PropertyType), null);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            return p;
        }
        /// <summary>
        /// Получает всех поставщиков
        /// </summary>
        /// <returns></returns>
        public static DataTable GetProviders ()
        {
            return Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetProviders()");
        }
    }
}
