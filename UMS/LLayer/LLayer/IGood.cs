﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LLayer
{
    public interface IGood
    {
        Good g { get; set; }
        int id { get; set; }
        int Type_Id { get; set; }
        string BarCode { get; set; }
        string Name { get; set; }
        double Price { get; set; }
        double mrp { get; set; }
        int amount { get; set; }
        int provider_id { get; set; }
    }
}
