﻿
using System;
using System.Data;
using System.Reflection;
using System.Globalization;
using System.Windows.Forms;
using System.IO;

namespace LLayer
{
    /// <summary>
    /// Класс глобальныъх переменных и констант
    /// </summary>
    public static class Global
    {
        /// <summary>
        /// Возвращает текущую региональную настройку
        /// </summary>
        public static CultureInfo culture = new CultureInfo(CultureInfo.CurrentUICulture.Name);
        /// <summary>
        /// Возвращает текущую инвариантную региональную настройку
        /// </summary>
        public static CultureInfo InvariantCulture =  CultureInfo.InvariantCulture;
        /// <summary>
        /// Названия месяцев в родительном падеже
        /// </summary>
        enum MonthsInGenetiveCase {января, февраля, марта, апреля, мая, июня, июля, августа, сентября, октября, ноября, декабря};
        /// <summary>
        /// Код России в таблице стран
        /// </summary>
        public static int RussianCountryCode { get; set; }
        /// <summary>
        /// Код города
        /// </summary>
        public static int CurrentCityCode { get; set; }
        /// <summary>
        /// Ставка НДС (дробное)
        /// </summary>
        public static double NDS { get; set; }
        /// <summary>
        /// Аргументы запуска программы
        /// </summary>
        public static string[] args;
        /// <summary>
        /// Пароль администратора кассы
        /// </summary>
        public static int KassaAdminPassword { get; set; }
        /// <summary>
        /// Пароль оператора кассы
        /// </summary>
        public static int KassaUserPassword { get; set; }
        /// <summary>
        /// Код Петербургского филиала Гарантпоста в таблице контрагентов
        /// </summary>
        public static int SpbContractAgentCode { get; set; }
        /// <summary>
        /// Код Петербургского филиала Гарантпоста 
        /// </summary>
        public static int Spb_Bureau_id { get; set; }
        /// <summary>
        /// Код типа отправителя "Частное лицо"
        /// </summary>
        public static byte CashPayerTypeCode { get; set; }
        /// <summary>
        /// Путь к папке, где хранятся изображения почтовых ярлыков
        /// </summary>
        public static string OutgoingPostingsLabelsDirectory { get; set; }
        /// <summary>
        /// Текущая дата, полученная с сервера
        /// </summary>
        public static DateTime ServerDate { get; set; }

        /// <summary>
        /// Путь к архиву с ярлыками
        /// </summary>
        public static string LabelsArchivePath { get; set; }
        /// <summary>
        /// Путь к папке с отчетами для Москвы
        /// </summary>
        public static string ReportPath { get; set; }
        private static string TempFolder;
        /// <summary>
        /// Возвращает полный путь к временной папке приложения
        /// </summary>
        public static string TempFolderPath
        {
            get
            {
                return GetTempFolder();
            }
        }
        #region Статичные методы
        /// <summary>
        /// Получает значения глобальных настроек из базы
        /// </summary>
        public static bool GetGlobalVariables()
        {
            try
            {
                DataTable dtGlobal = new DataTable();
                dtGlobal = Session.DataBases[Session.DataBasesName.IndEnt].FillDataTableByQuery("CALL GetGlobalVariables()");
                PropertyInfo fi;
                foreach (DataRow Row in dtGlobal.Rows)
                {
                    fi = typeof(Global).GetProperty(Row["Name"].ToString());
                    if (fi != null)
                    {
                        object value = Row["value"];
                        if (fi.PropertyType == typeof(double))
                        {
                            value = Double.Parse(value.ToString(), InvariantCulture);
                        }
                        fi.SetValue(null, Convert.ChangeType(value, fi.PropertyType), null);
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
        /// <summary>
        /// Получает директорию для текущего дня,  в которой хранятся ярлыки отправлений
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        public static string GetOutgoingPostingsLabelsDirectory()
        {
            string Path = Global.OutgoingPostingsLabelsDirectory; //Базовый путь к ярлыкам
            Path += @"\" + ServerDate.Year.ToString();//Год
            Path += @"\" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(ServerDate.Month);//Название месяца
            Path +=@"\"+ ServerDate.Day.ToString("D2")+ " "+ ((MonthsInGenetiveCase)(ServerDate.Month-1)).ToString();
            Path += @"\";
            return Path;
            
        }
        /// <summary>
        /// Получает директорию для заданного дня,  в которой хранятся ярлыки отправлений
        /// </summary>
        /// <param name="Date">Дата</param>
        /// <returns></returns>
        public static string GetOutgoingPostingsLabelsDirectory(DateTime Date)
        {
            string Path = Global.OutgoingPostingsLabelsDirectory; //Базовый путь к ярлыкам
            Path += @"\" + Date.Year.ToString();//Год
            Path += @"\" + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Date.Month);//Название месяца
            Path += @"\" + String.Format("{0:D2}", Date.Day.ToString()) + " " + ((MonthsInGenetiveCase)(Date.Month - 1)).ToString();
            Path += @"\";
            return Path;

        }
        /// <summary>
        /// Получает путь к временному каталогу программы. Создает каталогв случае его отсутствия
        /// </summary>
        /// <returns></returns>
        public static string GetTempFolder()
        {
            string TempFolderPath = Application.StartupPath + "\\temp\\";
            if (!Directory.Exists(TempFolderPath))
            {
                Directory.CreateDirectory(TempFolderPath);
            }
            return TempFolderPath;
        }
        #endregion
    }
}
