﻿namespace LLayer
{
    /// <summary>
    /// Настройки, связанные со слоем логики
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Настройки подключений к базам данных
        /// </summary>
        public static class ConnectionSettings
        {
            private static string _UMSRootConnectionString; //Строка подключения к базе данных программы
            private static string _IntEndRootConnectionString; //Строка подключения к базе данных
            /// <summary>
            /// Возвращает или задает строку подключения к базе DeliveryService
            /// </summary>
            public static string DsConnectionString { get; set; }
            /// <summary>
            /// Возвращает или задает строку подключения к базе данных программы от имени адмиистратора
            /// </summary>
            public static string UMSRootConnectionString
            {
                get
                {
                    return _UMSRootConnectionString;
                }

                set
                {
                    _UMSRootConnectionString = value;
                }
            }
            /// <summary>
            /// Возвращает или задает строку подключения к базе данных от имени администратора
            /// </summary>
            public static string IntEndRootConnectionString
            {
                get
                {
                    return _IntEndRootConnectionString;
                }

                set
                {
                    _IntEndRootConnectionString = value;
                }
            }
        }
    }
}
