﻿using DAO;
using System.Data;
using System.IO;

namespace LLayer
{
    /// <summary>
    /// Объект стандартных методов работы с базой данных
    /// </summary>
    public class DataBase
    {

        string _ConnectionString;//Строка подключения к базе
        DbConnection _Connection;
        DbDataReader _drReader;
        /// <summary>
        /// Возвращает или задает соединение с базой данных
        /// </summary>
        private DbConnection Connection
        {
            get
            {
                return _Connection;
            }

            set
            {
                _Connection = value;
            }
        }

        /// <summary>
        /// Конструктор объекта стандартных методов работы с базой данных. Создет новое подключение к базе
        /// </summary>
        /// <param name="ConnectionString">Строка подключения</param>
        public DataBase(string ConnectionString)
        {
            _ConnectionString = ConnectionString;
            Connection = new DbConnection(_ConnectionString);
            Connection.Open();
        }
        /// <summary>
        /// Возвращает таблицу, заполненную на основе запроса
        /// </summary>
        /// <param name="Query">Запрос</param>
        /// <returns></returns>
        public DataTable FillDataTableByQuery(string Query)
        {
            return Connection.FillDataTableByQuery(Query);
        }
        /// <summary>
        /// Заполняет таблицу, заполненную на основе запроса с параметрами
        /// </summary>
        /// <param name="Query">Запрос</param>
        /// <param name="Parameters">Массив параметров</param>
        /// <returns></returns>
        public DataTable FillDataTableByQuery(string Query, object[] Parameters)
        {
            return Connection.FillDataTableByQuery(Query, Parameters);
        }
        /// <summary>
        /// Выполняет запрос с параметрами
        /// </summary>
        /// <param name="Query">Запрос</param>
        /// <param name="Parameters">Массив параметров (в том порядке, в каком они идут в строке запроса слева направо)</param>
        /// <returns></returns>
        public object ExecuteQuery(string Query, object[] Parameters = null)
        {
            return Connection.ExecuteQuery(Query, Parameters);
        }

        /// <summary>
        /// Сохраняет результат запроса в файл
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <param name="Query">текст запроса</param>
        /// <param name="Parameters">параметры</param>
        public bool SaveToFile(string path, string Query, object[] Parameters = null)
        {
            return Connection.SaveToFile(path, Query, Parameters);
        }


        ///// <summary>
        ///// Заполняет DataTable на основе запроса
        ///// </summary>
        ///// <param name="Query">Запрос к базе данных</param>
        ///// <returns>DataTable с данными</returns>
        //public static DataTable FillDataTableByQuery(string Query)
        //{
        //    using (DbConnection Connect = new DbConnection(Settings.ConnectionSettings.EmsConnectionString))
        //    {
        //        using (DbCommand command = new DbCommand(Query, Connect))
        //        {
        //            using (DbDataAdapter adapter = new DbDataAdapter(command))
        //            {
        //                using (DataTable data = new DataTable())
        //                {
        //                    adapter.Fill(data);
        //                    return data;
        //                }
        //            }
        //        }
        //    }
        //}
        ///// <summary>
        ///// Заполняет DataTable на основе запроса и параметров
        ///// </summary>
        ///// <param name="Query">Запрос к базе данных</param>
        ///// <param name="Parameters">Массив параметров</param>
        ///// <returns>DataTable с данными</returns>
        //public static DataTable FillDataTableByQuery(string Query, DbParameters Parametres)
        //{
        //    using (DbConnection Connect = new DbConnection(Settings.ConnectionSettings.EmsConnectionString))
        //    {
        //        Connect.Open();
        //        using (DbCommand command = new DbCommand(Query, Connect))
        //        {
        //            foreach (MyDbParameter Par in Parametres)
        //            {
        //                command.Parameters.AddWithValue(Par.ParameterName, Par.Value);
        //            }
        //            using (DbDataAdapter adapter = new DbDataAdapter(command))
        //            {
        //                using (DataTable data = new DataTable())
        //                {
        //                    adapter.Fill(data);
        //                    return data;
        //                }
        //            }
        //        }
        //    }
        //}
        ///// <summary>
        ///// Выполняет запрос с указанными параметрами
        ///// </summary>
        ///// <param name="Query">Запрос</param>
        ///// <param name="Parameters">Параметры</param>
        ///// <returns></returns>
        //public static object ExecuteQuery(string Query, DbParameters Parametres)
        //{
        //    using (DbConnection Connect = new DbConnection(Settings.ConnectionSettings.EmsConnectionString))
        //    {
        //        Connect.Open(); //Если не открыто - может выдать исключение "Не указана ссылка на объект"
        //        using (DbCommand command = new DbCommand(Query, Connect))
        //        {
        //            foreach (MyDbParameter Par in Parametres)
        //            {
        //                command.Parameters.AddWithValue(Par.ParameterName, Par.Value);
        //            }

        //            return command.ExecuteScalar();
        //        }
        //    }
        //}
        ///// <summary>
        ///// Выполняет комманду и возвращает результат выполнения
        ///// </summary>
        ///// <param name="Query">Запрос команды</param>
        ///// <returns></returns>
        //public static object ExecuteScalar(string Query)
        //{
        //    using (DbConnection Connect = new DbConnection(Settings.ConnectionSettings.EmsConnectionString))
        //    {
        //        using (DbCommand command = new DbCommand(Query, Connect))
        //        {
        //            return command.ExecuteScalar();
        //        }
        //    }
        //}
        ///// <summary>
        ///// Выполняет комманду с заданными параметрами и возвращает результат выполнения
        ///// </summary>
        ///// <param name="Query">Запрос команды</param>
        ///// <param name="Parametres">Параметры</param>
        ///// <returns></returns>
        //public static object ExecuteScalar(string Query, DbParameters Parametres)
        //{
        //    using (DbConnection Connect = new DbConnection(Settings.ConnectionSettings.EmsConnectionString))
        //    {
        //        using (DbCommand command = new DbCommand(Query, Connect))
        //        {
        //            foreach (MyDbParameter Par in Parametres)
        //            {
        //                command.Parameters.AddWithValue(Par.ParameterName, Par.Value);
        //            }
        //            return command.ExecuteScalar();
        //        }
        //    }
        //}
        /// <summary>
        /// Получает список используемых БД и данные для аутентификации
        /// </summary>
        /// <returns></returns>
        public static DataTable GetDataBases()
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetDataBases()");
        }
        /// <summary>
        /// Устанавливает строку подключения к базе данных программы
        /// </summary>
        public static void SetUMSRootConnectionString(string server, uint port, string dbname, string user, string passord)
        {
            DbConnectionStringBuilder UMSConnectionStringBuilder = new DbConnectionStringBuilder();
            UMSConnectionStringBuilder.Server = server;
            UMSConnectionStringBuilder.Port = port;
            UMSConnectionStringBuilder.UserID = user;
            UMSConnectionStringBuilder.Database = dbname;
            LLayer.Settings.ConnectionSettings.UMSRootConnectionString = UMSConnectionStringBuilder.ConnectionString + ";Convert Zero Datetime=True";
        }
    }
}
