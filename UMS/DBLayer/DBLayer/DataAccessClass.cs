﻿using System;
using MySql.Data.MySqlClient;
using System.Data;
using System.Collections;
using System.Data.Common;
using System.Text.RegularExpressions;
using System.IO;

namespace DAO //Весьма велосипедно
{
    ///Сие есть слой доступа к данным. Использовать только в слое логике. Использование в других местах указывает на то, что что-то пошло не так
    ///Entity Framework? Не, не слышали
    /// <summary>
    /// Класс подключения к базе
    /// </summary>
    public class DbConnection : IDbConnection
    {
        private bool disposed = false; // IsDisposed
        private MySqlConnection _Connect; //Собственно, коннект, с которым работаем
        private MySqlDataReader _dr;
        /// <summary>
        /// Конструктор соединения
        /// </summary>
        public DbConnection()
        {
            _Connect = new MySqlConnection();
        }
        /// <summary>
        /// Конструктор соединения
        /// </summary>
        /// <param name="connectionString">Строка подключения к базе</param>
        public DbConnection(string connectionString)
        {
            _Connect = new MySqlConnection(connectionString);
        }
        /// <summary>
        /// Открывает подключение
        /// </summary>
        public void Open()
        {
            _Connect.Open();
        }
        /// <summary>
        /// Закрывает подключение
        /// </summary>
        public void Close()
        {
            _Connect.Close();
                
        }
        /// <summary>
        /// Клонирует текущее подключение
        /// </summary>
        /// <returns></returns>
        public DbConnection Clone()
        {
            return  (DbConnection)_Connect.Clone();
        }
        /// <summary>
        /// Возвращает состояние подключения
        /// </summary>
        public ConnectionState State
        {
            get { return _Connect.State; }
        }

        public string ConnectionString
        {
            get
            {
                if (_Connect.ConnectionString == "")
                {
                    _Connect.ConnectionString = "User Id=root;Host=localhost;Character Set=cp1251";
                }
                return _Connect.ConnectionString;
                throw new NotImplementedException();
            }

            set
            {
                _Connect.ConnectionString = value;
            }
        }

        public int ConnectionTimeout
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string Database
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public MySqlConnection GetConnection()
        {
            return _Connect;
        }
        ///// <summary>
        ///// Метод явного преобразования
        ///// </summary>
        ///// <param name="v">Connection</param>
        //public static explicit operator MySqlConnection(DbConnection v)
        //{
        //    return _Connect;
        //    throw new NotImplementedException();
        //}
        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                disposed = true;
            }
        }

        public IDbTransaction BeginTransaction()
        {
            throw new NotImplementedException();
        }

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            throw new NotImplementedException();
        }

        public void ChangeDatabase(string databaseName)
        {
            throw new NotImplementedException();
        }

        public IDbCommand CreateCommand()
        {
            throw new NotImplementedException();
        }

        ///// <summary>
        ///// Заполняет DataTable на основе запроса
        ///// </summary>
        ///// <param name="Query">Запрос к базе данных</param>
        ///// <returns>DataTable с данными</returns>
        //public DataTable FillDataTableByQuery_old(string Query)
        //{
        //    using (DbConnection conn = new DbConnection(ConnectionString))
        //    {
        //        using (DbCommand command = new DbCommand(Query, conn))
        //        {
        //            using (DbDataAdapter adapter = new DbDataAdapter(command))
        //            {
        //                using (DataTable data = new DataTable())
        //                {
        //                    adapter.Fill(data);
        //                    return data;
        //                }
        //            }
        //        }
        //    }
        //}
        /// <summary>
        /// Заполняет DataTable на основе запроса
        /// </summary>
        /// <param name="Query">Запрос к базе данных</param>
        /// <returns>DataTable с данными</returns>
        public DataTable FillDataTableByQuery(string Query)
        {
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(Query, conn))
                {
                    using (MySqlDataAdapter adapter = new MySqlDataAdapter(command))
                    {
                        using (DataTable data = new DataTable())
                        {
                            adapter.Fill(data);
                            return data;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Заполняет DataTable на основе запроса
        /// </summary>
        /// <param name="Query">Запрос к базе данных</param>
        /// <param name="Parameters">Массив с параметрами</param>
        /// <returns>DataTable с данными</returns>
        /// 
        public DataTable FillDataTableByQuery_old(string Query, object[] Parameters)
        {
            //Найдем имена параметров в запросе с помощью регулярного выражения
            Regex reg = new Regex("\\?\\w+", RegexOptions.IgnoreCase);
            MatchCollection mc = reg.Matches(Query);
            //Проверим, совпадаетли число параметров, которые нашли в запросе и количество параметров, которые передали
            if (mc.Count != Parameters.Length) { new Exception("Количество передаваемых параметров  (" + Parameters.Length.ToString() + ") не совпадает с количеством ожидаемых параметров (" + mc.Count.ToString() + ")"); }
            using (DbConnection conn = new DbConnection(ConnectionString))
            {
                using (DbCommand command = new DbCommand(Query, conn))
                {
                    for (int i = 0; i < Parameters.Length; i++)
                    {
                        command.Parameters.AddWithValue(mc[i].ToString(), Parameters[i]);
                    }
                    using (DbDataAdapter adapter = new DbDataAdapter(command))
                    {
                        using (DataTable data = new DataTable())
                        {
                            adapter.Fill(data);
                            return data;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Заполняет DataTable на основе запроса
        /// </summary>
        /// <param name="Query">Запрос к базе данных</param>
        /// <param name="Parameters">Массив с параметрами</param>
        /// <returns>DataTable с данными</returns>
        /// 
        public DataTable FillDataTableByQuery(string Query, object[] Parameters)
        {
            //Найдем имена параметров в запросе с помощью регулярного выражения
            Regex reg = new Regex("\\?\\w+", RegexOptions.IgnoreCase);
            MatchCollection mc = reg.Matches(Query);
            //Проверим, совпадаетли число параметров, которые нашли в запросе и количество параметров, которые передали
            if (mc.Count != Parameters.Length) { throw(new Exception("Количество передаваемых параметров  (" + Parameters.Length.ToString() + ") не совпадает с количеством ожидаемых параметров (" + mc.Count.ToString() + ")")); }
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(Query, conn))
                {
                    for (int i = 0; i < Parameters.Length; i++)
                    {
                        command.Parameters.AddWithValue(mc[i].ToString(), Parameters[i]);
                    }
                    using (MySqlDataAdapter adapter = new MySqlDataAdapter(command))
                    {
                        using (DataTable data = new DataTable("NoName"))
                        {
                            adapter.Fill(data);
                            return data;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Выполняет запрос с указанными параметрами
        /// </summary>
        /// <param name="Query">Запрос</param>
        /// <param name="Parameters">Параметры</param>
        /// <returns></returns>
        public object ExecuteQuery(string Query, DbParameters Parametres)
        {
            //Не уверен насчет такого использования подключения. Если подключение занято - может не сработать. Возможно, нужно клонировать подключение перед выполнением
            if (this.State != ConnectionState.Open) { this.Open(); }
            using (DbConnection conn = new DbConnection(ConnectionString))
            {
                if (conn.State != ConnectionState.Open) { conn.Open(); }
                using (DbCommand command = new DbCommand(Query, conn))
                {
                    foreach (MyDbParameter Par in Parametres)
                    {
                        command.Parameters.AddWithValue(Par.ParameterName, Par.Value);
                    }
                    return command.ExecuteScalar();
                }
            }
            
        }

        /// <summary>
        /// Выполняет запрос с указанными параметрами
        /// </summary>
        /// <param name="Query">Запрос</param>
        /// <param name="Parameters">Массив Параметров (в том порядке, в каком они идут в строке запроса слева направо)</param>
        /// <returns></returns>
        public object ExecuteQuery_old(string Query, object[] Parameters = null)
        {
            object ret;
            if (Parameters == null) { Parameters = new object[] { }; }
                //Найдем имена параметров в запросе с помощью регулярного выражения
                Regex reg = new Regex("\\?\\w+", RegexOptions.IgnoreCase);
            MatchCollection mc = reg.Matches(Query);
            //Проверим, совпадаетли число параметров, которые нашли в запросе и количество параметров, которые передали
            if (mc.Count != Parameters.Length) { new Exception("Количество передаваемых параметров  (" + Parameters.Length.ToString() + ") не совпадает с количеством ожидаемых параметров (" + mc.Count.ToString() + ")"); }
            //Не уверен насчет такого использования подключения. Если подключение занято - может не сработать. Возможно, нужно клонировать подключение перед выполнением
            using (DbConnection conn = new DbConnection(ConnectionString))
            {
                //Console.WriteLine("Поток сервера:" + _Connect.ServerThread);
                if (conn.State != ConnectionState.Open) { conn.Open(); }
                using (DbCommand command = new DbCommand(Query, conn))
                {
                    if (!Parameters.Equals(null))
                    {
                        for (int i = 0; i < Parameters.Length; i++)
                        {
                            command.Parameters.AddWithValue(mc[i].ToString(), Parameters[i]);
                        }
                    }
                    try
                    {
                        ret = command.ExecuteScalar();

                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine("Поток сервера:" + _Connect.ServerThread);
                        //Console.WriteLine(ex);
                        //Console.WriteLine(command.CommandText);
                        //for (int i = 0; i < Parameters.Length; i++)
                        //{
                        //    Console.WriteLine(mc[i].ToString() + " "+ Parameters[i]);
                        //}
                        throw new Exception(ex.ToString());
                    }

                }
            }
            return ret;

        }
        /// <summary>
        /// Выполняет запрос с указанными параметрами
        /// </summary>
        /// <param name="Query">Запрос</param>
        /// <param name="Parameters">Массив Параметров (в том порядке, в каком они идут в строке запроса слева направо)</param>
        /// <returns></returns>
        public object ExecuteQuery(string Query, object[] Parameters = null)
        {
            object ret;
            if (Parameters == null) { Parameters = new object[] { }; }
            //Найдем имена параметров в запросе с помощью регулярного выражения
            Regex reg = new Regex("\\?\\w+", RegexOptions.IgnoreCase);
            MatchCollection mc = reg.Matches(Query);
            //Проверим, совпадаетли число параметров, которые нашли в запросе и количество параметров, которые передали
            if (mc.Count != Parameters.Length) { new Exception("Количество передаваемых параметров  (" + Parameters.Length.ToString() + ") не совпадает с количеством ожидаемых параметров (" + mc.Count.ToString() + ")"); }
            //Не уверен насчет такого использования подключения. Если подключение занято - может не сработать. Возможно, нужно клонировать подключение перед выполнением
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                //Console.WriteLine("Поток сервера:" + _Connect.ServerThread);
                if (conn.State != ConnectionState.Open) { conn.Open(); }
                using (MySqlCommand command = new MySqlCommand(Query, conn))
                {
                    if (!Parameters.Equals(null))
                    {
                        for (int i = 0; i < Parameters.Length; i++)
                        {
                            command.Parameters.AddWithValue(mc[i].ToString(), Parameters[i]);
                        }
                    }
                    // try
                    //{
                    //Console.WriteLine("===============П А Р А М Е Т Р Ы==================");
                    //for (int i = 0; i < Parameters.Length; i++)
                    //{
                    //    Console.WriteLine(mc[i].ToString() + " " + Parameters[i]);
                    //}
                    ret = command.ExecuteScalar();

                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.WriteLine("Поток сервера:" + _Connect.ServerThread);
                    //    Console.WriteLine(ex);
                    //    Console.WriteLine(command.CommandText);
                    //    for (int i = 0; i < Parameters.Length; i++)
                    //    {
                    //        Console.WriteLine(mc[i].ToString() + " " + Parameters[i]);
                    //    }
                    //    throw new Exception(ex.ToString());
                    //}

                }
            }
            return ret;

        }
        /// <summary>
        /// Сохраняет результат запроса, прости господи, в файл *facepalm*
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <param name="Query">Запрос</param>
        /// <param name="Parameters">Массив Параметров (в том порядке, в каком они идут в строке запроса слева направо)</param>
        /// <returns></returns>
        public bool SaveToFile(string path, string Query, object[] Parameters = null)
        {
            if (Parameters == null) { Parameters = new object[] { }; }
            //Найдем имена параметров в запросе с помощью регулярного выражения
            Regex reg = new Regex("\\?\\w+", RegexOptions.IgnoreCase);
            MatchCollection mc = reg.Matches(Query);
            //Проверим, совпадаетли число параметров, которые нашли в запросе и количество параметров, которые передали
            if (mc.Count != Parameters.Length) { new Exception("Количество передаваемых параметров  (" + Parameters.Length.ToString() + ") не совпадает с количеством ожидаемых параметров (" + mc.Count.ToString() + ")"); }
            //Не уверен насчет такого использования подключения. Если подключение занято - может не сработать. Возможно, нужно клонировать подключение перед выполнением
            using (MySqlConnection conn = new MySqlConnection(ConnectionString))
            {
                //Console.WriteLine("Поток сервера:" + _Connect.ServerThread);
                if (conn.State != ConnectionState.Open) { conn.Open(); }
                using (MySqlCommand command = new MySqlCommand(Query, conn))
                {
                    if (!Parameters.Equals(null))
                    {
                        for (int i = 0; i < Parameters.Length; i++)
                        {
                            command.Parameters.AddWithValue(mc[i].ToString(), Parameters[i]);
                        }
                    }
                    MySqlDataReader dr = command.ExecuteReader();
                    FileStream stream;
                    BinaryWriter writer;
                    int bufferSize = 100;
                    byte[] outByte = new byte[bufferSize];
                    long retval;
                    long startIndex = 0;
                    while (dr.Read())
                    {
                        if (dr[0] != DBNull.Value) //Если не -1 - значит время закачки в базу было позже сохраненной версии у пользователя и надо скачать новый шаблон
                        {
                            //System.IO.File.WriteAllBytes(FilePath, (byte[])TemplateReader[0]);
                            stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                            writer = new BinaryWriter(stream);
                            startIndex = 0;
                            retval = dr.GetBytes(0, startIndex, outByte, 0, bufferSize);
                            while (retval == bufferSize)
                            {
                                writer.Write(outByte);
                                writer.Flush();
                                startIndex += bufferSize;
                                retval = dr.GetBytes(0, startIndex, outByte, 0, bufferSize);
                            }
                            writer.Write(outByte, 0, (int)retval);
                            writer.Flush();
                            writer.Close();
                            stream.Close();
                        }
                    }
                }
            }
            return true;
        }


       
        // Use C# destructor syntax for finalization code.
        ~DbConnection()
        {
            // Simply call Dispose(false).
            Dispose(false);
        }
    }
        /// <summary>
        /// Класс команды
        /// </summary>
        public class DbCommand : IDbCommand, IDisposable
    {
        private bool disposed = false;
        static MySqlCommand _command;
        DbDataReader _datareader = new DbDataReader();
        public DbCommand.DbParameters Parameters= new DbCommand.DbParameters();
        public class DbParameters : IDataParameterCollection
        {
            //private MySqlParameterCollection ParameterCollection;

            public object this[int index]
            {
                get
                {
                    return _command.Parameters[index];
                    throw new NotImplementedException();
                }

                set
                {
                    _command.Parameters[index].Value = value;
                    throw new NotImplementedException();
                }
            }

            public object this[string parameterName]
            {
                get
                {
                    return _command.Parameters[parameterName];
                    throw new NotImplementedException();
                }

                set
                {
                    _command.Parameters[parameterName].Value = value;
                    throw new NotImplementedException();
                }
            }

            public int Count
            {
                get
                {
                    return _command.Parameters.Count;
                    throw new NotImplementedException();
                }
            }

            public bool IsFixedSize
            {
                get
                {
                    return _command.Parameters.IsFixedSize;
                    throw new NotImplementedException();
                }
            }

            public bool IsReadOnly
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public bool IsSynchronized
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public object SyncRoot
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public int Add(object value)
            {
                _command.Parameters.Add(value);
                throw new NotImplementedException();
            }

            public void Clear()
            {
                _command.Parameters.Clear();
                //throw new NotImplementedException();
            }

            public bool Contains(object value)
            {
                throw new NotImplementedException();
            }

            public bool Contains(string parameterName)
            {
                throw new NotImplementedException();
            }

            public void CopyTo(Array array, int index)
            {
                throw new NotImplementedException();
            }

            public IEnumerator GetEnumerator()
            {
                throw new NotImplementedException();
            }

            public int IndexOf(object value)
            {
                return _command.Parameters.IndexOf(value);
                throw new NotImplementedException();
            }

            public int IndexOf(string parameterName)
            {
                return _command.Parameters.IndexOf(parameterName);
                throw new NotImplementedException();
            }

            public void Insert(int index, object value)
            {

                throw new NotImplementedException();
            }

            public void Remove(object value)
            {
                _command.Parameters.Remove(value);
                //throw new NotImplementedException();
            }

            public void RemoveAt(int index)
            {
                _command.Parameters.RemoveAt(index);
               // throw new NotImplementedException();
            }

            public void RemoveAt(string parameterName)
            {
                _command.Parameters.RemoveAt(parameterName);
                //ParameterCollection.RemoveAt(parameterName);
                //throw new NotImplementedException();
            }
            public void AddWithValue(string ParametrName, object Value)
            {
                //ParameterCollection.AddWithValue(ParametrName, Value);
                _command.Parameters.AddWithValue(ParametrName, Value);
            }
        }
        //Конструкторы
        /// <summary>
        /// Создает новый эксземпляр команды
        /// </summary>
        public DbCommand()
        {
            _command = new MySqlCommand();
        }
        /// <summary>
        /// Создает новый эксземпляр команды
        /// </summary>
        /// <param name="cmdText">Текст команды</param>
        public DbCommand(string cmdText)
        {
            _command = new MySqlCommand(cmdText);
        }
        /// <summary>
        /// Создает новый эксземпляр команды
        /// </summary>
        /// <param name="cmdText">Текст команды</param>
        /// <param name="connect">Соединение</param>
        public DbCommand(string cmdText, DbConnection connect)
        {
            _command = new MySqlCommand(cmdText, connect.GetConnection());
        }
        /// <summary>
        /// Выполняет команду
        /// </summary>
        /// <returns>Результат (первый столбец первой строки)</returns>
        public object ExecuteScalar()
        {
            if (_command.Connection.State != ConnectionState.Open) { _command.Connection.Open();}
            //try
            //{
            return _command.ExecuteScalar();
            //}
            //catch (Exception ex)
            //{
            //    return 0;
            //}
        }
        /// <summary>
        /// Выполняет команду
        /// </summary>
        public void ExecuteNonQuery()
        {
            _command.ExecuteNonQuery();
        }
        /// <summary>
        /// Выполняет команду
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public DbDataReader ExecuteReader()
        {
            _datareader.Reader= _command.ExecuteReader();
            return _datareader;
        }
        ///// <summary>
        ///// Параметры команды
        ///// </summary>
        //public DbParameters Parameters
        //{
        //    get {return _cmd.Parameters; }
        //}
        /// <summary>
        /// Задает или получает текст команды
        /// </summary>
        public string CommandText
        {
            get { return _command.CommandText; }
            set { _command.CommandText = value; }
        }
        /// <summary>
        /// Задает или получает соединение
        /// </summary>
        public DbConnection Connection
        {
            //get { return _cmd.Connection; }
            set { _command.Connection =value.GetConnection();}
        }

        IDbConnection IDbCommand.Connection
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public IDbTransaction Transaction
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int CommandTimeout
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public CommandType CommandType
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        IDataParameterCollection IDbCommand.Parameters
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public UpdateRowSource UpdatedRowSource
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Метод явного преобразования
        /// </summary>
        /// <param name="v">Connection</param>
        public static explicit operator MySqlCommand(DbCommand cmd)
        { 
            return _command;
            throw new NotImplementedException();
        }

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                disposed = true;
            }
        }

        public void Prepare()
        {
            throw new NotImplementedException();
        }

        public void Cancel()
        {
            throw new NotImplementedException();
        }

        public IDbDataParameter CreateParameter()
        {
            throw new NotImplementedException();
        }

        int IDbCommand.ExecuteNonQuery()
        {
            throw new NotImplementedException();
        }

        IDataReader IDbCommand.ExecuteReader()
        {
            throw new NotImplementedException();
        }

        public IDataReader ExecuteReader(CommandBehavior behavior)
        {
            throw new NotImplementedException();
        }

        // Use C# destructor syntax for finalization code.
        ~DbCommand()
        {
            // Simply call Dispose(false).
            Dispose(false);
        }
    }
    /// <summary>
    /// Класс адаптера
    /// </summary>
    public class DbDataAdapter : IDisposable
    {
        private bool disposed = false;
        private MySqlDataAdapter _adapter;
        private DbCommand _selectCommand;
        /// <summary>
        /// Создает адаптер
        /// </summary>
        public DbDataAdapter()
        {
            _adapter = new MySqlDataAdapter();
            _selectCommand = new DbCommand();
        }
        /// <summary>
        /// Создает адаптер
        /// </summary>
        /// <param name="Command">Команда адаптера</param>
        public DbDataAdapter(DbCommand Command)
        {
            _adapter = new MySqlDataAdapter();
            _selectCommand = Command;
            _adapter.SelectCommand = (MySqlCommand)_selectCommand;
        }
        public DbCommand SelectCommand
        {
            get { return _selectCommand; }
            set { _selectCommand = value;  _adapter.SelectCommand =(MySqlCommand)_selectCommand;}
        }
        /// <summary>
        /// Заполняет таблицу данными
        /// </summary>
        /// <param name="dtData">Таблица</param>
        public int Fill(DataTable dtData)
        {
            //try
            //{
                return _adapter.Fill(dtData);
            //}
            //catch(Exception ex)
            //{
            //    return -1;
            //}
        }

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                disposed = true;
            }
        }

        // Use C# destructor syntax for finalization code.
        ~DbDataAdapter()
        {
            // Simply call Dispose(false).
            Dispose(false);
        }
    }
    /// <summary>
    /// Класс Ридера
    /// </summary>
    public class DbDataReader: IDataReader, IConvertible
    {
        MySqlDataReader _drDataReader;
        public DbDataReader()
        {
            _drDataReader = null;
        }
        public MySqlDataReader Reader
        {
            get { return _drDataReader; }
            set { _drDataReader = value; }
        }
        /// <summary>
        /// Читает строку и переводит указатель на следующую строку
        /// </summary>
        /// <returns></returns>
        public bool Read()
        {
            return _drDataReader.Read();
        }
        
        public void Close()
        {
            _drDataReader.Close();
            //throw new NotImplementedException();
        }

        public DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }

        public bool NextResult()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _drDataReader.Dispose();
            //throw new NotImplementedException();
        }

        public string GetName(int i)
        {
            return _drDataReader.GetName(i);
            //throw new NotImplementedException();
        }

        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public object GetValue(int i)
        {
            return _drDataReader.GetValue(i);
            throw new NotImplementedException();
        }
        /// <summary>
        /// возвращает количество полей
        /// </summary>
        /// <returns></returns>
        public int FieldCount
        {
            get
            {
                return _drDataReader.FieldCount;
            }
        }
        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            return _drDataReader.GetBytes(i, fieldOffset, buffer, bufferoffset, length);
            throw new NotImplementedException();
        }

        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        public int GetInt32(int i)
        {
            return _drDataReader.GetInt32(i);
            throw new NotImplementedException();
        }
        int IConvertible.ToInt32(IFormatProvider provider)
        {
            return Convert.ToInt32(_drDataReader);
        }

        public long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        public float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public string GetString(int i)
        {
            return _drDataReader.GetString(i);
            throw new NotImplementedException();
        }

        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }

        public bool IsDBNull(int i)
        {
            throw new NotImplementedException();
        }

        public TypeCode GetTypeCode()
        {
            throw new NotImplementedException();
        }

        public bool ToBoolean(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public char ToChar(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public sbyte ToSByte(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public byte ToByte(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public short ToInt16(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public ushort ToUInt16(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public int ToInt32(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public uint ToUInt32(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public long ToInt64(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public ulong ToUInt64(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public float ToSingle(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public double ToDouble(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public decimal ToDecimal(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public DateTime ToDateTime(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public string ToString(IFormatProvider provider)
        {
            return _drDataReader.ToString();
            throw new NotImplementedException();
        }

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            throw new NotImplementedException();
        }
        public object this[string name]
        {
            get
            {
                return _drDataReader[name];
                throw new NotImplementedException();
            }
        }

        public object this[int i]
        {
            get
            {
                return _drDataReader[i];
                throw new NotImplementedException();
            }
        }
        /// <summary>
        /// Возвращает true если есть непрочитанные строки
        /// </summary>
        public bool HasRows
        {
            get { return _drDataReader.HasRows; }
        }

        public int Depth
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsClosed
        {
            get
            {
                return _drDataReader.IsClosed;
                //throw new NotImplementedException();
            }
        }

        public int RecordsAffected
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
    public class DbConnectionStringBuilder
    {
        MySqlConnectionStringBuilder _connectionStringBuilder;
        public DbConnectionStringBuilder()
        {
            _connectionStringBuilder = new MySqlConnectionStringBuilder();
        }
        /// <summary>
        /// Задает или получает имя сервера для подключения
        /// </summary>
        public string Server
        {
            get { return _connectionStringBuilder.Server; }
            set { _connectionStringBuilder.Server = value; }
        }
        /// <summary>
        /// Здадает или получает значение порта для подключения к серверу
        /// </summary>
        public uint Port
        {
            get { return _connectionStringBuilder.Port; }
            set { _connectionStringBuilder.Port = value; }
        }
        /// <summary>
        /// Здадает или получает значение Имени пользователя для подключения к серверу
        /// </summary>
        public string UserID
        {
            get { return _connectionStringBuilder.UserID; }
            set { _connectionStringBuilder.UserID = value; }
        }
        /// <summary>
        /// Здадает или получает значение имени базы данных для подключения к серверу
        /// </summary>
        public string Database
        {
            get { return _connectionStringBuilder.Database; }
            set { _connectionStringBuilder.Database = value; }
        }
        /// <summary>
        /// Возвращает строку для подключения к базе данных
        /// </summary>
        public string ConnectionString
        {
            get { return _connectionStringBuilder.ConnectionString; }
            set { _connectionStringBuilder.ConnectionString = value; }
        }
        /// <summary>
        /// Возвращает или задает кодировку подключения к базе данных
        /// </summary>
        public string CharacterSet
        {
            get { return _connectionStringBuilder.CharacterSet; }
            set { _connectionStringBuilder.CharacterSet = value; }
        }
        /// <summary>
        /// Возвращает или задает значение пароля для подключения к базе данных
        /// </summary>
        public string Password
        {
            get { return _connectionStringBuilder.Password; }
            set { _connectionStringBuilder.Password = value; }
        }
    
    }
    public class MyDbParameter : DbParameter, IDbDataParameter, IDataParameter
    {
        MySqlParameter par;

        public MyDbParameter()
        {
            par = new MySqlParameter();
        }
        public override DbType DbType
        {
            get
            {
                return par.DbType;
                throw new NotImplementedException();
            }

            set
            {
                par.DbType = value;
                //throw new NotImplementedException();
            }
        }

        public override ParameterDirection Direction
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool IsNullable
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override string ParameterName
        {
            get
            {
                return par.ParameterName;
                throw new NotImplementedException();
            }

            set
            {
                par.ParameterName = value;
                //throw new NotImplementedException();
            }
        }

        public override int Size
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override string SourceColumn
        {
            get
            {
                return par.SourceColumn;
                throw new NotImplementedException();
            }

            set
            {
                par.SourceColumn = value;
                //throw new NotImplementedException();
            }
        }

        public override bool SourceColumnNullMapping
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override object Value
        {
            get
            {
                return par.Value;
                throw new NotImplementedException();
            }

            set
            {
                par.Value = value;
                //throw new NotImplementedException();
            }
        }

        public override void ResetDbType()
        {
            throw new NotImplementedException();
        }
        public override DataRowVersion SourceVersion
        {
            get
            {
                return par.SourceVersion;
            }

            set
            {
                par.SourceVersion = value;
            }
        }
    }
    /// <summary>
    /// Класс параметров для команды 
    /// </summary>
    public class DbParameters : ArrayList
    {
        public ArrayList parameters;
        public DbParameters()
        {
            parameters = new ArrayList();
        }
        public void AddWithValue(string ParametrName, object Value)
        {
            MyDbParameter par = new MyDbParameter();
            par.ParameterName = ParametrName;
            par.Value = Value;
            parameters.Add(par);

        }
        public override int Count
        {
            get
            {
                return parameters.Count;
            }
        }
        public override object this[int index]
        {
            get
            {
                return parameters[index];
                throw new NotImplementedException();
            }

            set
            {
                parameters[index] = value;
                throw new NotImplementedException();
            }
        }
    }
}
