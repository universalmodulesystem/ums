﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DAO
{
    public static class CommonSettings
    {
         /// <summary>
         /// Адрес сервера БД
         /// </summary>
        public static string DB_url { get; set; }
        /// <summary>
        /// Рутовый пользователь
        /// </summary>
        public static string DB_root { get; set; }
        /// <summary>
        /// Пароль Рутового пользователя
        /// </summary>
        public static string DB_root_pass { get; set; }
        /// <summary>
        /// Название БД с настройками
        /// </summary>
        public static string DB_name { get; set; }      
    }
}
