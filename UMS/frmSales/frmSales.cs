﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LLayer;

namespace UMS
{
    public partial class frmSales : Form
    {

        Sale sale;
        Good g;
        public frmSales()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Настравивает отображение LissView с продажами
        /// </summary>
        private void ConfigureListView()
        {
            lvSale.View = View.Details;
            lvSale.Columns.Add("Название", 500, HorizontalAlignment.Left);
            lvSale.Columns.Add("Кол-во", 100, HorizontalAlignment.Left);
            lvSale.Columns.Add("Сумма", 100, HorizontalAlignment.Left);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
            ConfigureListView();

        }
        void ShowTotal(object sender, PropertyChangedEventArgs e)
        {
            if (!Equals(sale, null))
            {
                lblItogo.Text = sale.Total.ToString();
            }
            else
            {
                lblItogo.Text = "0.00";
            }
        }

        private void txtBarCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtBarCode_KeyDown(object sender, KeyEventArgs e)
        {
          
            switch(e.KeyCode)
            {
                case Keys.Enter:
                    {
                        SendKeys.Send("{TAB}");
                        break;
                    }
            }
        }

        private void txtBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
  
        }
        /// <summary>
        /// Загружает товар по штрих-коду
        /// </summary>
        /// <param name="barCode"></param>
        public void LoadGood(string barCode)
        {
            try
            {
                g = Good.GetGood(barCode);

                if (!Equals(g, null))
                {
                    numQuantity.Maximum = g.amount;
                    numQuantity.Value = 1;
                    lblMaxQuantity.Text = numQuantity.Maximum.ToString();
                    numQuantity.Focus();
                }
                else
                {
                    g = null;
                    MessageBox.Show("Товар с таким штрикодом не найден!", "Нет товара", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtBarCode.Text = "";
                    txtBarCode.Focus();

                }
            }
            catch(Exception e)
            {
                Errors.WriteError(e);
            }
            finally
            {
                //g = null;
            }
        }
        /// <summary>
        /// Добавляет товар в продажу
        /// </summary>
        public void AddToSale()
        {
            if (!Equals(g, null))
            {
                if (Equals(sale, null))
                {
                    sale = new Sale();
                    sale.TotalChanged += new PropertyChangedEventHandler(ShowTotal);
                }
                sale.AddGood(g, Convert.ToInt32(numQuantity.Value));
                DataTable goods = sale.GetGoods();
                double total = 0;
                lvSale.Items.Clear();
                
                foreach (DataRow r in goods.Rows)
                {
                    string[] ar = Array.ConvertAll<object, string>(r.ItemArray, p => p.ToString());
                    lvSale.Items.Add(new ListViewItem(ar));
                    //lvSale.Items.Add(new ListViewItem(new[] { r["good_name"].ToString(), r["Quantity"].ToString(), r["total_price"].ToString() }));
                    //lvSale.Items.Add(new ListViewItem(new[] { "sssss", "23","asasa" }));
                    total += Convert.ToDouble(r["total_price"]);
                }
                lblItogo.Text = total.ToString();
            }
            else
            {
                txtBarCode.Focus();
            }
        }

        private void numQuantity_KeyDown(object sender, KeyEventArgs e)
        {
            NumericUpDown t = (NumericUpDown)sender;
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        
                        AddToSale();
                        g = null;
                        txtBarCode.Text = "";
                        numQuantity.Value = 0;
                        lblMaxQuantity.Text = "";
                        break;
                    }
            }
        }

        private void numQuantity_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown n = (NumericUpDown)sender;
            if (!Equals(g, null))
            {
                // = Convert.ToInt32(n.Value);
            }
        }

        private void txtBarCode_Leave(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (t.Text != "")
            {
                LoadGood(t.Text);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (!Equals(sale, null) && sale.GetGoods().Rows.Count > 0)
            {
                sale.ExitSale();
                sale = null;
                lvSale.Clear();
                ConfigureListView();
                txtBarCode.Focus();
            }
        }

        private void btnSale_Click(object sender, EventArgs e)
        {
            if (!Equals(sale, null) && sale.GetGoods().Rows.Count>0)
            {
                sale.CloseSale();
                sale = null;
                lvSale.Clear();
                ConfigureListView();
                txtBarCode.Focus();
            }
        }
        private void Form_closing(object sender, FormClosingEventArgs e)
        {
            btnExitSale.PerformClick();
        }
    }
}
