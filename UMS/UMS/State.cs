﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace UMS
{
    /// <summary>
    /// Класс отображения информационного окна состояния
    /// </summary>
    public class State
    {
        frmState fState; //Форма, в которой отображается текущее состояние
        Thread StatusThread; //Поток для формы
        Form parent;
        string _Status;
        public State()
        {

        }

        public State(Form Parent)
        {
            parent = Parent;
        }
        /// <summary>
        /// Получает или задает текст статуса
        /// </summary>
        public string Status
        {
            get
            {
                return _Status;
            }

            set
            {

                _Status = value;
                while (!fState.IsLoad)
                { Thread.Sleep(100); }
                fState.Invoke(new MethodInvoker(() => { fState.StatusText=_Status; }));
            }
        }
        /// <summary>
        /// Получает или задает текст дополнительного статуса
        /// </summary>
        public string StatusInfo
        {
            get
            {
                return fState.lblState2.Text;
            }

            set
            {

                _Status = value;
                while (!fState.IsLoad)
                { Thread.Sleep(100); }
                fState.Invoke(new MethodInvoker(() => { fState.lblState2.Text = value; fState.lblState2.Visible = true; }));
            }
        }
        /// <summary>
        /// Получает или задает прогресс
        /// </summary>
        public int Progress
        {
            get
            {
                return fState.prbProgress.Value;
            }

            set
            {
                while (!fState.IsLoad)
                { Thread.Sleep(100); }
                fState.Invoke(new MethodInvoker(() => { fState.prbProgress.Value = value; fState.prbProgress.Visible = true; }));
            }
        }
        /// <summary>
        /// Получает или задает максимальной значение прогрес-бара
        /// </summary>
        public int MaxProgressValue
        {
            get
            {
                return fState.prbProgress.Maximum;
            }

            set
            {
                try
                {
                    while (!fState.IsLoad)
                    { Thread.Sleep(100); }
                    fState.Invoke(new MethodInvoker(() => { fState.prbProgress.Maximum = value; }));
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        /// <summary>
        /// Показывает форму состояния с заданным текстом
        /// </summary>
        /// <param name="Text">Текст на форме</param>
        public bool ShowState(string Text="Загрузка данных...")
        {
            fState = new frmState();
            if (Text.Length > 0)
            {
                fState.StatusText = Text;
            }
            try
            {
                StatusThread = new Thread(new ThreadStart(ShowStateForm)); //Запуск потока с формой
                StatusThread.Start();
                return true;
            }
            catch (Exception ex)
            {
                Errors.WriteError(ex);
                return false;
            }
            
        }
        /// <summary>
        /// Показывает форму. Приватный метод для вызова из нового потока
        /// </summary>
        private  void ShowStateForm()
        {
            fState.ShowDialog();
        }
        /// <summary>
        /// Скрывает форму с данными
        /// </summary>
        public void CloseState()
        {
            //fState.HideForm();
            fState.DialogResult = DialogResult.Cancel;
            //if (fState != null)
            //{
            //    //if (fState.Visible)
            //    //{
            //        fState.Invoke(new MethodInvoker(() => { fState.CloseStatus(); }));
            //   // }
            //}
            try {
                //StatusThread.Abort();
               // fState.Dispose();
            }
            catch(Exception)
            {
                //Сюда попадаем по Abort и ничего не делаем, все норм
            }
            Terminal.MainForm.Activate();
            
            if (parent!=null)
            {
                parent.Activate();
                parent.Focus();
            }
        }
        public void StatusText(string Status)
        {

        }
    }
}
