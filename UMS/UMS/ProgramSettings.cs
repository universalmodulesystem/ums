﻿using System;
using DAO;
using LLayer;
using Microsoft.Win32;
using System.Data;

namespace UMS
{
    /// <summary>
    /// Класс настроек программы
    /// </summary>
    public static class ProgramSettings
    {
        static string fullRegistryPath = "";
        static string LastUser = "LastUserId";
        /// <summary>
        /// Идентификатор последнего работавшего пользователя
        /// </summary>
        public static Int16 LastUserId { get; set; } = 0;
        /// <summary>
        /// Читает настройки программы из реестра. Создает ветвь с дефолтными настройками, если не существует
        /// </summary>
        /// <param name="RegistryKey">Ветвь реестра с настройками программы</param>
        public static void ReadSettingsFromRegistry(string RegistryKey)
        {
            fullRegistryPath = "Software\\" + RegistryKey;
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(fullRegistryPath, true);
            if (rk == null)
            {
                Registry.CurrentUser.CreateSubKey(fullRegistryPath);
                ReadSettingsFromRegistry(RegistryKey);
            }
            string DB_url = "db_url";
            if (rk.GetValue(DB_url) == null) { rk.SetValue(DB_url, "localhost"); }
            DAO.CommonSettings.DB_url = rk.GetValue(DB_url).ToString();

            string DB_root = "db_root";
            if (rk.GetValue(DB_root) == null) { rk.SetValue(DB_root, "root"); }
            DAO.CommonSettings.DB_root = rk.GetValue(DB_root).ToString();

            string DB_root_pass = "db_root_pass";
            if (rk.GetValue(DB_root_pass) == null) { rk.SetValue(DB_root_pass, ""); }
            DAO.CommonSettings.DB_root_pass = rk.GetValue(DB_root_pass).ToString();

            string DB_name = "db_name";
            if (rk.GetValue(DB_name) == null) { rk.SetValue(DB_name, "ums"); }
            DAO.CommonSettings.DB_name = rk.GetValue(DB_name).ToString();

           
            if (rk.GetValue(LastUser) == null) { rk.SetValue(LastUser, 0, RegistryValueKind.DWord); }
            Int16 lu = 0;
            Int16.TryParse(rk.GetValue(LastUser).ToString(), out lu);
            LastUserId = lu;
            //Добавим строку подключения к базе программы
            DataBase.SetUMSRootConnectionString(CommonSettings.DB_url, 3306, CommonSettings.DB_name, CommonSettings.DB_root, "");
        }
        /// <summary>
        /// Прописывает в реестр ID последнего входившего пользователя
        /// </summary>
        /// <param name="UserId"></param>
        public static void SetLastUserID(int UserId)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(fullRegistryPath, true);
            rk.SetValue(LastUser, UserId, RegistryValueKind.DWord);
        }
        
        /// <summary>
        /// Добавляет базы данных в словарик
        /// </summary>
        /// <param name="db_user_name"></param>
        public static void AddDataBases(string db_user_name)
        {
            //К остальным базам от юзера
            DataTable dtBases = DataBase.GetDataBases();
            foreach (DataRow dr in dtBases.Rows)
            {
                DbConnectionStringBuilder ConnectionStringBuilder = new DbConnectionStringBuilder();
                ConnectionStringBuilder.Server = dr["url"].ToString();
                ConnectionStringBuilder.Port = Convert.ToUInt16(dr["port"]);
                ConnectionStringBuilder.UserID = db_user_name;
                ConnectionStringBuilder.Database = dr["dbname"].ToString();
                Session.DataBases.Add(dr["name"].ToString(), new DataBase(ConnectionStringBuilder.ConnectionString + ";Convert Zero Datetime=True"));
            }
        }
    }
}
