﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LLayer;

namespace UMS
{
    /// <summary>
    /// Класс работы с меню
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// Получает все пункты меню, отсортированные по столбцу стортировки. Все подпункты помещены в конец таблицы.
        /// </summary>
        /// <param name="ShowNonActive">true - показывать неактивные</param>
        /// <returns></returns>
        public static DataTable GetMainMenu(bool ShowNonActive = false)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetMainMenu(?ShowNonActive)",  new object[] { ShowNonActive ? 1 : 0 });
        }
        /// <summary>
        /// Получает все пункты меню
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllMenuItems()
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetAllMenuItems()");
        }
    }
}
