﻿using System;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using DAO;
using LLayer;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Runtime.InteropServices;
using System.Linq;

namespace UMS
{

    public static class Terminal
    {
        private static Mutex m_instance;
        private const string m_appName = "UMS";
        public static Main MainForm; //Главная форма
        public static string sUpdatePath = "D:\\UMS\\"; //Папка с Update
        private static string sSettingsFileName = "main.ini";
        public static Session Session = new Session(); //Класс текущей сессии
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        /// <param name="args">Аргументы командной строки</param>
        [STAThread]
        
        static void Main(string[] args)
        {
            try
            {
                //Эта дичь для проверки уже запущенного экземпляра Терминала-2
                bool tryCreateNewApp;//Признак того, что таки надо создать новый экземпляр терминала
                m_instance = new Mutex(true, m_appName, out tryCreateNewApp);
                if (!tryCreateNewApp)
                {
                    RestoreWindowByName(m_appName);
                    RestoreWindow(m_appName);
                    return;
                }

                bool NotCheck = false;//Флаг "Не проверять обнолвение". Проверяется аргумент "n"
                if (args.Length > 0)
                { 
                    Global.args = args;
                    NotCheck = args[0] == "n";
                }
                //Проверим обновление
                //if (CheckUpdate() && !NotCheck)
                //{
                //    ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe");
                //    startInfo.WindowStyle = ProcessWindowStyle.Normal;    
                //    startInfo.Arguments = "/c rename.bat";
                //    Process.Start(startInfo);
                //    //Process.Start("cmd.exe");
                //}
                //else
                //{
                    //File.Delete("rename.bat");//Удалим batник обновления чтобы не маячил
                    ProgramSettings.ReadSettingsFromRegistry("UMS"); //Читаем настройки 
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(MainForm = new Main()); //Создаем и Запускаем главную MDI-форму
                //}
            }
            catch(Exception ex)
            {
                MessageBox.Show("Критическая ошибка: \n" + ex.ToString(), "Фаталити", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                File.AppendAllText("StartUpError.log", ex.ToString());
                
                //Errors.WriteErrorToDatabase(ex.ToString());
                //Logs.AddLog("Критическая ошибка");
            }
        }
        /// <summary>
        /// Проверяет обновление программы
        /// </summary>
        /// <returns>TRUE - если есть новая версия, иначе FALSE</returns>
        public static bool CheckUpdate()
        {
            if (System.IO.File.Exists(sUpdatePath + System.IO.Path.GetFileName(Application.ExecutablePath)))
            {
                bool Update = false; //Флаг необходимостиобновления, то есть зпуска batника
                string batText=""; //Текст bat-файла, который будет обновлять
                string exeText = "\n start UMS.exe ";//По итогам либо запускаем терминал сразу, либо, если exeшник обновл
                string[] UpdateFiles = Directory.GetFiles(sUpdatePath); //Все файлы в папке с апдейтом
                string[] CurrentFiles = Directory.GetFiles(Environment.CurrentDirectory); //Файлы в текущей директории
                foreach (string file in UpdateFiles)
                {
                    bool NotExist = true; 
                    foreach (string CurrentFile in CurrentFiles)
                    {
                        if (Path.GetFileName(file)== Path.GetFileName(CurrentFile))
                        {
                            NotExist = false;
                            break;
                        }
                    }

                    if (NotExist) //Если у нас нет файла - просто скопируем его
                    {
                        System.IO.File.Copy(sUpdatePath + System.IO.Path.GetFileName(file), System.IO.Path.GetFileName(file), true); //Скопируем
                    }
                    //Если файл не имеет версии - проверяем дату изменения
                    if (FileVersionInfo.GetVersionInfo(System.IO.Path.GetFileName(file)).FileVersion == null)
                    {
                        if(File.GetCreationTime(sUpdatePath+System.IO.Path.GetFileName(file))> File.GetCreationTime(Environment.CurrentDirectory + "\\" + Path.GetFileName(file))|| File.GetLastWriteTime(sUpdatePath + System.IO.Path.GetFileName(file)) > File.GetLastWriteTime(Environment.CurrentDirectory + "\\" + Path.GetFileName(file)))
                        {
                            System.IO.File.Copy(sUpdatePath + System.IO.Path.GetFileName(file), Path.GetFileName(file) + ".new", true);
                            batText += " Del " + Path.GetFileName(file) + "\n " + " Ren " + Path.GetFileName(file) + ".new " + Path.GetFileName(file) + " \n";
                            Update = true;
                        }
                    }
                    else  //Иначе Сравним версию файла, который в папке с обновлением и у нас
                    {
                      
                        if (new Version(FileVersionInfo.GetVersionInfo(sUpdatePath + System.IO.Path.GetFileName(file)).FileVersion) > new Version(FileVersionInfo.GetVersionInfo(Environment.CurrentDirectory + "\\" + System.IO.Path.GetFileName(file)).FileVersion))
                        {
                            System.IO.File.Copy(sUpdatePath + System.IO.Path.GetFileName(file), Path.GetFileName(file) + ".new", true);
                            //Если exe-шник - все немного сложнее
                            if (Path.GetFileName(Application.ExecutablePath) == Path.GetFileName(file))
                            {
                                //timeout неработает в XP
                                //exeText = ":delterm \n  IF EXIST UMS.exe.new Del UMS.exe  \n IF EXIST UMS.exe timeout /t 1 \n IF EXIST UMS.exe GOTO delterm \n Ren UMS.exe.new UMS.exe \n start UMS.exe \n Exit";
                                //Используем Ping -n 1 127.0.0.1 > nul в качестве timeout
                                exeText = ":delterm \n  IF EXIST UMS.exe.new Del UMS.exe  \n IF EXIST UMS.exe ping -n 1 127.0.0.1 > nul \n IF EXIST UMS.exe GOTO delterm \n Ren UMS.exe.new UMS.exe \n start UMS.exe \n Exit";
                            }
                            else //Иначе копируем файл и добавляем команду на удаление старого и переименование нового переименование
                            {
                                //batText += "\n  Del " + Path.GetFileName(file) + "\n " + " Ren " + Path.GetFileName(file) + ".new " + Path.GetFileName(file) + " \n";
                                batText += ":del"+ Path.GetFileNameWithoutExtension(file) + " \n IF EXIST " + Path.GetFileName(file) + ".new Del " + Path.GetFileName(file) + "  \n IF EXIST " + Path.GetFileName(file) + " ping -n 1 127.0.0.1 > nul \n IF EXIST " + Path.GetFileName(file) + " GOTO del"+ Path.GetFileNameWithoutExtension(file) + " \n Ren " + Path.GetFileName(file) + ".new " + Path.GetFileName(file) + " \n";
                            }
                            Update = true;
                        }
                    }
                }
                if (Update)
                {
                    batText += "\n " + exeText;
                    string batFileName = "rename.bat";
                    if (File.Exists(batFileName)) { File.Delete(batFileName); }
                    File.AppendAllText(batFileName, batText);
                    return true;
                }

                //if (new Version(FileVersionInfo.GetVersionInfo(sUpdatePath+ System.IO.Path.GetFileName(Application.ExecutablePath)).FileVersion) > Assembly.GetEntryAssembly().GetName().Version)
                //{
                //    System.IO.File.Copy(sUpdatePath+ System.IO.Path.GetFileName(Application.ExecutablePath), "UMS.exe.new"); //Скопируем
                //    //Создадим bat-ник для переименования
                //    if (!File.Exists("rename.bat"))
                //    {
                //        File.AppendAllText("rename.bat", ":delterm \n  IF EXIST UMS.exe.new Del UMS.exe  \n IF EXIST UMS.exe timeout /t 1 \n IF EXIST UMS.exe GOTO delterm \n Ren UMS.exe.new UMS.exe \n start UMS.exe \n Exit");
                //    }
                //    return true;
                //}
            }
            return false;
        }

        [DllImport("user32.dll")]
        static extern IntPtr FindWindowA(bool ClassName, string WindowName);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int Command);
        private static bool RestoreWindow(string processName)
        {
            bool result = false;
            Process[] proc = Process.GetProcessesByName(processName);
            if (proc.Length > 0)
                result = ShowWindow(proc[0].MainWindowHandle, 9);

            return result;
        }
        private static bool RestoreWindowByName(string wnd)
        {
            bool result = false;
            Process proc = Process.GetProcesses().First(p => p.ProcessName.Contains(wnd));
            if (proc != null)
                result = ShowWindow(proc.MainWindowHandle, 6);
                result = ShowWindow(proc.MainWindowHandle, 9);
                result = ShowWindow(proc.MainWindowHandle, 5);
            return result;
        }
    }
}
