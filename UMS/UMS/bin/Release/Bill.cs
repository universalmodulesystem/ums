﻿using System.Data;
using DAO;
using System;

namespace LLayer
{
    /// <summary>
    /// Класс работы со счетами
    /// </summary>
    public class Bill
    {
        DbConnection _Connection;
        public Bill(DbConnection Connection)
        {
            _Connection = Connection;
        }
        #region Методы
        #endregion
        #region Статичные методы
        /// <summary>
        /// Получает счета в заданном промежутке дат
        /// </summary>
        /// <param name="startDate">Начальная дата</param>
        /// <param name="endDate">Конечная дата</param>
        /// <returns></returns>
        public static DataTable GetBills(DateTime startDate, DateTime endDate)
        {
            return Session.DataBases[Session.DataBasesName.EMS].FillDataTableByQuery("CALL GetBillsByPeriod(?startDate, ?endDate)", new object[] { startDate, endDate });
        }
        /// <summary>
        /// Получает информацию о номере счета и дате
        /// </summary>
        /// <param name="billCode">Идентификатор счета</param>
        /// <returns></returns>
        public static string GetBillNumberAndDate(int billCode)
        {
            return Convert.ToString(Session.DataBases[Session.DataBasesName.EMS].ExecuteQuery("SELECT GetBillNumberAndDate(?billCode)", new object[] { billCode}));
        }
        /// <summary>
        /// Получает все отправления в счете
        /// </summary>
        /// <param name="BillCode">Код счета</param>
        /// <returns></returns>
        public static DataTable GetPostingsByBillCode(int BillCode)
        {
            return Session.DataBases[Session.DataBasesName.EMS].FillDataTableByQuery("CALL GetPostingsByBillCode(?BillCode)", new object[] { BillCode });
        }
        /// <summary>
        /// Получает комментарии к счету
        /// </summary>
        /// <param name="BillCode">Идентификатор счета</param>
        /// <returns></returns>
        public static string[] GetBillComments(int BillCode)
        {
            string Comments = Convert.ToString(Session.DataBases[Session.DataBasesName.EMS].ExecuteQuery("SELECT GetBillComments(?BillCode)", new object[] { BillCode }));
            return Comments.Split('\0');
        }
        public static void Update(int billCode)
        {

        }
        #endregion
    }
}
