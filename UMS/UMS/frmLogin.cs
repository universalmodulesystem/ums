﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using LLayer;

namespace UMS
{
    public partial class frmLogin : Form
    {
        DataTable dtUsers = new DataTable(); //Список пользователей
        bool ok = false;
        public bool Ok
        {
            get
            {
                return ok;
            }

            set
            {
                ok = value;
            }
        }
        public frmLogin()
        {
                InitializeComponent();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            FillOperators();
            SelectLastOperator();
            DialogResult = DialogResult.Cancel;
        }
        /// <summary>
        /// Заполнение списка операторов
        /// </summary>
        private void FillOperators()
        {
            try
            {
                int ComputerId= Computer.GetComputerId(Computer.GetComputerName());
                if (ComputerId > 0)
                {
                    Session.Computer.Id = ComputerId;
                }
                dtUsers = User.GetUsers(Session.Computer.Id);
                cmbOperator.DisplayMember = "FIO";
                cmbOperator.ValueMember = "id";
                cmbOperator.DataSource = dtUsers;
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        /// <summary>
        /// Нажатие ОК
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (cmbOperator.SelectedIndex>-1) 
            {
                //Если кто-то выбран в списке
                //iUserId = Convert.ToInt16(dtUsers.Rows[cmbOperator.SelectedIndex]["id"]);
                Session.User.UserId = Convert.ToInt16(cmbOperator.SelectedValue);
                if (User.CheckPassword(Session.User.UserId, txtPassword.Text))
                {
                    DialogResult = DialogResult.OK;
                    Ok = true;
                    btnCancel_Click(null, null);
                }
                else
                {
                    MessageBox.Show("Неправильный пароль", "Неверный пароль", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Выберите оператора из списка!", "Оператор не выбран", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbOperator.Focus();
            }
        }
        /// <summary>
        /// Нажатие Закрыть
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //public int UserId
        //{
        //    get { return iUserId; }
        //}

        private void cmbOperator_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.F2:
                    {
                        if (e.Control)
                        {
                            MessageBox.Show("Комрьютер " + Computer.GetComputerName() +" добавлен с идентификатором " +  Computer.AddComputerToBase(Computer.GetComputerName()));
                        }
                        break;
                    }
            }
        }

        private void cmbOperator_KeyDown(object sender, KeyEventArgs e)
        {
            if (cmbOperator.SelectedIndex != -1) //Если выбран - переходим на ОК
            {
                txtPassword.Focus();
            }
        }
        /// <summary>
        /// Выбирает в списке оператора, который записан в списке Smena
        /// </summary>
        private void SelectLastOperator()
        {
            cmbOperator.SelectedIndex = -1;
            if (ProgramSettings.LastUserId > 0)
            {
                foreach (DataRowView drv in cmbOperator.Items)
                {
                    if (Convert.ToInt16(drv[cmbOperator.ValueMember]) == ProgramSettings.LastUserId)
                    {
                        cmbOperator.SelectedIndex = cmbOperator.Items.IndexOf(drv);
                        return;
                    }
                }
            }
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
           
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        btnOk.Focus();
                        break;
                    }
            }
        }
    }
}
