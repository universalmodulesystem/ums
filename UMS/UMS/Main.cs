﻿
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
//using KKM;
using LLayer;
using System.Security.Policy;
using System.Collections.Generic;
using System.Diagnostics;

namespace UMS
{
    public partial class Main : Form
    {
        frmLogin fLogin; //Форма логина
        MenuStrip mnuStripMain; //Панель меню
        KassaSession kassaSession; //Работа с кассой
        List<Form> OpenedChildForms = new List<Form>(); //Список открытых дочерних форм
        //Chat c;//Почему бы и нет?
        public Main()
        {
            InitializeComponent();
            //Добавим подключение к программной базе от рута
            try
            {
                UseWaitCursor = true;
                Session.DataBases.Add(Session.DataBasesName.UMS, new DataBase(Settings.ConnectionSettings.UMSRootConnectionString));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Не удалось подключиться к серверу: " + ex.Message + "\r\n" + "Проверьте адрес подключения", "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
            }
            finally
            {
                UseWaitCursor = false;
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            this.Text = "UMS: IntEnd (ver. " + Application.ProductVersion.ToString() + ")";
            if (!Directory.Exists("forms")) //Существует ли папка для загрузки форм
            {
                Directory.CreateDirectory("forms");
            }
            this.StatusText = Computer.GetComputerName();
        }
        /// <summary>
        ///  Показывает окно входа в систему и делает неактивным главную форму
        /// </summary>
        /// <remarks>Это нужно т.к. нельзя вызывать  диалоговые окна из mdi форм</remarks>
        public void ShowLogin()
        {
            //если оператор еще не выбран, или до этого был выбран, но хотим поменять - покажем снова окно выбора
            if (Session.User.UCid==0 || MessageBox.Show("Сменить оператора?", "смена оператора", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            { 
                //Закроем все формы, чтобы не было проблем в случае использования формой текущего пользователя
                while (OpenedChildForms.Count>0)
                {
                    OpenedChildForms[0].Close();
                }
                fLogin = new frmLogin();
                //Так как дети mdi-контейнера не могут быть модальными приходится исхитряться:
                fLogin.FormClosed += new FormClosedEventHandler(fLogin_FormClosing); //Добавляем обработку на закрытие формы входа
                this.Enabled = false; //Делаем неактивной главную форму, ибо пока не вошли - ничего сделать не можем
                fLogin.Show(MdiParent); //Показываем форму входа
            }
        }
        /// <summary>
        /// Вызывается при первом отображении формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_Shown(object sender, EventArgs e)
        {
            //bool bShowLogin = false;
            ////Если в текущем дне уже была выбрана смена - загружаем ее, иначе загружаем окно входа в систему
            //if (File.Exists("Smena"))
            //{
            //    string[] sSmenaData = File.ReadAllLines("Smena")[0].Split('\t');// Открыли, разделили строку по табуляции
            //    //Так как надо избавляться от прописывания номера компьютера в main.ini - сначал апроверим, есть ли запись компа в базе
            //    //Если есть - установим этот номер. Если нет - оставим номер, хранящийся в main.ini
            //    int ComputerId = Computer.GetComputerId(Computer.GetComputerName());
            //    if (ComputerId > 0)
            //    {

            //        Session.Computer.Id = ComputerId;
            //    }
            //    DateTime dtmSmena = Convert.ToDateTime(sSmenaData[0]);// взяли первый элемент - это время последнего входа
            //    if (dtmSmena.Date == DateTime.Now.Date)
            //    {
            //        Session.User.UCid = Convert.ToInt16(sSmenaData[1]); //id записи в таблице user-computer
            //        EnterToSystem();
            //    }
            //    else
            //    {
            //        bShowLogin = true;
            //    }
            //}
            //else
            //{
            //    bShowLogin = true;
            //}
            //if (bShowLogin) //Если нет файла смены или дата смен не совпадает - показываем окно входа
            //{
                ShowLogin();  
            //}               
        }
        /// <summary>
        /// Действие при закртыии формы входа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void fLogin_FormClosing(object sender, FormClosedEventArgs e)
        {
            //frmLogin fLogin = (frmLogin)sender;
            //if (fLogin.DialogResult == DialogResult.OK) //Не понимаю, почему не срабатывает. Пришлось свойство ОК вводить
            if (fLogin.Ok) //Если на форме входа нажали OK
            {
                EnterToSystem(Session.User.UserId, Session.Computer.Id);             
            }
            else
            {
                this.Dispose(); //Пока-пока
            }
            this.Enabled = true;
        }
        /// <summary>
        /// Производит вход в систему: загружает меню, получает настройки, проверяет
        /// </summary>
        private bool EnterToSystem(int UserId, int ComputerId)
        {
            if (Login(UserId, ComputerId))
            {
                //Добавим в списки базы к которым будем коннектиться
                ProgramSettings.AddDataBases(Session.User.db_user_name);
                LoadMenu(Session.User.dtMenu);
                Global.GetGlobalVariables(); //Получим глобальные переменные
                //OrganizationInformation.GetActualOrganizationInformation(); //Получим актуальные данные организации
                if (Session.User.IsKassir && Session.Computer.IsKassa)//Если пользователь кассир - загрузим объект кассы
                {
                   CreateKassaObject(Session.User.UserId, Global.KassaAdminPassword);
                    //Проверим смену на открытость
                    State st = new State();
                    st.ShowState("Проверка состояния кассового аппарата...");
                    bool IsOpen = KassaSession.GetSessionState();
                    st.CloseState();
                    if (!IsOpen)
                    {
                        if (MessageBox.Show("Смена не открыта. Открыть смену?", "Смена ККМ", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            KassaSession.OpenSession();
                        }
                    }
                    else
                    { 
                        //Деактивировать пункт "Открыть смену"
                    }
                    Dictionary<string, string> s =(Dictionary < string, string>)KassaSession.GetECRStatus();
                    //File.AppendAllText("kkm.log","Смена открыта. Оператор " + s["OperatorNumber"]+"\n");
                }
                ProgramSettings.SetLastUserID(UserId);
                string OperatorInfo = " Оператор: " + Session.User.ShortName + " (" + Session.User.GroupName + ")";
                ToolStripLabel tslOperator = new ToolStripLabel(OperatorInfo);
                tslOperator.Alignment = ToolStripItemAlignment.Right;
                mnuStripMain.Items.Add(tslOperator);
                //smnuStripMain.Margin = new Padding(mnuStripMain.Width-, 3, 3, 3);
                return true;
            }
            else
            {
                MessageBox.Show("Неправильный пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

        }
        /// <summary>
        /// Производит попытку входа в систему.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>true - в случае успешного входа, иначе false</returns>
        bool Login(int UserId, int ComputerId)
        {
            try
            {
                return Session.User.Login(UserId, ComputerId);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }
        /// <summary>
        /// Загружает пункты меню
        /// </summary>
        /// <param name="UCid">Таблица с меню</param>
        public void LoadMenu(DataTable Menu)
        {
            try
            {
                if (mnuStripMain != null)
                {
                    mnuStripMain.Dispose();
                }
                this.Update();
                this.Refresh();
                mnuStripMain = new MenuStrip();
                mnuStripMain.Dock = DockStyle.Top;
                mnuStripMain.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                mnuStripMain.AllowMerge = true;
                mnuStripMain.AutoSize = true;
                mnuStripMain.Location= new Point(0, 0);
                mnuStripMain.GripStyle = ToolStripGripStyle.Hidden;
                mnuStripMain.Visible = true;
                mnuStripMain.RenderMode = ToolStripRenderMode.ManagerRenderMode;
                mnuStripMain.AllowDrop = false;
                mnuStripMain.Name = "mnuStripMain";
                
                ToolStripMenuItem MenuItem; //Пункт главного меню
                ToolStripMenuItem SubMenuItem; //Подпункт главного меню
                ToolStripMenuItem SubSubMenuItem; //Пункт подпункта главного меню. 2 уровень меню, проще говоря
                //Добавим их в панель меню MDI-формы
                foreach (DataRow Row in Menu.Rows)
                {
                    if (Row["id"] != DBNull.Value) //Привязка к пункту меню
                    {
                        if (Row["ObjectName"] == DBNull.Value) //Если нет имени объекта - значит это форма
                        {
                            if (Row["IdUP"] == DBNull.Value) //0 - значит основной подпункт меню. 
                            {
                                if (mnuStripMain.Items.Find(Row["MainMenuName"].ToString(), false).Length == 0) //Если главного пункта меню нет - надо добавить
                                {
                                    MenuItem = new ToolStripMenuItem();
                                    MenuItem.Text = Row["MainMenuName"].ToString();
                                    MenuItem.Name = Row["MainMenuName"].ToString();
                                    //MenuItem.Tag = Row["UFAid"];
                                    this.mnuStripMain.Items.Add(MenuItem);
                                }
                                else
                                {
                                    MenuItem = (ToolStripMenuItem)mnuStripMain.Items[Row["MainMenuName"].ToString()]; //Пункт меню по молчанию другого типа, у которого нет коллекции подменю. Поэтому преобразуем сначала
                                }
                                //Теперь добавляем подпункт в меню
                                SubMenuItem = new ToolStripMenuItem();
                                SubMenuItem.Text = Row["MenuName"].ToString();
                                SubMenuItem.Name = Row["FormName"].ToString();
                                //SubMenuItem.Tag = Row["UFAid"];
                                if (Row["icon"] != System.DBNull.Value) //Если есть иконка - преобразуем байты в рисунок  и назанчим его
                                {
                                    ImageConverter converter = new ImageConverter();
                                    try
                                    {
                                        SubMenuItem.Image = (Image)converter.ConvertFrom((byte[])Row["icon"]);
                                    }
                                    catch (Exception ex)
                                    {
                                        Errors.WriteErrorToDatabase("Ошибка загрузки изображения пункта меню " + Row["MenuName"].ToString() + "\n" + ex.ToString());
                                    }
                                }
                                SubMenuItem.Click += new EventHandler(SubMenu_Click); //Событие на нажатие пункта меню
                                                                                      //Третий уровень не делаем, он и не нужен вроде как. Если что - все в цикл =)
                                MenuItem.DropDownItems.Add(SubMenuItem);
                            }
                            else //Значит подпункт второго уровня
                            {
                                if (mnuStripMain.Items.Find(Row["LevelUpName"].ToString(), false).Length == 0) //Если нет даже главного пункта меню нет - надо добавить
                                {
                                    MenuItem = new ToolStripMenuItem();
                                    MenuItem.Text = Row["LevelUpName"].ToString();
                                    MenuItem.Name = Row["LevelUpName"].ToString();
                                    MenuItem.Tag = Row["UFAid"];
                                    this.mnuStripMain.Items.Add(MenuItem);
                                }
                                //Теперь проверим, есть ли подпункт
                                MenuItem = (ToolStripMenuItem)mnuStripMain.Items[Row["LevelUpName"].ToString()]; //Выбрали главный пункт
                                if (mnuStripMain.Items.Find(Row["MainMenuName"].ToString(), false).Length == 0) //Это уже подпункт в списке
                                {
                                    SubMenuItem = new ToolStripMenuItem();
                                    SubMenuItem.Text = Row["MainMenuName"].ToString();
                                    SubMenuItem.Name = Row["MainMenuName"].ToString();
                                    SubMenuItem.Tag = Row["UFAid"];
                                    MenuItem.DropDownItems.Add(SubMenuItem);
                                }
                                SubMenuItem = (ToolStripMenuItem)MenuItem.DropDownItems[Row["MainMenuName"].ToString()]; //Выбрали подпункт пункт
                                SubMenuItem.Name = null; //без названия будет, чтобы форму не вызывал по клику
                                                         //Добавим в него подпункт. Схема добавления та же   
                                SubSubMenuItem = new ToolStripMenuItem();
                                SubSubMenuItem.Text = Row["MenuName"].ToString(); //Название подпункта
                                SubSubMenuItem.Name = Row["FormName"].ToString(); //имя, по которому форму вызывать будем. Нет. Теперь по айдишнику
                                                                                  //SubSubMenuItem.Tag = Row["UFAid"]; //По id тоже можно вызывать
                                if (Row["icon"] != System.DBNull.Value) //Добавляем иконку, если есть
                                {
                                    try
                                    {

                                        ImageConverter converter = new ImageConverter();
                                        SubSubMenuItem.Image = (Image)converter.ConvertFrom((byte[])Row["icon"]);
                                    }
                                    catch (Exception ex)
                                    {
                                        Errors.WriteErrorToDatabase("Ошибка загрузки изображения пункта меню " + Row["MenuName"].ToString() + "\n" + ex.ToString());
                                    }
                                }
                                SubSubMenuItem.Click += new EventHandler(SubMenu_Click); //Событие тоже не меняется 
                                SubMenuItem.DropDownItems.Add(SubSubMenuItem);
                            }
                        }
                        else //Если не привязан к пункту меню
                        {
                            if (mnuStripMain.Items.Find(Row["MainMenuName"].ToString(), false).Length == 0) //Если главного пункта меню нет - надо добавить
                            {
                                MenuItem = new ToolStripMenuItem();
                                MenuItem.Text = Row["MainMenuName"].ToString();
                                MenuItem.Name = Row["MainMenuName"].ToString();
                                //MenuItem.Tag = Row["UFAid"];
                                this.mnuStripMain.Items.Add(MenuItem);
                            }
                            else
                            {
                                MenuItem = (ToolStripMenuItem)mnuStripMain.Items[Row["MainMenuName"].ToString()]; //Пункт меню по молчанию другого типа, у которого нет коллекции подменю. Поэтому преобразуем сначала
                            }
                            if (MenuItem == null) //Если главного пункта меню нет - создаем
                            {
                                MenuItem = new ToolStripMenuItem();
                                MenuItem.Text = Convert.ToString(Row["MainMenu"]);
                                MenuItem.Name = Convert.ToString(Row["MainMenu"]);
                                mnuStripMain.Items.Add(MenuItem);
                            }
                            SubMenuItem = new ToolStripMenuItem();
                            SubMenuItem.Text = Row["MenuName"].ToString();
                            SubMenuItem.Name = Row["MenuName"].ToString();
                            SubMenuItem.Tag = Row["ObjectName"].ToString() + "." + Row["MethodName"].ToString();
                            if (Row["icon"] != System.DBNull.Value) //Добавляем иконку, если есть
                            {
                                try
                                {
                                    ImageConverter converter = new ImageConverter();
                                    SubMenuItem.Image = (Image)converter.ConvertFrom((byte[])Row["Icon"]);
                                }
                                catch (Exception ex)
                                {
                                    Errors.WriteErrorToDatabase("Ошибка загрузки изображения пункта меню " + Row["MenuName"].ToString() + "\n" + ex.ToString());
                                }
                            }
                            SubMenuItem.Click += new EventHandler(SubMenuWithoutForms_Click);
                            MenuItem.DropDownItems.Add(SubMenuItem);
                        }
                    }
                }
                //Пункт настройки ? должны быть в самом конце. Для этого сначала удалим их, а потом добавим в конец
                //Все это тоже надо отвзать от конкретных названий, а сделать группу общих менюи скопом добавлять
                //Настройки
                //MenuItem = new ToolStripMenuItem();
                //MenuItem.Text = "Настройки";
                //MenuItem.Name = "frmSettings";
                //MenuItem.Click += new EventHandler(SubMenu_Click); //Событие на нажатие пункта меню
                //mnuStripMain.Items.Add(MenuItem);

                //if (Session.User.IsKassir && Session.Computer.IsKassa)
                //{
                //    {
                        //DataTable dtMenu = Session.User.GetUserMenuWithoutForms();
                        //foreach (DataRow Row in dtMenu.Rows)
                        //{
                        //    MenuItem = (ToolStripMenuItem)mnuStripMain.Items[Convert.ToString(Row["MainMenu"])]; //Выберем главное меню
                        //    if (MenuItem == null) //Если главного пункта меню нет - создаем
                        //    {
                        //        MenuItem = new ToolStripMenuItem();
                        //        MenuItem.Text = Convert.ToString(Row["MainMenu"]);
                        //        MenuItem.Name = Convert.ToString(Row["MainMenu"]);
                        //        mnuStripMain.Items.Add(MenuItem);
                        //    }
                        //    SubMenuItem = new ToolStripMenuItem();
                        //    SubMenuItem.Text = Row["MenuName"].ToString();
                        //    SubMenuItem.Name = Row["MenuName"].ToString();
                        //    SubMenuItem.Tag = Row["ObjectName"].ToString() + "." + Row["MethodName"].ToString();
                        //    if (Row["icon"] != System.DBNull.Value) //Добавляем иконку, если есть
                        //    {
                        //        try
                        //        { 
                        //            ImageConverter converter = new ImageConverter();
                        //            SubMenuItem.Image = (Image)converter.ConvertFrom((byte[])Row["Icon"]);
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            Errors.WriteErrorToDatabase("Ошибка загрузки изображения пункта меню " + Row["MenuName"].ToString() + "\n" + ex.ToString());
                        //        }
                        //    }
                        //    SubMenuItem.Click += new EventHandler(SubMenuWithoutForms_Click);
                        //    MenuItem.DropDownItems.Add(SubMenuItem);
                        //}
                        //dtMenu.Dispose();
                    //}
                //}

                //?
                //MenuItem = new ToolStripMenuItem();
                //MenuItem.Text = "?";
                //MenuItem.Name = "frmAbout";
                //MenuItem.Click += new EventHandler(SubMenu_Click); //Событие на нажатие пункта меню
                //mnuStripMain.Items.Add(MenuItem);
                ////Debugging только для отдела автоматизации
                if (Session.User.GroupID == 1 || Session.Computer.ComputerName== "COMPNATALY")
                {
                    MenuItem = new ToolStripMenuItem();
                    MenuItem.Text = "LoadDebuggingForm";
                    MenuItem.Name = "frmDebugging";
                    MenuItem.Click += new EventHandler(loadDebuggingFormToolStripMenuItem_Click); //Событие на нажатие пункта меню
                    mnuStripMain.Items.Add(MenuItem);
                    this.Controls.Add(mnuStripMain);//Добавляем менюшку на форму
                    this.MainMenuStrip = mnuStripMain;//Указываем, что менюшка -главная
                }
                //Выход
                MenuItem = new ToolStripMenuItem();
                MenuItem.Text = "Выход";
                MenuItem.Name = "Exit";
                MenuItem.Click += new EventHandler(Exit); //Событие на нажатие пункта меню
                mnuStripMain.Items.Add(MenuItem);

                this.Controls.Add(mnuStripMain);//Добавляем менюшку на форму
                this.MainMenuStrip = mnuStripMain;//Указываем, что менюшка -главная

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// Событие на нажатие пункта подменю. Получает данные формы из базы, сравнивает версию в базе с версией у пользователя и, при необходимости, загружает из базы новую версию формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubMenu_Click(object sender, EventArgs e)
        {

            //А проверим, не изменился ли LLayer
            //string Lfile = "LLayer.dll";
            //if (new Version(FileVersionInfo.GetVersionInfo(Terminal.sUpdatePath + System.IO.Path.GetFileName(Lfile)).FileVersion) > new Version(FileVersionInfo.GetVersionInfo(Application.StartupPath + "\\" + System.IO.Path.GetFileName(Lfile)).FileVersion))
            //{
            //    MessageBox.Show("Внимание! Было произведено обновление файлов терминала. Пожалуйста, закройте и откройте терминал заново!", "Обновление", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //}

            State State = new State();
            State.ShowState("Проверка обновления формы...");
            try
            {
                this.Cursor = Cursors.WaitCursor; //Покрутим курсор в ожидании
                statusMain.Text = "Открытие формы "; //Внизу напишем, чего ждем
                ToolStripMenuItem SenderMenu;
                SenderMenu = (ToolStripMenuItem)sender;
                Forms Form = Forms.GetForm(SenderMenu.Name);//Получаем данные формы
                if (Form.CheckUpdate())//Проверяем, надо ли обновить версию файла на компьютере                 
                {
                    if (!Equals(OpenedChildForms.Find(f=>f.Name ==Form.FormName),null))//Найдем наше форму в списке открытых
                    {
                        Form frm = OpenedChildForms.Find(f => f.Name == Form.FormName);
                        if (frm.WindowState==FormWindowState.Minimized)
                        {
                            frm.WindowState = FormWindowState.Maximized;

                        }
                        frm.Visible = true; //Доступ к ликвидированному объекту невозможен? Юзай Form.Close(), чтобы из списка OpenedChildForms форма убралась!
                        frm.MdiParent = this;
                        frm.Focus();
                    }
                    else
                    { 
                        //Assembly assembly assembly = Assembly.LoadFile(@"" + Form.FullPath); //Загружаем dll-ку. Загружали раньше                   
                        //Теперь будем получать файл, используя fileStream и загружая его в память, чтобы в случае обновления на лету при обновлении файл не оказался занят
                        File.AppendAllText("formfullpath",Form.FullPath);
                        using (var file = new FileStream(@"" + Form.FullPath, FileMode.Open, FileAccess.Read, FileShare.Delete))
                        {
                            Assembly assembly = null;
                            byte[] bin = new byte[524288];//dll-ка больше 500кб? Ну уж нееет, больше сотни-то через чур
                            long rdlen = 0;
                            long total = file.Length;
                            int len;
                            MemoryStream memStream = new MemoryStream((int)total);
                            rdlen = 0;
                            while (rdlen < total)
                            {
                                len = file.Read(bin, 0, 524288);
                                memStream.Write(bin, 0, len);
                                rdlen = rdlen + len;
                            }
                            // done with input file
                            file.Close();
                            assembly = Assembly.Load(memStream.ToArray());
                            //assembly = Assembly.LoadFile(@"" + Form.FullPath); //Загружаем dll-ку                      
                            Type type = assembly.GetType("UMS." + Form.FormName); //Определяем тип класса. Обратите внимание! Namespace  - UMS 
                            //По нажатию на меню не всегда вызывается форма
                            //if (type.BaseType.Name=="Form") //Если форма
                            //{
                            //    Form form = (Form)Activator.CreateInstance(type); //Создаем экземпляр формы.
                            //    form.MdiParent = this;
                            //    form.Name = Form.FormName;
                            //    form.Text += " (" + form.ProductVersion.ToString() + ")";
                            //    form.Tag = SenderMenu.Tag;//Идентификатор из UserFormAccess
                            //    form.Show();
                            //    State.CloseState();
                            //}
                            if (type.BaseType.Equals(typeof(Form)))
                            {
                                Form form = (Form)Activator.CreateInstance(type); //Создаем экземпляр формы.
                                form.FormClosed += new FormClosedEventHandler(OnChildFormClosed);
                                form.Disposed += new EventHandler((s, a) => { GC.Collect(); GC.WaitForPendingFinalizers(); });
                                form.Shown += new EventHandler(OnChildFormShown);
                                form.MdiParent = this;
                                form.Name = Form.FormName;
                                //form.Text += " (" + form.ProductVersion.ToString() + ")";
                                form.Text += " (" + Form.version.ToString() + ")";
                                form.Tag = SenderMenu.Tag;//Идентификатор из UserFormAccess
                                form.Show();
                            }
                            else if (type.BaseType.Equals(typeof(Forms)))
                            {
                                Forms form = (Forms)Activator.CreateInstance(type);//Создаем экземпляр формы.
                                form.FormClosed += new FormClosedEventHandler(OnChildFormClosed);
                                form.Disposed += new EventHandler((s, a) => { GC.Collect(); GC.WaitForPendingFinalizers(); });
                                form.Shown += new EventHandler(OnChildFormShown);
                                form.MdiParent = this;
                                form.Name = Form.FormName;
                                form.Text += " (" + Form.version.ToString() + ")";
                                form.Tag = SenderMenu.Tag;//Идентификатор из UserFormAccess
                                form.id = Form.id;
                                form.Show();     
                            }
                        }
                    }
                }
                else
                {
                    State.CloseState();
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Не удалось обновить форму. Пожалуйста, закройте и откройте программу заново для успешного  завершения обновления формы!", "Обновление", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch(Exception ex)
            {
                State.CloseState();
                Errors.WriteError(ex);
            }
            finally
            {
                State.CloseState();
            }
            this.Cursor = Cursors.Default;
            statusMain.Text = "";

        }
        /// <summary>
        /// Событие "закртыие дочерней формы". Убирает форму из списка открытых форм
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnChildFormClosed(object sender, FormClosedEventArgs e)
        {
            Form f = (Form)sender;
            OpenedChildForms.Remove(f);
            GC.Collect();
        }
        /// <summary>
        /// Событие "отображение дочерней формы". Добавляет форму в список открытых форм
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnChildFormShown(object sender, EventArgs e)
        {
            Form f = (Form)sender;
            OpenedChildForms.Add(f);
        }
        /// <summary>
        /// Пункт меню для отладки dll формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadDebuggingFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ofdSelectDll.FileName == "") //Если ничего не было выбрано заранее. Чтобы не выбирать каждый раз в рамках одного запуска
            {
                ofdSelectDll.Title = "Выберите файл dll, который следует отладить";
                ofdSelectDll.ShowDialog(); //Открываем диалог выбора файла
                ofdSelectDll.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            }
            if (ofdSelectDll.FileName != "")
            {
                try
                {
                    //Надо почитать про MEF или доразобраться с AppDomain

                    string sDllPath = ofdSelectDll.FileName;
                    Assembly assembly = Assembly.LoadFile(@sDllPath); //Загружаем dll-ку
                    Type type = assembly.GetType("UMS." + Path.GetFileNameWithoutExtension(sDllPath)); //Получаем тип вида frmName.frmName
                    //Form form = (Form)Activator.CreateInstance(type); //Создаем экземпляр формы. Если здесь ошибка в mscorlib.dll значицца имя проекта и имя класса формы не совпадают. Или вообще пространство имен у загружаемой формы не UMS! Внимательнее надо быть
                    //По нажатию на меню может запускаться обычная форма, наша расширенная форма или просто запускаться метод
                    if (type.BaseType.Equals(typeof(Form)))
                    {
                        Form form = (Form)Activator.CreateInstance(type); //Создаем экземпляр формы.
                        form.MdiParent = this;
                        form.Name = Path.GetFileNameWithoutExtension(sDllPath);
                        form.Text += " (" + form.ProductVersion.ToString() + ")";
                        form.Show();
                    }
                    else if (type.BaseType.Equals(typeof(Forms)))
                    {
                        Forms form = (Forms)Activator.CreateInstance(type); //Создаем экземпляр формы.
                        form.MdiParent = this;
                        form.Name = Path.GetFileNameWithoutExtension(sDllPath);
                        form.Text += " (" + form.ProductVersion.ToString() + ")";
                        
                        form.Show();
                    }
                    else
                    {
                        Object obj = (Object)Activator.CreateInstance(type); //Создаем экземпляр иного класса.
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Ошибка при загрузке формы", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Errors.WriteErrorToDatabase(ex.ToString());
                }
            }
        }
        private void Exit(object sender, EventArgs e)
        {
            this.Close();
            GC.Collect();
        }
        /// <summary>
        /// Возвращает или задает текст статуса, отображаемого в нижней части окна
        /// </summary>
        public string StatusText
        {
            get
            {
                return lblStatus.Text;
            }
            set
            {
                lblStatus.Text = value;
            }
        }
        /// <summary>
        /// Возвращает объект работы ссессией кассы
        /// </summary>
        public KassaSession KassaSession
        {
            get
            {
                return kassaSession;
            }
        }

        /// <summary>
        /// Срабатывает при вызове меню, для которых не должна загружаться форма, а должен вызываться метод
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubMenuWithoutForms_Click(object sender, EventArgs e)
        {
            //Пока только для кассы, но можно переделать, чтобы вызывался метод у нужного объекта
            try
            {
                ToolStripMenuItem m = (ToolStripMenuItem)sender;
                //Kassa = Activator.CreateInstance(assembly.GetType("KKM.KKM"),param );
                //var methodInfo = kassaSession.GetMethod(m.Tag.ToString());
                string[] ObjectMethod = m.Tag.ToString().Split('.'); // в Tag хранится имя объекта и его вызываемого метода: Object.Method. Разделим их.
                //MethodInfo mi = typeof(KassaSession).GetMethod(m.Tag.ToString());
                Type t = Type.GetType("UMS."+ObjectMethod[0]);//Мы в пространстве имен UMS
                if (t != null)
                {
                    MethodInfo mi = t.GetMethod(ObjectMethod[1]);
                    if (mi == null) // the method doesn't exist
                    {
                        throw new Exception("Не найден вызываемый метод");
                    }
                    else
                    {
                        //Нет времени объяснять - делай так!  (Но потом сделай нормально! Как вариант: создать статический класс и дергать его методы по имени)
                        if (t.Name==this.GetType().Name)
                        {
                            mi.Invoke(this, null);
                        }
                        else if (t.Name==KassaSession.GetType().Name)
                        {
                            mi.Invoke(KassaSession, null);
                        }
                        
                        //if (this.GetType().Equals(t))
                        //{
                        //    mi.Invoke(this, null);
                        //}
                        //else
                        //{
                        //    mi.Invoke(t, null);
                        //}
                        //MessageBox.Show("Метод есть");
                    }
                }
                else
                {
                    MessageBox.Show("Тип "+ "'UMS." + ObjectMethod[0] + "' не найден. Невозможно вызвать метод '" + ObjectMethod[1]+"'");
                }
            }
            catch (Exception ex)
            {
                Errors.WriteError(ex);
            }
        }
        /// <summary>
        /// Создает объект для работы с кассовым аппаратом
        /// </summary>
        /// <param name="AdminPassword">Пароль администратора кассы</param>
        /// <param name="UserPassword">Пароль оператора кассы</param>
        /// <remarks>Было бы неплохо сделать общий метод для создания объектов. Или научитсья использовать MEF</remarks>
        private void CreateKassaObject(int UserPassword, int AdminPassword)
        {
            kassaSession = new KassaSession(UserPassword, AdminPassword);
            //Assembly KassaAssembly; //Сборка бибилиотеки кассы
            //KassaAssembly = Assembly.LoadFile(Application.StartupPath + "\\KKM.dll"); //Загружаем dll-ку
            //_KassaType = KassaAssembly.GetType("KKM.KKM");
            //object[] param = { UserPassword, AdminPassword };
            //object kassa = Activator.CreateInstance(_KassaType, param);
        }



        //Ниже идт неведомая фигня для экспериментов с AppDomain
        internal interface IAssemblyAnalyzer
        {
            string[] Analyze(string assemblyPath);
        }

        internal class AssemblyAnalyzer : MarshalByRefObject, IAssemblyAnalyzer
        {
            public string[] Analyze(string assemblyName)
            {
                var assembly = AppDomain.CurrentDomain.Load(assemblyName);
                return assembly.GetTypes().Select(t => t.FullName).ToArray();
            }
        }

        private void tmrCheckUpdate_Tick(object sender, EventArgs e)
        {
            Terminal.CheckUpdate();
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                //case Keys.T:
                //    {
                //        if (e.Control && c == null)
                //        {
                //            c = new Chat(this);
                //        }
                //        break;
                //    }
                //case Keys.Escape:
                //    {
                //        if (c != null)
                //        {
                //            c.Visible = false;
                //            c.Dispose();
                //            c = null;
                //        }
                //        break;
                //    }
            }
        }
        private void Main_Closing(object sender, EventArgs e)
        {
            ClearTempFolder();
            if (!Equals(Session.User, null))
            {
                Session.User.Exit(); 
            }
        }
        /// <summary>
        /// Удаляет все файлы во временной директории программы
        /// </summary>
        private void ClearTempFolder()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Global.TempFolderPath);
            foreach (FileInfo file in dirInfo.GetFiles())
            {
                try
                {
                    file.Delete();
                }
                catch (Exception ex)
                {
                    Errors.WriteError(ex, false);
                }
            }
        }
    }
}
