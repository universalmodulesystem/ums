﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LLayer;

namespace UMS
{
    public partial class Chat : UserControl
    {
        Form p;
        int userID = Session.User.UserId;
        object SelectedAddresse = null;
        public Chat(Form parent)
        {
            p = parent;
            InitializeComponent();
            parent.Controls.Add(this);
            this.AutoSize = true;
            this.Visible = true;
            this.Left = parent.Width - this.Width - 20;
            this.Top = parent.Height - this.Height - 100;
            this.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
            pnlChat.AutoScroll = true;
            lstChat.ScrollAlwaysVisible = true;

        }

        private void Chat_Load(object sender, EventArgs e)
        {
            GetActiveUsers();
            AddToActive(userID);
            SendMessage(userID, User.GetOperatorFIO(userID) + " ЗАШЕЛ В ЧАТ", null);
            txtMessage.Focus();
        }

        private void tmrGetMessages_Tick(object sender, EventArgs e)
        {
            //lstChat.Items.Clear();
            GetActiveUsers();
            lstChat.DisplayMember = "Message";
            lstChat.DataSource=GetMessages(userID);
            lstChat.DisplayMember = "Message";
            lstChat.SelectedIndex = lstChat.Items.Count-1;
        }
        /// <summary>
        /// Отправляет сообщение
        /// </summary>
        /// <param name="text"></param>
        public void SendMessage(int userID, string text, object AddressID)
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL AddMessageToChat(?userID, ?Message, ?AddressTo)", new object[] { userID, text, AddressID });
        }
        public DataTable GetMessages(int userID)
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("Call GetMessageFromChat(?userID)", new object[] { userID});
        }
        public void AddToActive(int userID)
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("INSERT INTO  chat_active(userID) VALUES (?userID)", new object[] { userID});
        }
        public void Exit(int userID)
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL DeleteFromChatActive(?userID)", new object[] { userID });
        }
        public void GetActiveUsers()
        {
            cmbUsers.DataSource=Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetActiveChatUsers()");
            cmbUsers.DisplayMember = "FIO";
            cmbUsers.ValueMember = "userID";
            //if (SelectedAddresse !=null && cmbUsers.Items.Contains(SelectedAddresse))
            //{
            //    cmbUsers.SelectedValue = SelectedAddresse;
            //}
            //else
            //{
            //    cmbUsers.Text = "";
            //    cmbUsers.SelectedIndex = -1;   
            //}
        }

        private void txtMessage_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (e.KeyCode == Keys.Enter)
            {
               
                SendMessage(userID, t.Text, SelectedAddresse);
                t.Text = "";
                GetMessages(userID);
            }
        }

        private void cmbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox c = (ComboBox)sender;
            if (c.SelectedIndex == c.Items.Count-1)
            {
                SelectedAddresse = null;
            }
            else
            {
                SelectedAddresse = c.SelectedValue;
            }
        }

        private void txtMessage_TextChanged(object sender, EventArgs e)
        {

        }

        private void pnlChat_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
            {
                Exit(userID);
                SendMessage(userID, User.GetOperatorFIO(userID) + " ВЫШЕЛ ИЗ ЧАТА", null);
            }
        }
    }

}
