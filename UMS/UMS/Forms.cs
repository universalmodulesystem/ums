﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LLayer;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace UMS
{
    /// <summary>
    /// Базовый класс подклюаемых форм
    /// </summary>
    /// <remarks>Если твоя форма подклюаемая - наследуйся от этой формы</remarks>
    public class Forms : Form
    {
        int _Id; //Идентификатор формы
        byte[] _DLL;//dll-файл формы
        string _Args; //Аргументы при открытии
        string _Comment; //Комментарий к форме
        Version _Version; //Версия формы
        int _MenuId; //Идентификатор меню к которому относится форма
        string _FormName; //Имя формыdrRow("FileName")
        string _MenuName; //Имя, отображаемое в пункте меню по нажатию на который будет вызвана форма
        byte[] _Icon; //Иконка пункта меню
        bool _Active; //Флаг активности
        DateTime _TimeStamp;//Дата последнего обновления формы
                            //string _FileName; //Имя dll-файла с формой
                            // string _Path; //Относительный путь к форме
        string _NameWithNamepase;//Имя формы, включая пространство имен
        const string FormsFolder = "forms";//Папка с формами
        /// <summary>
        /// Путь к папке с настрофками формы
        /// </summary>
        public static string FormSettingsFolder = "forms\\settings";
        private HelpProvider help;
        List<Control> FormControls = new List<Control>(); //Список с элементами управления формы, состояние которых мы будем запоминать

        //private void InitializeComponent()
        //{

        //    this.SuspendLayout();
        //    // 
        //    // Forms
        //    // 
        //    this.ClientSize = new System.Drawing.Size(284, 262);
        //    this.Name = "Forms";
        //    this.Load += new System.EventHandler(this.Forms_Load);
        //    this.ResumeLayout(false);

        //}

        /// <summary>
        /// Возвращает или задает массив байтов dll-файла формы
        /// </summary>
        public byte[] dll
        {
            get
            {
                return _DLL;
            }

            set
            {
                _DLL = value;
            }
        }
        /// <summary>
        /// возвращает или задает аргументы формы 
        /// </summary>
        public string args
        {
            get
            {
                return _Args;
            }

            set
            {
                _Args = value;
            }
        }
        /// <summary>
        /// Возвращает или задает комментарий к форме
        /// </summary>
        public string Comment
        {
            get
            {
                return _Comment;
            }

            set
            {
                _Comment = value;
            }
        }
        /// <summary>
        /// Возвращает или задает версию формы
        /// </summary>
        public Version version
        {
            get
            {
                return _Version;
            }

            set
            {
                _Version = value;
            }
        }
        /// <summary>
        /// Возвращает или задает идентификатор меню, к которому относится форма
        /// </summary>
        public int MenuId
        {
            get
            {
                return _MenuId;
            }

            set
            {
                _MenuId = value;
            }
        }
        /// <summary>
        /// Возвращает или задает имя формы
        /// </summary>
        public string FormName
        {
            get
            {
                return _FormName;
            }

            set
            {
                _FormName = value;
            }
        }
        /// <summary>
        /// Возвращает или задает название пункта меню по которому происходит вызов формы
        /// </summary>
        public string MenuName
        {
            get
            {
                return _MenuName;
            }

            set
            {
                _MenuName = value;
            }
        }
        /// <summary>
        /// Возвращает или задает массив байтов иконки формы, отображаемой в меню
        /// </summary>
        public byte[] icon
        {
            get
            {
                return _Icon;
            }

            set
            {
                _Icon = value;
            }
        }
        /// <summary>
        /// Возвращает или задает флаг активности
        /// </summary>
        public bool Active
        {
            get
            {
                return _Active;
            }

            set
            {
                _Active = value;
            }
        }
        /// <summary>
        /// Возвращает дату и время последнего изменения формы
        /// </summary>
        public DateTime TIMESTAMP
        {
            get
            {
                return _TimeStamp;
            }
            set
            {
                _TimeStamp = value;
            }
        }
        /// <summary>
        /// Возвращает или задает идентификатор формы
        /// </summary>
        public int id
        {
            get
            {
                return _Id;
            }

            set
            {
                _Id = value;
            }
        }
        /// <summary>
        /// Возвращает имя фйла формы
        /// </summary>
        public string FileName
        {
            get
            {
                return FormName + ".dll";
            }
        }
        /// <summary>
        /// Возвращает относительный путь к форме
        /// </summary>
        public string Path
        {
            get
            {
                return FormsFolder + "\\" + FileName;
            }
        }
        /// <summary>
        /// Возвращает полный путь к форме
        /// </summary>
        public string FullPath
        {
            get
            {
                return Application.StartupPath + "\\" + Path;
            }
        }

        public string NameWithNamepase
        {
            get
            {
                return _NameWithNamepase;
            }

            set
            {
                _NameWithNamepase = value;
            }
        }
        /// <summary>
        /// Загружает данные формы в базу
        /// </summary>
        /// <returns></returns>
        /// <remarks>Если нет записи формы - создает запись</remarks>
        public bool UpdateForm()
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL SetFormData(?id, ?dll, ?args, ?Comment,?version, ?MenuId, ?FormName, ?MenuName, ?icon, ?Active)", new object[] { this.id, dll, args, Comment, version, MenuId, FormName, MenuName, icon, Active });
            return true;
        }
        /// <summary>
        /// Сохраняет настройки отображения формы 
        /// </summary>
        /// <param name="userId">ИД пользовтеля</param>
        /// <param name="formId">ИД формы</param>
        /// <param name="SettingsText"></param>
        /// <returns></returns>
        public static bool SetViewSettings(int userId, int formId, string SettingsText)
        {
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL SetViewSettings(?UserId, ?FormId, ?SettinsText)", new object[] { userId, formId, SettingsText });
            return true;
        }
        /// <summary>
        /// Получает настройки отображения формы 
        /// </summary>
        /// <param name="userId">ИД пользователя</param>
        /// <param name="formId">ИД формы</param>
        /// <param name="SettingsText"></param>
        /// <returns></returns>
        public static string GetViewSettings(int userId, int formId)
        {
            return Convert.ToString(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL GetViewSettings(?UserId, ?FormId)", new object[] { userId, formId }));
        }

        /// <summary>
        /// Получает форму
        /// </summary>
        /// <param name="FormName">Имя формы</param>
        /// <returns></returns>
        public static Forms GetForm(string FormName, bool OnlyActive = false)
        {
            Forms Form = null;
            if (FormName.Length > 0)
            {
                Form = new Forms();
                DataTable dtForm = Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetFormByName(?FormName, ?OnlyActive)", new object[] { FormName, OnlyActive });
                if (dtForm.Rows.Count > 0)
                {
                    foreach (DataColumn Col in dtForm.Columns)
                    {
                        PropertyInfo fi = typeof(Forms).GetProperty(Col.ColumnName);
                        if (fi != null)
                        {
                            Console.WriteLine(dtForm.Rows[0][Col].ToString());
                            try
                            {

                                if (fi.PropertyType.Equals(typeof(Version)))
                                {
                                    fi.SetValue(Form, Convert.ChangeType(new Version(dtForm.Rows[0][Col].ToString()), fi.PropertyType), null);
                                }
                                else
                                {
                                    fi.SetValue(Form, Convert.ChangeType(dtForm.Rows[0][Col], fi.PropertyType), null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }


                    }
                }
            }
            return Form;
        }
        /// <summary>
        /// Получает форму
        /// </summary>
        /// <param name="id">Идентификатор формы</param>
        /// <returns></returns>
        public static Forms GetForm(int id, bool OnlyActive = false)
        {
            Forms Form = null;
            if (id > 0)
            {
                Form = new Forms();
                DataTable dtForm = Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetFormById(?id, ?OnlyActive)", new object[] { id, OnlyActive });
                if (dtForm.Rows.Count > 0)
                {
                    foreach (DataColumn Col in dtForm.Columns)
                    {
                        PropertyInfo fi = typeof(Forms).GetProperty(Col.ColumnName);
                        if (fi != null)
                        {
                            Console.WriteLine(dtForm.Rows[0][Col].ToString());
                            try
                            {

                                if (fi.PropertyType.Equals(typeof(Version)))
                                {
                                    fi.SetValue(Form, Convert.ChangeType(new Version(Convert.ToString(dtForm.Rows[0][Col])), fi.PropertyType), null);
                                }
                                else
                                {
                                    object value = dtForm.Rows[0][Col];
                                    if (DBNull.Value == value)
                                    {
                                        if (fi.PropertyType.IsValueType)
                                        {
                                            value = Activator.CreateInstance(fi.PropertyType);
                                        }
                                        else
                                        {
                                            value = null;
                                        }
                                    }
                                    fi.SetValue(Form, Convert.ChangeType(value, fi.PropertyType), null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }


                    }
                }
            }
            return Form;
        }
        /// <summary>
        /// Проверяет версию формы и скачивает новый экземпляр в случае необходимости
        /// </summary>
        public bool CheckUpdate()
        {
            if (System.IO.File.Exists(FullPath))
            {
                if (new Version(System.Diagnostics.FileVersionInfo.GetVersionInfo(FullPath).FileVersion) >= version)
                {
                    return true;
                }
            }
            return SaveForm(FullPath, dll);
        }
        /// <summary>
        /// Сохраянет DLL-файл формы в папке с формами
        /// </summary>
        /// <param name="FullPath">Относительный путь сохранения</param>
        /// <param name="dll">Форма</param>
        /// <remarks>Если файл формы уже был - удаляется</remarks>
        private bool SaveForm(string FullPath, byte[] dll)
        {
            try
            {
                if (System.IO.File.Exists(FullPath))
                {
                    System.IO.File.Delete(FullPath);
                }
                System.IO.File.WriteAllBytes(FullPath, dll);
            }
            catch (Exception ex)
            {
                Errors.WriteErrorToDatabase(ex.ToString());
                return false;
            }
            return true;
        }
        /// <summary>
        /// Получает количество форм
        /// </summary>
        /// <returns></returns>
        public static int GetFormsCount()
        {
            return Convert.ToInt16(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT GetFormsCount()"));
        }
        /// <summary>
        /// Получает все формы
        /// </summary>
        /// <param name="OnlyActive">Только активные</param>
        /// <returns></returns>
        public static DataTable GetForms()
        {
            return Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetAllForms()");
        }
        /// <summary>
        /// Получает все элементы управления контейнера и элементы управления дочерних контейнеров
        /// </summary>
        /// <param name="cc">Родительский контейнер</param>
        /// <returns>Список элементов управления</returns>
        public static List<Control> GetControlsList(Control.ControlCollection cc)
        {
            List<Control> Controls = new List<Control>();
            foreach (Control c in cc)
            {
                Controls.Add(c);
                if (c.HasChildren)
                {
                    List<Control> ChildControls = new List<Control>();
                    ChildControls = GetControlsList(c.Controls);
                    Controls.AddRange(ChildControls);
                    ChildControls = null;
                }
            }
            return Controls;
        }
        /// <summary>
        /// Возвращает элемент управления формы по имени
        /// </summary>
        /// <param name="Controls">Список элементов управления</param>
        /// <param name="ControlName">Имя элемента управления</param>
        /// <returns>Первый найденный элемент</returns>
        /// <remarks></remarks>
        public static Control GetControlByName(List<Control> Controls, string ControlName)
        {
            return Controls.Find(x => x.Name == ControlName);
        }
        /// <summary>
        /// Устанавливает значение свойства у объекта
        /// </summary>
        /// <param name="control">Объект</param>
        /// <param name="PropertyName">Имя свойства</param>
        /// <param name="value">Значение свойства</param>
        /// <returns></returns>
        public static bool SetControlProperty(object control, string PropertyName, object value)
        {
            try
            {
                if (control != null)
                {
                    Type ControlType = control.GetType();
                    PropertyInfo Property = ControlType.GetProperty(PropertyName);
                    Property.SetValue(control, Convert.ChangeType(value, Property.PropertyType), null);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Errors.WriteError(ex);
                return false;
            }

        }
        /// <summary>
        /// Получает словарь со значениями свойства объекта
        /// </summary>
        /// <param name="control">Объект</param>
        /// <returns>Словарь имя свойства - значение</returns>
        public static Dictionary<string, object> GetControlPropertiesValues(object control)
        {
            Dictionary<string, object> PropertiesValues = new Dictionary<string, object>();
            if (control != null)
            {
                Type ControlType = control.GetType();
                foreach (PropertyInfo pi in ControlType.GetProperties())
                {
                    PropertiesValues.Add(pi.Name, pi.GetValue(control, null));
                }
            }
            return PropertiesValues;

        }
        /// <summary>
        /// Получает значение свойства объекта по имени
        /// </summary>
        /// <param name="control">Объект</param>
        /// <param name="PropertyName">Имя свойства</param>
        /// <returns>Значение свойства</returns>
        public static object GetControlPropertyValue(object control, string PropertyName)
        {
            Type ControlType = control.GetType();
            return ControlType.GetProperty(PropertyName).GetValue(control, null);
        }
        /// <summary>
        /// Сохраняет свойства указанных элементов формы в формате xml (ветвь ViewSettins) и записывает их в базу
        /// </summary>
        /// <param name="f"></param>
        public void SaveFormViewSettings()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("ViewSettings");
                doc.AppendChild(root);
                XmlNode ControlPropertiesNode = doc.CreateNode(XmlNodeType.Element, "ControlProperties", doc.NamespaceURI); //Ветвь, в которой будут храниться свойства элементов 
                doc.DocumentElement.AppendChild(ControlPropertiesNode);
                //Получим навание элементов управления и названия свойств, которые необходимо сохранять
                DataTable ControlPropertiesToSave = Session.DataBases[Session.DataBasesName.UMS].FillDataTableByQuery("CALL GetFormControlsProperties(?FormId)", new object[] { this.id });
                List<Control> FormControls = new List<Control>(); //Список всех элементов формы
                FormControls = GetControlsList(this.Controls);
                foreach (DataRow Row in ControlPropertiesToSave.Rows)
                {

                    if (Convert.ToBoolean(Row["IsContainer"])) //Если указано, что элемент - контейнер
                    {
                        Control cc = GetControlByName(FormControls, Row["ControlName"].ToString());
                        foreach (Control c in cc.Controls)
                        {
                            XmlNode ControlNode = doc.DocumentElement.SelectSingleNode(ControlPropertiesNode.Name).SelectSingleNode(c.Name); //Ветвь элемента управления
                            if (ControlNode == null)
                            {
                                ControlNode = doc.CreateNode(XmlNodeType.Element, c.Name, doc.NamespaceURI);
                                doc.DocumentElement.SelectSingleNode(ControlPropertiesNode.Name).AppendChild(ControlNode);
                            }
                            string PropertyName = Row["PropertyName"].ToString();
                            XmlAttribute PropertyAttribute = doc.CreateAttribute(PropertyName);
                            if (c != null)
                            {
                                object Value = GetControlPropertyValue(c, PropertyName);
                                PropertyAttribute.Value = Value.ToString();
                                ControlNode.Attributes.Append(PropertyAttribute);
                            }
                            else
                            {
                                LLayer.Logs.AddLog(3, "Нельзя получить свойство Элемента управления с имененм " + Row["ControlName"].ToString() + ", так как форма " + this.Name + " не содержит такого элемента управления");
                            }
                        }
                    }
                    else
                    {
                        Control c = GetControlByName(FormControls, Row["ControlName"].ToString());
                        if (c == null)
                        {
                            Errors.WriteError("Нельзя получить свойство Элемента управления с имененм " + Row["ControlName"].ToString() + ", так как форма " + this.Name + " не содержит такого элемента управления. Сообщите в отдел автоматизации.");
                            return;
                        }
                        XmlNode ControlNode = doc.DocumentElement.SelectSingleNode(ControlPropertiesNode.Name).SelectSingleNode(c.Name); //Ветвь элемента управления
                        if (ControlNode == null)
                        {
                            ControlNode = doc.CreateNode(XmlNodeType.Element, c.Name, doc.NamespaceURI);
                            doc.DocumentElement.SelectSingleNode(ControlPropertiesNode.Name).AppendChild(ControlNode);
                        }
                        string PropertyName = Row["PropertyName"].ToString();
                        XmlAttribute PropertyAttribute = doc.CreateAttribute(PropertyName);
                        if (c != null)
                        {



                            object Value = GetControlPropertyValue(c, PropertyName);
                            PropertyAttribute.Value = Value.ToString();
                            ControlNode.Attributes.Append(PropertyAttribute);
                        }
                        else
                        {
                            LLayer.Logs.AddLog(3, "Нельзя получить свойство Элемента управления с имененм " + Row["ControlName"].ToString() + ", так как форма " + this.Name + " не содержит такого элемента управления");
                        }
                    }
                }
                using (var stringWriter = new StringWriter())
                {
                    using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                    {
                        doc.WriteTo(xmlTextWriter);
                        xmlTextWriter.Flush();
                        //SetViewSettings(Convert.ToInt16(Tag), stringWriter.GetStringBuilder().ToString());
                        SetViewSettings(Session.User.UserId,this.id,stringWriter.GetStringBuilder().ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                Errors.WriteError(ex);
            }

        }

        /// <summary>
        /// Загружает настройки отображения формы из базы и применяет их
        /// </summary>
        public void LoadFormViewSettings()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                //string SettingsXml = GetViewSettings(Convert.ToInt16(Tag));
                string SettingsXml = GetViewSettings(Session.User.UserId, this.id);
                if (SettingsXml.Length > 0)
                {
                    doc.LoadXml(SettingsXml); //Загрузим xml с сохраненными данными из базы
                    XmlNodeList ControlPropertiesNodeList = doc.DocumentElement.SelectSingleNode("ControlProperties").ChildNodes; //Выберем все ветви в ветви настроек элементов управления
                    List<Control> FormControls = new List<Control>(); //Список всех элементов формы
                    FormControls = GetControlsList(this.Controls);
                    foreach (XmlNode ControlNode in ControlPropertiesNodeList)
                    {
                        //Просмотрим все атрибуты
                        foreach (XmlAttribute ControlAttribute in ControlNode.Attributes)
                        {
                            Control c = GetControlByName(FormControls, ControlNode.Name);
                            SetControlProperty(c, ControlAttribute.Name, ControlAttribute.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Errors.WriteError(ex);
            }
        }

        private void Forms_Load(object sender, EventArgs e)
        {

        }

        private void InitializeComponent()
        {
            this.help = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // Forms
            // 
            this.ClientSize = new System.Drawing.Size(288, 262);
            this.HelpButton = true;
            this.Name = "Forms";
            this.Load += new System.EventHandler(this.Forms_Load_1);
            this.ResumeLayout(false);

        }

        private void Forms_Load_1(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Подключает форму
        /// </summary>
        /// <param name="FormName">Имя подключаемой формы</param>
        /// <returns>Тип формы</returns>
        public static Type LoadForm(string FormName)
        {
            //Сразу инициализируем форму редактирования отправиетля
            Forms form = Forms.GetForm(FormName);//Получаем форму
            form.CheckUpdate();//Проверяем, надо ли обновить версию файла на компьютере
            Assembly assembly = Assembly.LoadFile(@"" + form.FullPath); //Загружаем dll-ку
            return assembly.GetType("UMS." + form.FormName); //Определяем тип класса. Обратите внимание! Namespace  - UMS 

        }
        /// <summary>
        /// Показывает форму
        /// </summary>
        /// <param name="FormName">Имя вызываемой формы</param>
        /// <param name="parameters">Параметры</param>
        /// <param name="dialog">Как диалоговое окно</param>
        public static DialogResult ShowForm(string formName, object[] parameters = null, bool dialog = true)
        {
            Forms form = Forms.GetForm(formName);//Получаем форму
            form.CheckUpdate();//Проверяем, надо ли обновить версию файла на компьютере
            //Assembly assembly = Assembly.LoadFile(@"" + fInfo.FullPath); //Загружаем dll-ку
            //Type fInfoType = assembly.GetType("UMS." + fInfo.FormName); //Определяем тип класса. Обратите внимание! Namespace  - UMS 
            //fInfo = (Forms)Activator.CreateInstance(fInfoType, parameters); //Создаем экземпляр формы.
            //if (dialog)
            //{
            //    return fInfo.ShowDialog();
            //}
            //else
            //{
            //    return DialogResult.OK;
            //}

            Assembly assembly = null;
            File.AppendAllText("formfullpath", form.FullPath);
            using (var file = new FileStream(@"" + form.FullPath, FileMode.Open, FileAccess.Read, FileShare.Delete))
            {
                byte[] bin = new byte[524288];//dll-ка больше 500кб? Ну уж нееет, больше сотни-то через чур
                long rdlen = 0;
                long total = file.Length;
                int len;
                MemoryStream memStream = new MemoryStream((int)total);
                rdlen = 0;
                while (rdlen < total)
                {
                    len = file.Read(bin, 0, 524288);
                    memStream.Write(bin, 0, len);
                    rdlen = rdlen + len;
                }
                file.Close();
                assembly = Assembly.Load(memStream.ToArray());
                Type type = assembly.GetType("UMS." + form.FormName); //Определяем тип класса. Обратите внимание! Namespace  - UMS 
                form = (Forms)Activator.CreateInstance(type, parameters); //Создаем экземпляр формы.
                if (dialog)
                {
                    return form.ShowDialog();
                }
                else
                {
                    return DialogResult.OK;
                }
            }
        }
        /// <summary>
        /// Сохраняет настройки отображения грида на форме
        /// </summary>
        /// <param name="str"></param>
        public void SaveGridLayot(Stream str)
        {
            str.Seek(0, System.IO.SeekOrigin.Begin);
            StreamReader reader = new StreamReader(str);
            string text = reader.ReadToEnd();
            Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("CALL SaveGridLayot(?UFAid, ?Settings)", new object[] { this.Tag, text });
        }
        /// <summary>
        /// Получает настройки отображения грида на форме
        /// </summary>
        /// <param name="str"></param>
        public MemoryStream GetGridLayot()
        {
            string text = Convert.ToString(Session.DataBases[Session.DataBasesName.UMS].ExecuteQuery("SELECT GetGridLayot(?UFAid)", new object[] { this.Tag}));
            byte[] byteArray = Encoding.ASCII.GetBytes(text);
            MemoryStream stream = new MemoryStream(byteArray);
            return stream;
        }
        private bool disposed = false;

        //Implement IDisposable.
        public new void Dispose()
        {
            DisposeControl(this.Controls);
            Dispose(true);
            GC.SuppressFinalize(this);
            GC.Collect();
        }

        /// <summary>
        /// Рекурсивно уничтожает все объекты Control в контейнере
        /// </summary>
        /// <param name="c"></param>
        private void DisposeControl(Control.ControlCollection controls)
        {
            foreach(Control c in controls)
            {
                if (c.HasChildren)
                {
                    DisposeControl(c.Controls);
                }
                else
                {
                    c.Dispose();
                    //c = null;
                }
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (!disposed)
        //    {
        //        if (disposing)
        //        {
        //            // Free other state (managed objects).
        //        }
        //        // Free your own state (unmanaged objects).
        //        // Set large fields to null.
        //        disposed = true;
        //    }
        //}

        // Use C# destructor syntax for finalization code.
        ~Forms()
        {
            // Simply call Dispose(false).
            Dispose();
        }
        /// <summary>
        /// Класс работы с настройками доступа формы
        /// </summary>
        public class Formaccess
        {
            int _UFAId; //Идентификатор записи из таблицы доступа к формам
            int _FormId; //Идентификатор формы
            int _UCId; //Идентификатор записи из таблицы соответствий пользователя и компьютера
            string _ViewSettings; //Настройки отображения формы
            string _AccessSettings; //Дополнительные настройки доступа к форме
                                    /// <summary>
                                    /// Возвращает или задает идентификатор записи из таблицы доступа к формам
                                    /// </summary>
            public int UFAId
            {
                get
                {
                    return _UFAId;
                }

                set
                {
                    _UFAId = value;
                }
            }
            /// <summary>
            /// Возвращает или задает идентификатор формы
            /// </summary>
            public int FormId
            {
                get
                {
                    return _FormId;
                }

                set
                {
                    _FormId = value;
                }
            }
            /// <summary>
            /// Возвращает или задает идентификатор из таблицы соответствий пользователь-компьютер
            /// </summary>
            public int UCId
            {
                get
                {
                    return _UCId;
                }

                set
                {
                    _UCId = value;
                }
            }
            /// <summary>
            /// Возвращает или задает настройки отображения формы
            /// </summary>
            public string ViewSettings
            {
                get
                {
                    return _ViewSettings;
                }

                set
                {
                    _ViewSettings = value;
                }
            }
            /// <summary>
            /// Возвращает ил задает дополнительные настройки доступа к элементам формы
            /// </summary>
            public string AccessSettings
            {
                get
                {
                    return _AccessSettings;
                }

                set
                {
                    _AccessSettings = value;
                }
            }
        }
    }
}
