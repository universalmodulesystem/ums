﻿using LLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace UMS
{
    /// <summary>
    /// Класс смены на кассовом аппарате
    /// </summary>
    public class KassaSession
    {
        Type kassa; //Тип объекта кассы
        object _kassa; //Сам объект
        public KassaSession( int UserPassword, int AdminPassword)
        {

            Assembly assembly = Assembly.LoadFile(Application.StartupPath + "\\KKM.dll"); 
            kassa = (Type)assembly.GetType("KKM.KKM");
            object[] args = { UserPassword, AdminPassword };
            _kassa = Activator.CreateInstance(kassa, args);
        }
        /// <summary>
        /// Вызывает метод открытия смены
        /// </summary>
        public void OpenSession()
        {
            kassa.InvokeMember("OpenSmena", BindingFlags.InvokeMethod, null, _kassa, null);
        }
        /// <summary>
        /// Вызывает метод открытия смены
        /// </summary>
        /// <param name="KassirID">Пароль кассира (ID из users)</param>
        public void OpenSession(long KassirID)
        {
            kassa.InvokeMember("OpenSmena", BindingFlags.InvokeMethod, null, _kassa, new object[] { KassirID});
        }
        /// <summary>
        /// Вызывает метод закрытия смены (Печать Z-отчета)
        /// </summary>
        public void CloseSession()
        {
            kassa.InvokeMember("CloseSmena", BindingFlags.InvokeMethod, null, _kassa, null);
        }
        /// <summary>
        /// Вызывает метод печатаи предварительного отчета (X-отчет)
        /// </summary>
        public void PrintXReport()
        {
            kassa.InvokeMember("PrintXReport", BindingFlags.InvokeMethod, null, _kassa, null);
        }
        ///// <summary>
        ///// Вызывает метод печати чека по отправлению с одной позицией продажи и общей суммой
        ///// </summary>
        ///// <param name="Posting">Отправление, для которого надо напечатать чек</param>
        ///// <param name="PayByCard">Признак оплаты по карте</param>
        //public void PrintCheckForPosting(OutgoingPosting Posting, bool PayByCard = false)
        //{
        //    object[] args = { Posting, PayByCard };
        //    //kassa.InvokeMember("PrintCheck", BindingFlags.InvokeMethod,null, _kassa, args);
        //    kassa.InvokeMember("PrintCheck", BindingFlags.InvokeMethod, null, _kassa, args);
        //}
        /// <summary>
        /// Вызывает метод печати чека по отправлению с раздельными позициями продаж
        /// </summary>
        /// <param name="Posting">Отправление, для которого надо напечатать чек</param>
        /// <param name="PayByCard">Признак оплаты по карте</param>
        //public void PrintCheckForPostingWithSeparatePositions(OutgoingPosting Posting, bool PayByCard = false)
        //{
        //    object[] args = { Posting, PayByCard };
        //    //kassa.InvokeMember("PrintCheck", BindingFlags.InvokeMethod,null, _kassa, args);
        //    kassa.InvokeMember("PrintCheckWithSeparatePositions", BindingFlags.InvokeMethod, null, _kassa, args);
        //}
        ///// <summary>
        ///// Вызывает метод печати возвратного чека по отправлению с раздельными позициями продаж
        ///// </summary>
        ///// <param name="Posting">Отправление, для которого надо напечатать чек</param>
        ///// <param name="PayByCard">Признак оплаты по карте</param>
        //public void PrintReturnCheckForPostingWithSeparatePositions(OutgoingPosting Posting, bool PayByCard = false)
        //{
        //    object[] args = { Posting, PayByCard };
        //    //kassa.InvokeMember("PrintCheck", BindingFlags.InvokeMethod,null, _kassa, args);
        //    kassa.InvokeMember("PrintReturnCheckWithSeparatePositions", BindingFlags.InvokeMethod, null, _kassa, args);
        //}
        ///// <summary>
        ///// Вызывает метод печати возвратного чека по отправлению
        ///// </summary>
        ///// <param name="Posting">Отправление</param>
        //public void PrintReturnCheckForPosting(OutgoingPosting Posting)
        //{
        //    object[] args = { Posting, OutgoingPosting.GetPaymentMethod(Posting.Properties.id)};
        //    kassa.InvokeMember("PrintReturnCheck", BindingFlags.InvokeMethod, null, _kassa, args);
        //}
        /// <summary>
        /// Проверяет открыта ли смена
        /// </summary>
        /// <returns>false - закрыта, иначе - true</returns>
        public bool GetSessionState()
        {
            return Convert.ToBoolean(kassa.InvokeMember("GetSmenaState", BindingFlags.InvokeMethod, null, _kassa, null));
        }
        /// <summary>
        /// Получает состояние ККМ
        /// </summary>
        /// <returns>Словарик состояние - значение</returns>
        public object GetECRStatus()
        {
            return kassa.InvokeMember("GetStatus", BindingFlags.InvokeMethod, null, _kassa, null);
        }

        public void CloseNonFiscal()
        {
            kassa.InvokeMember("CloseNonFiscalDocument", BindingFlags.InvokeMethod, null, _kassa, null);
        }
        /// <summary>
        /// Закрывает чек
        /// </summary>
        /// <param name="Posting">Отправление, для которого надо напечатать чек</param>
        /// <param name="PayByCard">Признак оплаты по карте</param>
        public void CloseCheck()
        {
            //kassa.InvokeMember("PrintCheck", BindingFlags.InvokeMethod,null, _kassa, args);
            kassa.InvokeMember("CloseCheckWithNull", BindingFlags.InvokeMethod, null, _kassa, null);
        }
    }
}
