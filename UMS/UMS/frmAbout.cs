﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Terminal2
{
    public partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {
            lblVersion.Text = Application.ProductVersion.ToString();
        }
        /// <summary>
        /// Получает количество активных форм из базы и выводит в текстовое поле
        /// </summary>
        private void GetForms()
        {
            MySqlCommand cmdGetForms = new MySqlCommand("SELECT GetFormsCount()", Terminal2.Terminal.TerminalConnection);
            lblFormCount.Text = cmdGetForms.ExecuteScalar().ToString();
            cmdGetForms.Dispose();
        }
    }
}
