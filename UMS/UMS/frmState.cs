﻿using System;
using System.Windows.Forms;

namespace UMS
{
    /// <summary>
    /// НЕ ИСПОЛЬЗОВАТЬ НАПРЯМУЮ! Для отображения информации о загрузке используйте класс State - эта форма для него!
    /// </summary>
    public partial class frmState : Form
    {
        public bool IsLoad { get; private set; } = false;
        public frmState()
        {
            TopMost = true;
            InitializeComponent();

        }
        public string StatusText
        {
            get { return lblLoading.Text; }
            set { lblLoading.Text = value; }
        }
        /// <summary>
        /// Скрывает форму
        /// </summary>
        public void HideForm()
        {
            this.Visible = false;
        }

        public void CloseStatus()
        {
            DialogResult = DialogResult.Cancel;
        }
        private void frmState_Load(object sender, EventArgs e)
        {

        }

        private void lblLoading_Click(object sender, EventArgs e)
        {

        }
        public void ShowStatus()
        {
            this.ShowDialog();
        }

        private void frmState_Shown(object sender, EventArgs e)
        {
            IsLoad = true;
        }
    }
}
