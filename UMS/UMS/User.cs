﻿using System.Data;

namespace Terminal2
{
    /// <summary>
    /// Настройки пользователя
    /// </summary>
    public class User
    {
        int iId; //Идентификатор записи пользователь-компьютер
        int iComputerId; //Номер компьютера
        int iUserId; //Номер оператора
        string sOperatorName; //Имя оператора
        string sOperatorMiddleName;//Отчество оператора
        string sOperatorSurname; //Фамилия оператора
        int iGroupId; //Код группы
        public DataTable dtMenu = new DataTable(); //Данные по меню

        /// <summary>
        /// Идентификатор пользователя в users
        /// </summary>
        public int UserId
        {
            get { return iUserId; }
            set { iUserId = value; }
        }

        /// <summary>
        /// Идентификатор компьютера из main.ini
        /// </summary>
        public int ComputerId
        {
            get { return iComputerId; }
            set { iComputerId = value; }
        }
        /// <summary>
        /// Идентификатор записи пользователь-компьютер
        /// </summary>
        public int id
        {
            get { return iId; }
            set { iId = value; }
        }

        /// <summary>
        /// Имя Оператора
        /// </summary>
        public string Name
        {
            get { return sOperatorName; }
            set { sOperatorName = value; }
        }
        /// <summary>
        /// Отчество Оператора
        /// </summary>
        public string MiddleName
        {
            get { return sOperatorMiddleName; }
            set { sOperatorMiddleName = value; }
        }
        /// <summary>
        /// Фамилия Оператора
        /// </summary>
        public string Surname
        {
            get { return sOperatorSurname; }
            set { sOperatorSurname = value; }
        }

        /// <summary>
        /// Полные Фамилия Имя Отчество
        /// </summary>
        public string FullName
        {
            get { return sOperatorSurname+" "  + sOperatorName + " "+ sOperatorMiddleName; }
        }
        /// <summary>
        /// Сокращенные Фамилия И.О.
        /// </summary>
        public string ShortName
        {
            get { return sOperatorSurname + " " + sOperatorName[0] + "." + sOperatorMiddleName[0]+"."; }
        }
        /// <summary>
        /// Возвращает или задает Код группы, к которой относится пользователь
        /// </summary>
        public int GroupID
        {
            get
            {
                return iGroupId;
            }

            set
            {
                iGroupId = value;
            }
        }
    }
}
