﻿namespace UMS
{
    partial class frmState
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picLoading = new System.Windows.Forms.PictureBox();
            this.lblLoading = new System.Windows.Forms.Label();
            this.prbProgress = new System.Windows.Forms.ProgressBar();
            this.lblState2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // picLoading
            // 
            //this.picLoading.Image = global::UMS.Properties.Resources;
            this.picLoading.Location = new System.Drawing.Point(40, 125);
            this.picLoading.Name = "picLoading";
            this.picLoading.Size = new System.Drawing.Size(220, 19);
            this.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLoading.TabIndex = 0;
            this.picLoading.TabStop = false;
            // 
            // lblLoading
            // 
            this.lblLoading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLoading.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblLoading.Location = new System.Drawing.Point(0, 60);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(300, 62);
            this.lblLoading.TabIndex = 1;
            this.lblLoading.Text = "Загрузка данных...";
            this.lblLoading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblLoading.Click += new System.EventHandler(this.lblLoading_Click);
            // 
            // prbProgress
            // 
            this.prbProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.prbProgress.Location = new System.Drawing.Point(0, 177);
            this.prbProgress.Name = "prbProgress";
            this.prbProgress.Size = new System.Drawing.Size(300, 23);
            this.prbProgress.TabIndex = 2;
            this.prbProgress.Visible = false;
            // 
            // lblState2
            // 
            this.lblState2.Location = new System.Drawing.Point(1, 155);
            this.lblState2.Name = "lblState2";
            this.lblState2.Size = new System.Drawing.Size(288, 13);
            this.lblState2.TabIndex = 3;
            this.lblState2.Text = "Загружено 1/1";
            this.lblState2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblState2.Visible = false;
            // 
            // frmState
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(300, 200);
            this.ControlBox = false;
            this.Controls.Add(this.lblState2);
            this.Controls.Add(this.prbProgress);
            this.Controls.Add(this.lblLoading);
            this.Controls.Add(this.picLoading);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmState";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmState_Load);
            this.Shown += new System.EventHandler(this.frmState_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.picLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picLoading;
        public System.Windows.Forms.Label lblLoading;
        public System.Windows.Forms.ProgressBar prbProgress;
        public System.Windows.Forms.Label lblState2;
    }
}