﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UMS
{
    public class DataGridViewExt: DataGridView
    {
        public void SaveColumnOrder()
        {
            if (this.AllowUserToOrderColumns)
            {
                List<ColumnOrderItem> columnOrder = new List<ColumnOrderItem>();
                DataGridViewColumnCollection columns = this.Columns;
                for (int i = 0; i < columns.Count; i++)
                {
                    columnOrder.Add(new ColumnOrderItem
                    {
                        ColumnIndex = i,
                        DisplayIndex = columns[i].DisplayIndex,
                        Visible = columns[i].Visible,
                        Width = columns[i].Width
                    });
                }
                DataGridViewSetting.Default.ColumnOrder[this.Name] = columnOrder;
                DataGridViewSetting.Default.Save();
            }
        }

        public void SetColumnOrder()
        {
            if (!DataGridViewSetting.Default.ColumnOrder.ContainsKey(this.Name))
                return;

            List<ColumnOrderItem> columnOrder =
                DataGridViewSetting.Default.ColumnOrder[this.Name];

            if (columnOrder != null)
            {
                var sorted = columnOrder.OrderBy(i => i.DisplayIndex);
                foreach (var item in sorted)
                {
                    this.Columns[item.ColumnIndex].DisplayIndex = item.DisplayIndex;
                    this.Columns[item.ColumnIndex].Visible = item.Visible;
                    this.Columns[item.ColumnIndex].Width = item.Width;
                }
            }
        }
    }

    internal sealed class DataGridViewSetting : ApplicationSettingsBase
    {
        private static DataGridViewSetting _defaultInstace =
            (DataGridViewSetting)ApplicationSettingsBase.Synchronized(new DataGridViewSetting());

        public static DataGridViewSetting Default
        {
            get { return _defaultInstace; }
        }

        [UserScopedSetting]
        [SettingsSerializeAs(SettingsSerializeAs.Binary)]
        [DefaultSettingValue("")]

        public Dictionary<string, List<ColumnOrderItem>> ColumnOrder
        {
            get { return this["ColumnOrder"] as Dictionary<string, List<ColumnOrderItem>>; }
            set { this["ColumnOrder"] = value; }
        }
    }

    [Serializable]
    public sealed class ColumnOrderItem
    {
        public int DisplayIndex { get; set; }
        public int Width { get; set; }
        public bool Visible { get; set; }
        public int ColumnIndex { get; set; }
    }
}
