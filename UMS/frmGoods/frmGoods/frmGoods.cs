﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LLayer;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace UMS
{
    public partial class frmGoods : Forms
    {
        public frmGoods()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataBind();
            WindowState = FormWindowState.Maximized;
            this.gpGoods.GridView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gpGoods_KeyUp);
            this.gpGoods.GridView.DoubleClick += new System.EventHandler(this.gpGoods_CellMouseDoubleClick);
            this.gpGoods.GridView.Click += new System.EventHandler(this.gpGoods_Click);
        }
        private void DataBind()
        {
            gpGoods.DataSource = Good.GetAllGoods();
            gpGoods.BestFitColumns();
            gpGoods.ShowAutoFilter = true;
            foreach (GridColumn c in gpGoods.GridView.Columns)
            {
                c.OptionsFilter.AutoFilterCondition = AutoFilterCondition.Contains;
            }
        }

        private void txtBarCode_TextChanged(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            if (t.Text.Trim().Length > 0)
            {
                string ColumnName = "Штрих-код";
                //gpGoods.AddFilterCriteria("Штрих-код", t.Text);
                gpGoods.GridView.Columns[ColumnName].FilterInfo = new ColumnFilterInfo("[" + ColumnName + "] LIKE '%"+t.Text+"%'");
            }
            else
            {
                gpGoods.GridView.ActiveFilter.Clear();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            gpGoods.Export();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gpGoods_DoubleClick(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Открывает оконо редактирования товара
        /// </summary>
        private void ShowEdit()
        {
            int[] selRows = gpGoods.GridView.GetSelectedRows();
            if (selRows[0] >= 0)
            {
                DataRowView selRow = (DataRowView)((gpGoods.GridView).GetRow(selRows[0]));
                frmGoodEdit fEdit = new frmGoodEdit(Good.GetGood(Convert.ToInt32(selRow["id"])));
                fEdit.StartPosition = FormStartPosition.CenterScreen;
                fEdit.ShowDialog();
                DataBind();
                

            }
            else
            {
                SendKeys.Send("{DOWN}");
            }
        }

        private void gpGoods_CellMouseDoubleClick(object sender, EventArgs e)
        {
            ShowEdit();
        }

        private void gpGoods_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        ShowEdit();
                        break;
                    }
            }
        }

        private void gpGoods_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("ok");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmGoodEdit fEdit = new frmGoodEdit();
            fEdit.IsNew = true;
            fEdit.StartPosition = FormStartPosition.CenterScreen;
            fEdit.ShowDialog();
            DataBind();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            DataBind();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int[] selRows = gpGoods.GridView.GetSelectedRows();
            if (selRows[0] >= 0)
            {
                DataRowView selRow = (DataRowView)((gpGoods.GridView).GetRow(selRows[0]));
                Good g = Good.GetGood(Convert.ToInt32(selRow["id"]));
                if (MessageBox.Show("Удалить товар с ID " + g.id.ToString() + " '" + g.Name + "'?", "Удаление товара", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    g.Delete();
                    btnRefresh.PerformClick();
                }
            }
        }
    }
}
