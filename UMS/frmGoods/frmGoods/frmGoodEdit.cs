﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LLayer;

namespace UMS
{
    public partial class frmGoodEdit : Form
    {
        public bool IsNew { get; set; } = false;
        public Good Good {get; set;}
        public frmGoodEdit()
        {
            InitializeComponent();
            Good = new Good();
        }
        public frmGoodEdit(Good g)
        {
            InitializeComponent();
            Good = g;
        }


        private void frmGoodEdit_Load(object sender, EventArgs e)
        {
            cmbProvider.DataSource = LLayer.Provider.GetProviders();
            cmbProvider.DisplayMember = "Название";
            cmbProvider.ValueMember = "id";
            cmbType.DataSource = LLayer.GoodType.GetGoodTypes();
            cmbType.DisplayMember = "Name";
            cmbType.ValueMember = "id";
            if (!IsNew)
            {
                Fill();
            }
        }
        private void Fill()
        {
            txtBarCode.Text = Good.BarCode;
            NumAmount.Value = Good.amount;
            cmbType.SelectedValue = Good.Type_Id;
            cmbProvider.SelectedValue = Good.provider_id;
            txtName.Text = Good.Name;
            numMRP.Value = Convert.ToDecimal(Good.mrp);
            numPrice.Value = Convert.ToDecimal(Good.Price);
            numPurchasePrice.Value = Convert.ToDecimal(Good.purchase_price);



        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            //Сделать форму и повесить всё это на изменение полей
            Good.BarCode = txtBarCode.Text;
            Good.amount= Convert.ToInt16(NumAmount.Value);
            Good.Type_Id = Convert.ToInt16(cmbType.SelectedValue);
            Good.provider_id = Convert.ToInt16(cmbProvider.SelectedValue);
            Good.Name= txtName.Text;
            Good.mrp=Convert.ToDouble(numMRP.Value );
            Good.Price= Convert.ToDouble(numPrice.Value );
            Good.purchase_price= Convert.ToDouble(numPurchasePrice.Value);
            Good.Update();
            this.Close();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
