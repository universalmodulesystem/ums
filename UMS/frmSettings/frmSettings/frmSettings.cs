﻿using System;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Drawing;
using System.Data;
using LLayer;
//using DevExpress.XtraTreeList;

namespace UMS
{
    public partial class frmSettings : Form
    {
        
        /// <summary>
        /// Форма, которую добавляем/редактируем
        /// </summary>
        Forms f;
        /// <summary>
        /// Пользователь, которого добавляем/редактируем
        /// </summary>
        User u;
        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            try
            {
                FillMenuComboBox();
                FillFormComboBox();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
          
        }
        /// <summary>
        /// Заполняет список с названиями Пунктов Меню
        /// </summary>
        private void FillMenuComboBox()
        {
            cmbMenu.DataSource = global::UMS.Menu.GetMainMenu(true);
            cmbMenu.DisplayMember = "Name";
            cmbMenu.ValueMember = "id";

        }

        /// <summary>
        /// Заполняет список с названиями форм
        /// </summary>
        private void FillFormComboBox()
        {
            cmbFormName.DataSource = global::UMS.Forms.GetForms();
            cmbFormName.DisplayMember = "FormName";
            cmbFormName.ValueMember = "id";

        }

        ///// <summary>
        ///// Отображает меню выбранного элемента
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void treeMenu_AfterSelect(object sender, TreeViewEventArgs e)
        //{
        //    if (NowControl != null)
        //    {
        //        NowControl.Visible = false;
        //    }
        //    if (this.Controls.Find(treeMenu.SelectedNode.Name, true).Length > 0)
        //    {
        //        NowControl = this.Controls.Find(treeMenu.SelectedNode.Name, true)[0];
        //        NowControl.Visible = true;
        //    }
        //}
        /// <summary>
        ///// Загружает данные для соответствующего элемента
        ///// </summary>
        //private void GetControlElementData(object sender, EventArgs e)
        //{
        //    if (NowControl != null)
        //    {
        //        if (NowControl.Visible)
        //        {
        //            //Сюда потом добавим вызов метода по имени из базы
        //            MethodInfo mi = this.GetType().GetMethod("GetFormManageData", BindingFlags.NonPublic | BindingFlags.Instance);
        //            if (mi != null)
        //            {
        //                mi.Invoke(this, null);
        //            }
        //            else
        //            {
        //                Console.WriteLine("Wrong Function Name");
        //            }
        //        }
        //    }
        //}
        ///// <summary>
        ///// Создает экземпляр класса с настройками форм, заполняет его и список с формами
        ///// </summary>
        //private void GetFormManageData()
        //{
        //    try
        //    {
        //        fData = new FormData();
        //        fData.FillAllFormData();
        //        cmbForms.DataSource = fData.Data;
        //        cmbForms.DisplayMember = "FormName";
        //        cmbMenu.DataSource = fData.MenuData;
        //        cmbMenu.DisplayMember = "Name";
        //    }
        //    catch(Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
            
        //}

        //private void cmbForms_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ComboBox cmb = (ComboBox)sender;
        //    if (cmb.SelectedIndex != -1)
        //    {
        //        FillFormData(cmb.SelectedIndex);
        //    }
        //    else
        //    {

        //    }
        //}
        ///// <summary>
        ///// Заполняет поля выбранной формы в груп-боксе управления формами (gpbxFormManage)
        ///// </summary>
        ///// <param name="Index"></param>
        //private void FillFormData(int Index)
        //{
        //    txtComment.Text = fData.Data.Rows[Index]["Comment"].ToString();
        //    lblVersion.Text = fData.Data.Rows[Index]["version"].ToString();
        //    cmbMenu.Select((int)fData.Data.Rows[Index]["MenuId"],1);
        //    txtMenu.Text = fData.Data.Rows[Index]["MenuName"].ToString();
        //    chkActive.Checked=(Convert.ToInt16(fData.Data.Rows[Index]["Active"]) == 0) ? false : true; 
        //    lblTimeStamp.Text=fData.Data.Rows[Index]["Timestamp"].ToString();
        //    dll = null;//Используется только при добавлении или обновлении формы
        //    Version= fData.Data.Rows[Index]["Version"].ToString();
        //    lblNewDate.Visible = false;
        //    //Загрузим рисунок
        //    if (fData.Data.Rows[Index]["icon"] != System.DBNull.Value)
        //    {
        //        MemoryStream mStream = new MemoryStream();
        //        byte[] pData = (byte[])fData.Data.Rows[Index]["icon"];
        //        mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
        //        Bitmap Image = new Bitmap(mStream, false);
        //        mStream.Dispose();
        //        picImage.Image = Image;
        //    }
        //    else
        //    {
        //        picImage.Image = null;
        //    }
        //}

        private void FormManage_Enter(object sender, EventArgs e)
        {

        }

        //private void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (cmbMenu.SelectedIndex == -1)//Может возникнуть при добавлении новго пункта меню
        //        {
        //            MessageBox.Show("Выберете пункт меню, к которому принадлежит форма!", "Не выбран пункт меню!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //            cmbMenu.Focus();
        //            return;
        //        }
        //        if (txtMenu.Text.Trim() == "")
        //        {
        //            MessageBox.Show("Название пункта меню не может бытьт пустым!", "Нет названия пункта меню!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //            txtMenu.Focus();
        //            return;
        //        }
        //        int iSelectedIndex = cmbForms.SelectedIndex; //Запомним выбранный индекс
        //        cmbForms.DataSource = null; //Отвяжем DataTable От списка, ибо при изменении DataTable, которые будут дальше, инициировался SelectIndex у списка, загружались старые данные и обновление не происходило
        //        //Добавить значения для обновления!
        //        fData.Data.Rows[iSelectedIndex]["Comment"] = txtComment.Text.Trim(); //Задаем новое описание
        //        fData.Data.Rows[iSelectedIndex]["MenuId"] = Convert.ToInt16(fData.MenuData.Select("Name='" + cmbMenu.Text + "'")[0]["id"]); //Задаем новый пункт главного меню
        //        fData.Data.Rows[iSelectedIndex]["MenuName"] = txtMenu.Text.Trim(); //Задаем новое название пункта меню
        //        fData.Data.Rows[iSelectedIndex]["Active"] = (chkActive.Checked) ? 1:0;//if  в одну строку: Если check=true - ставим единицу, иначе 0
        //        if (dll!= null) //Если добавляли/обьновляли
        //        {
        //            fData.Data.Rows[iSelectedIndex]["dll"] = dll;
        //            fData.Data.Rows[iSelectedIndex]["Version"] = Version;
        //        }
        //        //Переконвертируем изображение обратно в байты
        //        ImageConverter converter = new ImageConverter();
        //        if (picImage.Image != null)
        //        {
        //            fData.Data.Rows[iSelectedIndex]["icon"] = (byte[])converter.ConvertTo(picImage.Image, typeof(byte[]));
        //        }
        //        else
        //        {
        //            fData.Data.Rows[iSelectedIndex]["icon"] = System.DBNull.Value;
        //        }

        //        fData.Data.EndInit(); //Заканчивам редактирование
        //        int iUpdates;
        //        iUpdates=fData.UpdateData();
        //        MessageBox.Show("Обновление прошло успешно. Обновлено форм: " + iUpdates.ToString(), "Обновлено", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        cmbForms.DataSource = fData.Data;
        //        cmbForms.DisplayMember = "FormName";
        //        cmbForms.SelectedIndex = iSelectedIndex;
        //        lblNewDate.Visible = false;
        //        Terminal.MainForm.LoadMenu(Session.User.dtMenu);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //}
        ///// <summary>
        /// Заполняет gpFormSettings элементами управления
        /// </summary>
        private void FillFormSettings()
        {
            //Идея такая: у форм будут настройки, значения которых будут прописываться в соответствующих настройках пользователя

        }

        private void btnBrowseIcon_Click(object sender, EventArgs e)
        {
            if (ofdSelectImage.ShowDialog() == DialogResult.OK)
            {
                if (ofdSelectImage.FileName != "")
                {
                    picIcon.ImageLocation = ofdSelectImage.FileName;
                    f.icon = File.ReadAllBytes(picIcon.ImageLocation);
                }
            }
        }
        /// <summary>
        /// Удаляет иконку пункта меню
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void btnDeleteImage_Click(object sender, EventArgs e)
        //{
        //    picImage.Image = null;
        //}

        private void btnUpdateForm_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Открывает окно выбора dll-файла, добавляет или обновляет данные по выбранной форме и записывает их в DataTable с формами
        ///// </summary>
        //private void UpdateOrAddForm()
        //{
        //    try
        //    {
        //        if (ofdSelectDll.ShowDialog()==DialogResult.OK)
        //        {
        //            if (ofdSelectDll.FileName!="")
        //            {
        //                //Проверим, новая ли форма, али добавлять надо
        //                string sFormName = Path.GetFileNameWithoutExtension(ofdSelectDll.FileName);
        //                DataRow Row; //Сюда запишем найденную строку или новую строку с параметрами формы
        //                if (fData.Data.Select("FormName='" + sFormName + "'").Length == 1) //Ищем строку с нужным именем формы. Напоминаю, что имя уникально
        //                {
        //                    Row = fData.Data.Select("FormName='" + sFormName + "'")[0];// Выберем найденную строку
        //                    cmbForms.SelectedIndex = cmbForms.Items.IndexOf(sFormName);
        //                    if (new Version(Row["version"].ToString()) >= new Version(System.Diagnostics.FileVersionInfo.GetVersionInfo(ofdSelectDll.FileName).FileVersion))
        //                    {
        //                        MessageBox.Show("Версия выбранного файла не больше, чем версия, хранящаяся в базе! В случае добавления в базу обновление формы у пользователей не произойдет!", "Неправильная версия файла", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //                    }
        //                }
        //                else
        //                {
        //                    Row = fData.Data.NewRow(); //Создадим новую строку
        //                    Row["MenuId"] = 1;
        //                    Row["FormName"] = sFormName;
        //                    Row["Active"] = 1;
        //                    Row["icon"] =System.DBNull.Value;
        //                    fData.Data.Rows.Add(Row);
        //                }
        //                lblNewDate.Text = "Дата изменения: " + File.GetLastWriteTime(ofdSelectDll.FileName).ToString();
        //                cmbForms.SelectedIndex = fData.Data.Rows.IndexOf(Row); //Выберем строку, чтобы данные в контролах обновились и очистились dll и Version
        //                //А теперь пропишем их
        //                Version = new Version(System.Diagnostics.FileVersionInfo.GetVersionInfo(ofdSelectDll.FileName).FileVersion).ToString();//Записываем версию
        //                dll = File.ReadAllBytes(ofdSelectDll.FileName); //Записываем байты dll-ки
        //                lblNewDate.Visible = true;
        //                cmbMenu.ResetText(); //Сбросим пункт меню формы, чтобы нужно было выбрать
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
    //}
        private void cmbFormName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox c = (ComboBox)sender;
            //int id = 0;
            //int.TryParse(c.SelectedValue.ToString(), out id);
            //f = form.GetForm(id);
            //FillFormData(f);

        }
        /// <summary>
        /// Заполняет область данных формы
        /// </summary>
        /// <param name="form"></param>
        private void FillFormData(Forms form)
        {
            try
            {
                if (form != null)
                {
                    chkHasMenu.CheckedChanged -= chkHasMenu_CheckedChanged; //Уберем событие на изменение статуса 
                     chkHasMenu.Checked = form.MenuId != 0;
                    chkHasMenu.CheckedChanged += chkHasMenu_CheckedChanged; //Добавим событие
                    if (form.MenuId == 0)
                    {
                        //cmbMenu.SelectedIndex = -1;
                        cmbMenu.Text = "Без пункта меню";
                        txtMenu.Text = "";
                    }
                    else
                    {
                        cmbMenu.SelectedValue = form.MenuId;
                        txtMenu.Text = form.MenuName;
                    }
                    txtArgs.Text = form.args;
                    txtVersion.Text = form.version.ToString();
                    txtComment.Text = form.Comment;
                    lblDate.Text = form.TIMESTAMP.ToString();
                    chkFormActive.Checked = form.Active;
                    picIcon.Image = null;
                    picIcon.Refresh();
                    //Некоторые иконки могут быть глючные. Если появляется крест вместо иконки - пересохраните исходный файл в ico И заново загрузите
                    if (form.icon != null && form.icon.Length > 0)
                    {
                        MemoryStream mStream = new MemoryStream();
                        byte[] pData = form.icon;
                        mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                        Bitmap Image = new Bitmap(mStream, false);
                        mStream.Dispose();
                        picIcon.Image = Image;
                    }
                    gpForm.Enabled = true;
                }
                else
                {
                    gpForm.Enabled = false;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
            this.Dispose();
        }

        private void frmSettings_Shown(object sender, EventArgs e)
        {
            //FillFormComboBox();
            //FillMenuComboBox();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (ofdSelectDll.ShowDialog() == DialogResult.OK)
            {
                if (ofdSelectDll.FileName != "")
                {
                    LoadForm(ofdSelectDll.FileName);
                }
            }
        }
        private void LoadForm(string path)
        {
            try
            {      
                //Проверим, новая ли форма, али добавлять надо
                string sFormName = System.IO.Path.GetFileNameWithoutExtension(path);
                int index = cmbFormName.FindString(sFormName); //Проверим, существует ли такая форма уже
                Version FileVerision = new Version(System.Diagnostics.FileVersionInfo.GetVersionInfo(path).FileVersion);
                if (index > -1)  //Если существует
                {
                    //Выберем форму
                    cmbFormName.SelectedIndex = index;
                    //Обновим у формы массив байтов dll-ки
                    if (f.version >= FileVerision)
                    {
                        MessageBox.Show("Версия выбранного файла не больше, чем версия, хранящаяся в базе! В случае добавления в базу обновление формы у пользователей не произойдет!", "Неправильная версия файла", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                    cmbFormName.DropDownStyle = ComboBoxStyle.DropDownList;
                    cmbFormName.Enabled = true;
                }
                else //Если новую добавлям
                {
                    f = new Forms();
                    cmbFormName.DropDownStyle = ComboBoxStyle.DropDown;
                    cmbFormName.Text = sFormName;
                    cmbFormName.Enabled = false;
                    f.id = 0;
                }
                f.dll = File.ReadAllBytes(path); //Считаем байты 
                f.version = FileVerision;
                f.FormName = sFormName;
                f.Name = sFormName;
                FillFormData(f);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                f.UpdateForm();
                UMS.MainForm.LoadMenu(Session.User.GetMenu());
                MessageBox.Show("Готово!", "Обновлено",MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void cmbFormName_SelectedValueChanged(object sender, EventArgs e)
        {
                ComboBox c = (ComboBox)sender;
                int id = 0;
                int.TryParse(c.SelectedValue.ToString(), out id);
                f = Forms.GetForm(id);
                FillFormData(f);
        }

        private void cmbMenu_SelectedValueChanged(object sender, EventArgs e)
        {
            if (f != null)
            {
                ComboBox c = (ComboBox)sender;
                int id = -1;
                int.TryParse(c.SelectedValue.ToString(), out id);
                f.MenuId = id;
            }
        }

        private void txtMenu_TextChanged(object sender, EventArgs e)
        {
            if (f != null)
            {
                TextBox t = (TextBox)sender;
                f.MenuName = t.Text;
            }
        }

        private void txtArgs_TextChanged(object sender, EventArgs e)
        {
            if (f != null)
            {
                TextBox t = (TextBox)sender;
                f.args = t.Text;
            }
        }

        private void txtVersion_TextChanged(object sender, EventArgs e)
        {
            if (f != null)
            {
                TextBox t = (TextBox)sender;
                f.version = new System.Version(t.Text);
            }
        }

        private void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            if (f != null)
            {
                CheckBox c = (CheckBox)sender;
                f.Active = c.Checked;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (tabSettings.SelectedTab == tabForms)
            {
                btnFormsApply.PerformClick();
                btnClose.PerformClick();
            }

        }

        private void chkHasMenu_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            pnlMenu.Enabled = chk.Checked;
            f.MenuId = 0;
            f.MenuName = "";
        }

        private void cmbFormName_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void txtComment_TextChanged(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            f.Comment = t.Text;
        }
        /// <summary>
        /// Заполняет вкладку Пользователи
        /// </summary>
      


        private void txtSurname_TextChanged(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            u.Surname = t.Text;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            u.Name= t.Text;
        }

        private void txtMiddleName_TextChanged(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            u.MiddleName = t.Text;
        }

        private void chkUserActive_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox c = (CheckBox)sender;
            u.Active = c.Checked;
        }

      

   

        private void clbComputers_ItemCheck(object sender, ItemCheckEventArgs e)
        {
          
        }

        private void clbComputers_Click(object sender, EventArgs e)
        {

        }

        private void clbComputers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    

        /// <summary>
        /// Ставит или снимает галочки у заданной ветви и у всех ее потомков
        /// </summary>
        /// <param name="node">Ветвь</param>
        /// <param name="isChecked">Признак выделения</param>
        private void CheckTreeViewNode(TreeNode node, Boolean isChecked)
        {
            foreach (TreeNode item in node.Nodes)
            {
                item.Checked = isChecked;

                if (item.Nodes.Count > 0)
                {
                    this.CheckTreeViewNode(item, isChecked);
                }
            }
        }
    }
}
