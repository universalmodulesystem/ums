﻿namespace LLayer
{
    /// <summary>
    /// Настройки, связанные со слоем логики
    /// </summary>
    public static class Settings
    {
        /// <summary>
        /// Настройки подключений к базам данных
        /// </summary>
        public static class ConnectionSettings
        {
            private static string _TerminalConnectionString; //Строка подключения к базе данных программы
            private static string _EmsConnectionString; //Строка подключения к базе данных
            /// <summary>
            /// Возвращает или задает строку подключения к базе данных программы
            /// </summary>
            public static string TerminalConnectionString
            {
                get
                {
                    return _TerminalConnectionString;
                }

                set
                {
                    _TerminalConnectionString = value;
                }
            }
            /// <summary>
            /// Возвращает или задает строку подключения к базе данных Грантпост
            /// </summary>
            public static string EmsConnectionString
            {
                get
                {
                    return _EmsConnectionString;
                }

                set
                {
                    _EmsConnectionString = value;
                }
            }
        }
    }
}
