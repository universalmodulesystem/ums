﻿namespace UMS
{
    partial class frmSettings
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.ofdSelectImage = new System.Windows.Forms.OpenFileDialog();
            this.ofdSelectDll = new System.Windows.Forms.OpenFileDialog();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabForms = new System.Windows.Forms.TabPage();
            this.pnlFormSettings = new System.Windows.Forms.Panel();
            this.gpForm = new System.Windows.Forms.GroupBox();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkHasMenu = new System.Windows.Forms.CheckBox();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.txtMenu = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbMenu = new System.Windows.Forms.ComboBox();
            this.btnFormsApply = new System.Windows.Forms.Button();
            this.lblDate = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gpIcon = new System.Windows.Forms.GroupBox();
            this.btnIconBrowse = new System.Windows.Forms.Button();
            this.picIcon = new System.Windows.Forms.PictureBox();
            this.chkFormActive = new System.Windows.Forms.CheckBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.labelVEr = new System.Windows.Forms.Label();
            this.txtArgs = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbFormName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.pnlControls.SuspendLayout();
            this.tabForms.SuspendLayout();
            this.pnlFormSettings.SuspendLayout();
            this.gpForm.SuspendLayout();
            this.pnlMenu.SuspendLayout();
            this.gpIcon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // ofdSelectImage
            // 
            this.ofdSelectImage.FileName = "ofdSelectIcon";
            this.ofdSelectImage.Filter = "Все файлы | *.*";
            // 
            // ofdSelectDll
            // 
            this.ofdSelectDll.DefaultExt = "*.dll";
            this.ofdSelectDll.FileName = "*.dll";
            this.ofdSelectDll.Filter = "Формы Терминала-2 | *.dll";
            // 
            // pnlControls
            // 
            this.pnlControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlControls.Controls.Add(this.btnOk);
            this.pnlControls.Controls.Add(this.btnClose);
            this.pnlControls.Location = new System.Drawing.Point(3, 272);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(881, 45);
            this.pnlControls.TabIndex = 4;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(695, 0);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(90, 43);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "ОК";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(791, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 43);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Закрыть (Esc)";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabForms
            // 
            this.tabForms.Controls.Add(this.pnlFormSettings);
            this.tabForms.Location = new System.Drawing.Point(4, 22);
            this.tabForms.Name = "tabForms";
            this.tabForms.Padding = new System.Windows.Forms.Padding(3);
            this.tabForms.Size = new System.Drawing.Size(873, 237);
            this.tabForms.TabIndex = 0;
            this.tabForms.Text = "Формы";
            this.tabForms.UseVisualStyleBackColor = true;
            // 
            // pnlFormSettings
            // 
            this.pnlFormSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFormSettings.Controls.Add(this.gpForm);
            this.pnlFormSettings.Controls.Add(this.panel1);
            this.pnlFormSettings.Controls.Add(this.cmbFormName);
            this.pnlFormSettings.Controls.Add(this.label2);
            this.pnlFormSettings.Location = new System.Drawing.Point(6, 6);
            this.pnlFormSettings.Name = "pnlFormSettings";
            this.pnlFormSettings.Size = new System.Drawing.Size(861, 225);
            this.pnlFormSettings.TabIndex = 1;
            // 
            // gpForm
            // 
            this.gpForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpForm.Controls.Add(this.txtComment);
            this.gpForm.Controls.Add(this.label7);
            this.gpForm.Controls.Add(this.chkHasMenu);
            this.gpForm.Controls.Add(this.pnlMenu);
            this.gpForm.Controls.Add(this.btnFormsApply);
            this.gpForm.Controls.Add(this.lblDate);
            this.gpForm.Controls.Add(this.label6);
            this.gpForm.Controls.Add(this.gpIcon);
            this.gpForm.Controls.Add(this.chkFormActive);
            this.gpForm.Controls.Add(this.txtVersion);
            this.gpForm.Controls.Add(this.labelVEr);
            this.gpForm.Controls.Add(this.txtArgs);
            this.gpForm.Controls.Add(this.label3);
            this.gpForm.Location = new System.Drawing.Point(3, 54);
            this.gpForm.Name = "gpForm";
            this.gpForm.Size = new System.Drawing.Size(442, 165);
            this.gpForm.TabIndex = 1;
            this.gpForm.TabStop = false;
            this.gpForm.Text = "Форма";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(75, 92);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(265, 20);
            this.txtComment.TabIndex = 18;
            this.txtComment.TextChanged += new System.EventHandler(this.txtComment_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Коммент";
            // 
            // chkHasMenu
            // 
            this.chkHasMenu.AutoSize = true;
            this.chkHasMenu.Location = new System.Drawing.Point(13, 21);
            this.chkHasMenu.Name = "chkHasMenu";
            this.chkHasMenu.Size = new System.Drawing.Size(15, 14);
            this.chkHasMenu.TabIndex = 16;
            this.chkHasMenu.UseVisualStyleBackColor = true;
            this.chkHasMenu.CheckedChanged += new System.EventHandler(this.chkHasMenu_CheckedChanged);
            // 
            // pnlMenu
            // 
            this.pnlMenu.Controls.Add(this.txtMenu);
            this.pnlMenu.Controls.Add(this.label5);
            this.pnlMenu.Controls.Add(this.label4);
            this.pnlMenu.Controls.Add(this.cmbMenu);
            this.pnlMenu.Location = new System.Drawing.Point(0, 18);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(342, 49);
            this.pnlMenu.TabIndex = 15;
            // 
            // txtMenu
            // 
            this.txtMenu.Location = new System.Drawing.Point(74, 27);
            this.txtMenu.Name = "txtMenu";
            this.txtMenu.Size = new System.Drawing.Size(265, 20);
            this.txtMenu.TabIndex = 9;
            this.txtMenu.TextChanged += new System.EventHandler(this.txtMenu_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Пункт меню";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Меню";
            // 
            // cmbMenu
            // 
            this.cmbMenu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMenu.FormattingEnabled = true;
            this.cmbMenu.Location = new System.Drawing.Point(74, 0);
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.Size = new System.Drawing.Size(265, 21);
            this.cmbMenu.TabIndex = 6;
            this.cmbMenu.SelectedValueChanged += new System.EventHandler(this.cmbMenu_SelectedValueChanged);
            // 
            // btnFormsApply
            // 
            this.btnFormsApply.Location = new System.Drawing.Point(348, 128);
            this.btnFormsApply.Name = "btnFormsApply";
            this.btnFormsApply.Size = new System.Drawing.Size(79, 26);
            this.btnFormsApply.TabIndex = 14;
            this.btnFormsApply.Text = "Применить";
            this.btnFormsApply.UseVisualStyleBackColor = true;
            this.btnFormsApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(74, 149);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(61, 13);
            this.lblDate.TabIndex = 13;
            this.lblDate.Text = "01.01.2016";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Дата";
            // 
            // gpIcon
            // 
            this.gpIcon.Controls.Add(this.btnIconBrowse);
            this.gpIcon.Controls.Add(this.picIcon);
            this.gpIcon.Location = new System.Drawing.Point(348, 21);
            this.gpIcon.Name = "gpIcon";
            this.gpIcon.Size = new System.Drawing.Size(79, 101);
            this.gpIcon.TabIndex = 11;
            this.gpIcon.TabStop = false;
            this.gpIcon.Text = "Иконка";
            // 
            // btnIconBrowse
            // 
            this.btnIconBrowse.Location = new System.Drawing.Point(8, 76);
            this.btnIconBrowse.Name = "btnIconBrowse";
            this.btnIconBrowse.Size = new System.Drawing.Size(60, 19);
            this.btnIconBrowse.TabIndex = 1;
            this.btnIconBrowse.Text = "Обзор";
            this.btnIconBrowse.UseVisualStyleBackColor = true;
            this.btnIconBrowse.Click += new System.EventHandler(this.btnBrowseIcon_Click);
            // 
            // picIcon
            // 
            this.picIcon.Location = new System.Drawing.Point(8, 15);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(60, 60);
            this.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picIcon.TabIndex = 0;
            this.picIcon.TabStop = false;
            // 
            // chkFormActive
            // 
            this.chkFormActive.AutoSize = true;
            this.chkFormActive.Location = new System.Drawing.Point(271, 148);
            this.chkFormActive.Name = "chkFormActive";
            this.chkFormActive.Size = new System.Drawing.Size(68, 17);
            this.chkFormActive.TabIndex = 10;
            this.chkFormActive.Text = "Активна";
            this.chkFormActive.UseVisualStyleBackColor = true;
            this.chkFormActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(74, 123);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(264, 20);
            this.txtVersion.TabIndex = 5;
            this.txtVersion.TextChanged += new System.EventHandler(this.txtVersion_TextChanged);
            // 
            // labelVEr
            // 
            this.labelVEr.AutoSize = true;
            this.labelVEr.Location = new System.Drawing.Point(21, 126);
            this.labelVEr.Name = "labelVEr";
            this.labelVEr.Size = new System.Drawing.Size(44, 13);
            this.labelVEr.TabIndex = 4;
            this.labelVEr.Text = "Версия";
            // 
            // txtArgs
            // 
            this.txtArgs.Location = new System.Drawing.Point(74, 71);
            this.txtArgs.Name = "txtArgs";
            this.txtArgs.Size = new System.Drawing.Size(265, 20);
            this.txtArgs.TabIndex = 3;
            this.txtArgs.TextChanged += new System.EventHandler(this.txtArgs_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Аргументы";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnBrowse);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 26);
            this.panel1.TabIndex = 0;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(267, 1);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Обзор";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Добавить или обновить форму:";
            // 
            // cmbFormName
            // 
            this.cmbFormName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormName.FormattingEnabled = true;
            this.cmbFormName.Location = new System.Drawing.Point(80, 32);
            this.cmbFormName.Name = "cmbFormName";
            this.cmbFormName.Size = new System.Drawing.Size(265, 21);
            this.cmbFormName.TabIndex = 1;
            this.cmbFormName.SelectedIndexChanged += new System.EventHandler(this.cmbFormName_SelectedIndexChanged_1);
            this.cmbFormName.SelectedValueChanged += new System.EventHandler(this.cmbFormName_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Имя формы";
            // 
            // tabSettings
            // 
            this.tabSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabSettings.Controls.Add(this.tabForms);
            this.tabSettings.Location = new System.Drawing.Point(3, 3);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(881, 263);
            this.tabSettings.TabIndex = 3;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 320);
            this.Controls.Add(this.pnlControls);
            this.Controls.Add(this.tabSettings);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.Shown += new System.EventHandler(this.frmSettings_Shown);
            this.pnlControls.ResumeLayout(false);
            this.tabForms.ResumeLayout(false);
            this.pnlFormSettings.ResumeLayout(false);
            this.pnlFormSettings.PerformLayout();
            this.gpForm.ResumeLayout(false);
            this.gpForm.PerformLayout();
            this.pnlMenu.ResumeLayout(false);
            this.pnlMenu.PerformLayout();
            this.gpIcon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog ofdSelectImage;
        private System.Windows.Forms.OpenFileDialog ofdSelectDll;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TabPage tabForms;
        private System.Windows.Forms.Panel pnlFormSettings;
        private System.Windows.Forms.GroupBox gpForm;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkHasMenu;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.TextBox txtMenu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbMenu;
        private System.Windows.Forms.Button btnFormsApply;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gpIcon;
        private System.Windows.Forms.Button btnIconBrowse;
        private System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.CheckBox chkFormActive;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label labelVEr;
        private System.Windows.Forms.TextBox txtArgs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbFormName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabSettings;
    }
}

