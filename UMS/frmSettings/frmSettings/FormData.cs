﻿using System;
using System.Data;

namespace UMS
{
    /// <summary>
    /// Класс данных по формам
    /// </summary>
    class FormData
    {
        //static DataSet dsData=new DataSet(); //Список всех форм и их данных
        //static DataTable dtMenuData = new DataTable(); //Список всех главных пунктов меню
        //MySqlConnection FormDataConenction = new MySqlConnection(UMS.Terminal.TerminalConnectionString);
        //MySqlDataAdapter daFormData;
        //MySqlDataAdapter daMenuData;
        //MySqlCommand GetMenuData;
        ///// <summary>
        ///// Конструктор класса. Вызывается при создании нового экземпляра
        ///// </summary>
        //public FormData()
        //{
        //    FormDataConenction.Open();
        //    //Заранее адапетрам назначим команды
        //    GetMenuData = new MySqlCommand("Call GetMainMenu(1)", FormDataConenction);
        //    daMenuData = new MySqlDataAdapter(GetMenuData);

        //    MySqlCommand GetFormData = new MySqlCommand("Call GetAllForms()", FormDataConenction);
        //    daFormData = new MySqlDataAdapter();
        //    daFormData.SelectCommand = GetFormData;
        //    MySqlCommandBuilder cmbdrCommands = new MySqlCommandBuilder(); //Билдер команд сам сделает необходимые команды, если в DataSet, который вернет адаптер будет уникальный ключ
        //    cmbdrCommands.DataAdapter = daFormData; //Назначали командбилдеру адаптер. команды на вставку, удаление и обновление создались сами. PROFIT!
        //}
        ///// <summary>
        ///// Получает данные по формам и меню и записывает их в dtData и dtMenuData
        ///// </summary>
        //public  void FillAllFormData()
        //{
        //    dtMenuData.Clear();
        //    dsData.Clear();
        //    daMenuData.Fill(dtMenuData);         
        //    daFormData.Fill(dsData);
        //}
        ///// <summary>
        ///// Возвращает DataTable с формам
        ///// </summary>
        //public DataTable Data
        //{
        //    get { return dsData.Tables[0]; }
        //}
        ///// <summary>
        ///// Возвращает DataTable с Меню
        ///// </summary>
        //public DataTable MenuData
        //{
        //    get { return dtMenuData; }
        //}
        ///// <summary>
        ///// Обновляет или вставляет измененные строки
        ///// </summary>
        //public int UpdateData()
        //{
        //    return daFormData.Update(dsData.Tables[0]);
        //}
    }
}
